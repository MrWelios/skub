// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    api: {
        // url : 'http://172.16.117.166:13000',
        url: 'http://41731.dev3.i-chain.net/api/',
        // url : 'https://41081.dev3.i-chain.net/api',
        // url : 'http://localhost:8580/api/',
        ws: 'ws://172.16.117.96:15000/ws',
        // ws: {
        //     host: '//172.16.117.166:14100/ws',
        //     protocol: 'ws',
        // },
        kibana: {
            host: 'http://172.16.117.65',
            port: '5601',
        },
    },
};
