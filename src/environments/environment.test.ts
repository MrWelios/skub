export const environment = {
    production: false,
    api: {
        url : '/api',
        ws : 'wss://test3.i-chain.net/api/ws',
        // ws: {
        //     host: '/api/ws',
        //     protocol: 'wss',
        // },
        kibana: {
            host: '/',
            port: '8570',
        },
    },
};
