export const environment = {
    production: false,
    api: {
        url : '/api',
        // url : 'http://41731.dev3.i-chain.net/api/',
        ws : '/api/ws',
        // ws: {
        //     host: '/api/ws',
        //     protocol: 'wss',
        // },
        kibana: {
            host: '/',
            port: '8570',
        },
    },
};
