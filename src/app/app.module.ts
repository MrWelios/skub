import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ErrorHandler, NgModule, enableProdMode } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppRoutingModule } from './routing.module';
import { SelectionModule } from 'app/directives/selection-area/selection.module';
import { UIService } from './services/UIService';
import { AppComponent } from './app.component';
import { ErrorPageComponent } from './pages/components/error-page/index';
import { TooltiplDirective } from './directives/tooltip.directive';
import { GlobalErrorHandler } from './services/GlobalErrorHandler';
import { LoggedGuard } from 'app/guards/LoggedGuards';
import { RoleGuard } from './guards/RoleGuard';
import { LoadGuard } from './guards/LoadGuard';
import { LoaderComponent } from './widgets/loader';
import { NotificationsModule } from './widgets/notifications/module';
import { NavbarModule } from './widgets/navbar/module';
import { WebSocketService } from './services/WebSocketService';
import { ManagerChannelWsService } from './services/ManagerChannelWsService';
import { FileService } from './services/FileService';
import { SettingsModule } from './pages/protected/settings/settings.module';
import { ClaimService } from 'app/services/pages/ClaimService';
import { HttpClientInterceptor } from './utilites/HttpClientInterceptor';
import { BPService } from './services/BPService';
import { EntityService } from './services/EntityService';
import { LoginService } from './services/pages/LoginService';
import { SidebarModule } from './widgets/sidebar/module';
import { UiTranslateLoader } from './utilites/UiTranslateLoader';
import { ProfileService } from './services/pages/ProfileService';
import { AuthGuard } from './guards/AuthGuard';
import { CustomViewService } from './services/CustomViewService';

// enableProdMode();
export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/app/', '.json');
}

@NgModule({
    imports: [
        SettingsModule,
        SidebarModule,
        NavbarModule,
        FlexLayoutModule,
        BrowserAnimationsModule,
        BrowserModule,
        FormsModule,
        StoreModule.forRoot({}),
        EffectsModule.forRoot([]),
        HttpModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useClass: UiTranslateLoader,
                /*useFactory: createTranslateLoader,*/
                deps: [UIService/*, HttpClient*/],
            },
            isolate: true,
        }),
        AppRoutingModule,
        StoreRouterConnectingModule,
        SelectionModule,
        NotificationsModule,
    ],
    declarations: [
        AppComponent,
        ErrorPageComponent,
        TooltiplDirective,
        LoaderComponent,
    ],
    providers: [
        ClaimService,
        UIService,
        LoggedGuard,
        LoginService,
        AuthGuard,
        RoleGuard,
        LoadGuard,
        FileService,
        ProfileService,
        WebSocketService,
        ManagerChannelWsService,
        EntityService,
        CustomViewService,
        BPService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpClientInterceptor,
            multi: true,
        },
        {
            provide: ErrorHandler,
            useClass: GlobalErrorHandler,
        },
    ],
    bootstrap: [AppComponent],
})
export class AppModule { }
