import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Storages } from 'app/utilites/Storages';
import { LS_SESSIONID } from 'app/services/constants/CustomHttpService.constants';

@Injectable()
export class HttpClientInterceptor implements HttpInterceptor {
    clientHash = Storages.getClientHash();

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const storageId = Storages.getSessionItem(LS_SESSIONID);
        if (storageId && !req.url.includes(':8570')) {
            req = req.clone({ headers: req.headers.set('Authorization', 'Bearer ' + storageId), withCredentials: true });
        } else if (storageId && req.url.includes(':8570')) {
            req = req.clone({ withCredentials: true });
        } else {
            req = req.clone({ headers: req.headers.set('CLIENT_HASH', this.clientHash), withCredentials: true });
        }
        return next.handle(req).catch((error: HttpErrorResponse) => {
            if (error.status === 401 && (window.location.href.match(/\?/g) || []).length < 2) {
                Storages.clearSessionInfo();
                window.location.href = '/login';
            }
            return Observable.throw(error);
        });
    }
}
