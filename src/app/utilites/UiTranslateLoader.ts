import { TranslateLoader } from '@ngx-translate/core';
import { Observable } from 'rxjs';

import { UIService } from 'app/services/UIService';

export class UiTranslateLoader implements TranslateLoader {
    constructor(private uiService: UIService) {}
  
    public getTranslation(lang: string): Observable<any> {
        return this.uiService.processLangState(lang);
    }
}
