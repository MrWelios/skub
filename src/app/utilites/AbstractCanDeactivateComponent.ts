import { HostListener } from '@angular/core';

export abstract class ComponentCanDeactivate {
    abstract hasUnsavedData = false;

    @HostListener('window:beforeunload', ['$event'])
        unloadNotification($event: any) {
        if (this.hasUnsavedData) {
            $event.returnValue = true;
        }
    }
}
