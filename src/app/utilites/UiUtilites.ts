import get from 'lodash/get';
import { isObject } from 'rxjs/internal-compatibility';

import { environment } from 'environments/environment';

export class UiUtilites {

    static getKibanaUrl(): string {
        const host = get(environment, 'api.kibana.host', '/');
        const port = get(environment, 'api.kibana.port', '');
        const displayPort = port ? `:${port}` : '';
        return host[0] === '/' ? `${window.location.protocol}//${window.location.host + displayPort + host}` : host + displayPort;
    }

    static sortObject(input, attribute) {
        if (!isObject(input)) {
            return input;
        }

        return Object.keys(input).sort((a, b) => {
            if (!input[a].hasOwnProperty(attribute)) {
                return -1;
            }
            if (!input[b].hasOwnProperty(attribute)) {
                return 1;
            }
            return parseInt(input[a][attribute], 10) - parseInt(input[b][attribute], 10);
        });
    }

    static sortArray(input, attribute) {
        return input.sort((a, b) => {
            if (a[attribute] > b[attribute]) {
                return 1;
            }
            if (a[attribute] < b[attribute]) {
                return -1;
            }
            return 0;
        });
    }
}
