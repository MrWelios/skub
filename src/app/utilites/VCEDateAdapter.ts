import { NativeDateAdapter } from '@angular/material';

export class VCEDateAdapter extends NativeDateAdapter {
    format(date: Date, displayFormat: Object): string {
        if (displayFormat === 'input') {
            const day = date.getDate();
            const month = date.getMonth() + 1;
            const year = date.getFullYear();
            return `${this.to2digit(day)}/${this.to2digit(month)}/${year}`;
        } 
        return date.toDateString();
    }

    getFirstDayOfWeek(): number {
        return 1;
    }

    private to2digit(n: number) {
        return ('00' + n).slice(-2);
    } 
}
