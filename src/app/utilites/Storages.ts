import { Observable } from 'rxjs';

import { ModuleSchemaInterface } from '../constants';
import { LS_SESSIONID } from '../services/constants/CustomHttpService.constants';

export class Storages {
    static getClientHash(): string {
        let sendVals = sessionStorage.getItem('clientHash');
        if (!sendVals) {
            sendVals = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
            sessionStorage.setItem('clientHash', sendVals);
        }
        return sendVals;
    }

    static getModulesSchema(): any[] {
        return JSON.parse(Storages.getSessionItem('modulesSchema'));
    }

    static setModulesSchema(schema: ModuleSchemaInterface): void {
        Storages.setSessionItem('modulesSchema', JSON.stringify(schema));
    }

    static setSidebarMenu(menuList): void {
        Storages.setSessionItem('sidebarMenu', JSON.stringify(menuList));
    }

    static getSidebarMenu(): any[] {
        return JSON.parse(Storages.getSessionItem('sidebarMenu'));
    }

    static getLangSchema(lang: string): any {
        return JSON.parse(Storages.getSessionItem(`langSchema_${lang}`));
    }

    static setLangSchema(lang: string, langSchema: any) {
        Storages.setSessionItem(`langSchema_${lang}`, JSON.stringify(langSchema));
    }

    static getLocales(): any {
        return JSON.parse(Storages.getSessionItem('locales'));
    }

    static setLocales(locales: string[]) {
        Storages.setSessionItem('locales', JSON.stringify(locales));
    }

    static getCurrentLang(): string {
        return Storages.getItem('currentLang');
    }

    static setCurrentLang(lang: string): void {
        return Storages.setItem('currentLang', lang);
    }

    static clearSessionInfo(): void {
        sessionStorage.clear();
        sessionStorage.removeItem(LS_SESSIONID);
    }

    static setQueryList(name: string, value: any) {
        const vals = JSON.stringify(value);
        sessionStorage.setItem(name, vals);
    }

    static getQueryList(item: string): any {
        // let value = JSON.parse(sessionStorage.getItem(item));
        // if (!value) {
        const value = {
            page: 1,
            itemsPerPage: 5,
            sort: {
            },
            filter: {
            },
        };
        // }
        return value;
    }

    /**
     * Set item in local storage
     * @param {String} item name of the value stored in localStorage
     * @param {String} value value stored in localStorage
     */
    static setItem(item: string, value: string): void {
        localStorage.setItem(item, value);
    }

    /**
     * Set item in session storage
     * @param {string} item name of the value stored in sessionStorage
     * @param {string} value value stored in sessionStorage
     */
    static setSessionItem(item: string, value: string): void {
        sessionStorage.setItem(item, value);
    }

    /**
     * Get item from local Storage
     * @param {string} item name of the value stored in localStorage
     * 
     * @returns {string}
     */
    static getItem(item: string): string {
        return localStorage.getItem(item);
    }

    /**
     * Get item from session Storage
     * @param {string} item name of the value stored in sessionStorage
     * 
     * @returns {string}
     */
    static getSessionItem(item: string): any {
        return sessionStorage.getItem(item);
    }

    /**
     * Set boolean item in local storage
     * @param {string} item name of the value stored in localStorage
     * @param {boolean} value value stored in localStorage
     */
    static setBoolean(item: string, value: boolean): void {
        localStorage.setItem(item, value.toString());
    }

    /**
     * Get boolean item from local Storage
     * @param {string} item name of the value stored in localStorage
     * 
     * @returns {boolean}
     */
    static getBoolean(item: string): boolean {
        const value = localStorage.getItem(item);
        return value === null ? null : value === 'true';
    }

    /**
     * delete item from local Storage
     * @param item name of the value stored in localStorage
     */
    static removeItem(item: string): void {
        localStorage.removeItem(item);
    }

    /**
     * delete item from Session Storage
     * @param item name of the value stored in sessionStorage
     */
    static removeSessionItem(item: string): void {
        sessionStorage.removeItem(item);
    }
}
