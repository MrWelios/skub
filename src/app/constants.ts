import { Params } from '@angular/router';

export const APP_LOADING_STATE_CHANGED = 'APP_LOADING_STATE_CHANGED';
export const RESET_APP_STATE = 'RESET_APP_STATE';
export const STATE_SESSION_STORAGE = 'STATE_SESSION_STORAGE';
export const STATE_SET_USER_ROLE = 'STATE_SET_USER_ROLE';
export const STATE_GET_USER_ROLE = 'STATE_GET_USER_ROLE';
export const STATE_LOADING_START = 'STATE_LOADING_START';
export const STATE_LOADING_PROCESS = 'STATE_LOADING_PROCESS';
export const STATE_LOADING_FINISH = 'STATE_LOADING_FINISH';
export const STATE_LOADING_ERROR = 'STATE_LOADING_ERROR';
export const STATE_LOADING_CLEAR = 'STATE_LOADING_CLEAR';
export const GET_MODULES_SCHEMA = 'GET_MODULES_SCHEMA';
export const DOWNLOAD_FILE_START = 'DOWNLOAD_FILE_START';
export const DOWNLOAD_FILE_FINISH = 'DOWNLOAD_FILE_FINISH';

export const SET_SYSTEM_LABEL_LINK = 'SET_SYSTEM_LABEL_LINK';
export const GET_SYSTEM_LABEL_LINK = 'GET_SYSTEM_LABEL_LINK';

export const GET_PROFILE_DATA = 'GET_PROFILE_DATA';
export const SET_PROFILE_DATA = 'SET_PROFILE_DATA';
export const SAVE_PROFILE_DATA = 'SAVE_PROFILE_DATA';

export const VCE_DATE_FORMATS = {
    parse: {
        dateInput: { month: 'short', year: 'long', day: 'numeric' },
    },
    display: {
        dateInput: 'input',
        monthYearLabel: { year: 'long', month: 'numeric' },
        dateA11yLabel: { year: 'numeric', month: 'long', day: 'numeric' },
        monthYearA11yLabel: { year: 'numeric', month: 'long' },
    },
};

export const CLOSE_MODAL = 'CLOSE_MODAL';
export const CANGE_MEDIA = 'CANGE_MEDIA';
export const REGISTER_ERRORS = 'REGISTER_ERRORS';

/**
 *  Notifications
 */
export const NOTIFICATION_TYPE_SUCCESS = 'NOTIFICATION_TYPE_SUCCESS';
export const NOTIFICATION_TYPE_ERROR = 'NOTIFICATION_TYPE_ERROR';
export const NOTIFICATION_TYPE_WARN = 'NOTIFICATION_TYPE_WARN';
export const NOTIFICATION_TYPE_INFO = 'NOTIFICATION_TYPE_INFO';

export const SCREEN_MEDIA = ['xl', 'lg', 'md', 'sm', 'xs'];

export type NotificationType =
    'NOTIFICATION_TYPE_SUCCESS' |
    'NOTIFICATION_TYPE_ERROR' |
    'NOTIFICATION_TYPE_WARN' |
    'NOTIFICATION_TYPE_INFO';

/**
 *  Mass actions
 */

export const DEFAULT_LANG = 'en';
export const DEFAULT_LOCALES = ['en'];
export const APP_LANG_STATE_CHANGED = 'APP_LANG_STATE_CHANGED';
export const APP_LOCALES_STATE_CHANGED = 'APP_LOCALES_STATE_CHANGED';

export const CLEAR_MODULES_SCHEMA = 'CLEAR_MODULES_SCHEMA';
export const SET_MODULES_SCHEMA = 'SET_MODULES_SCHEMA';
export const GENERATE_CODE = 'GENERATE_CODE';

export interface ModuleSchemaInterface {
    name: string;
    label: string;
    icon: string;
    moduleType: string;
    management: boolean;
    view: boolean;
    edit: boolean;
    edit_confirm: boolean;
    create: boolean;
    delete: boolean;
    object_type: string;
    entity_schema_id: number;
    showInMenu: boolean;
    customView: any;
    data: {
        [section_name: string]: {
            label: string;
            order: number;
            width: number;
            fields: {
                [field_name: string]: {
                    display_type: string;
                    view: boolean;
                    edit: boolean;
                    canCopy: boolean;
                    required: boolean;
                    label: string;
                    order: number;
                    width: number;
                    list_data?: {
                        label: string;
                        value: string;
                    }[];
                };
            },
        },
    };
}

export const FORM_TYPES = [
    'string', 'common_symbols_string', 'password', 'textarea', 'select', 'checkbox', 'multiselect', 'number',
    'chips', 'date', 'datetime', 'phone', 'zipcode', 'email', 'radio', 'float', 'link', 'file', 'label_link', 'external_link',
    'hidden', 'markdown', 'multipleFile', 'field_map', 'price', 'system_label_link', 'time', 'dependent_label_link', 'multi_label_link',
];

export const FILL_REQUIRED_LABEL = 'ERRORS.FILL_REQUIRED_LABEL';
export const FILL_FORM_CORRECT_LABEL = 'ERRORS.FILL_FORM_CORRECT_LABEL';

export const MONTH_NAMES = [
    'JANUARY',
    'FEBRUARY',
    'MARCH',
    'APRIL',
    'MAY',
    'JUNE',
    'JULY',
    'AUGUST',
    'SEPTEMBER',
    'OCTOBER',
    'NOVEMBER',
    'DECEMBER',
];
export interface BreadcrumbItem {
    label: string;
    params: Params;
    url: string;
    queryParams?: {
        [key: string]: any;
    };
}
