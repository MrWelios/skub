import { Injectable } from '@angular/core';
import { Router, CanActivate, CanDeactivate } from '@angular/router';

import { ComponentCanDeactivate } from '../utilites/AbstractCanDeactivateComponent';

@Injectable()
export class CanDeactivateGuard implements CanDeactivate<ComponentCanDeactivate> {
    canDeactivate(component: ComponentCanDeactivate): boolean {
        if (component.hasUnsavedData) {
            if (confirm('You have unsaved changes! If you leave, your changes will be lost.')) {
                return true;
            }
            return false;
        }
        return true;
    }
}
