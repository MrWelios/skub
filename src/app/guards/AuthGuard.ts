import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import get from 'lodash/get';
import 'rxjs/add/observable/of';

import { UIService } from 'app/services/UIService';
import { LS_SESSIONID, SS_RETURN_URL } from 'app/services/constants/CustomHttpService.constants';
import { LS_USER_LOGIN } from 'app/services/constants/LoginService.constants';
import cloneDeep from 'lodash/cloneDeep';
import { AppStateInterface } from 'app/store';
import { setAppLoadingState, setModulesSchema } from 'app/actions';
import { Storages } from 'app/utilites/Storages';

@Injectable()
export class AuthGuard implements CanActivate {
    allowedModules = [];
    schema: any;

    constructor(
        private router: Router,
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (document.cookie.match(LS_SESSIONID) || sessionStorage.getItem(LS_SESSIONID)) {
            return true;
        }
        
        this.router.navigate(['/']);
        return false;
    }

}
