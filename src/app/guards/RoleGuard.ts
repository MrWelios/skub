import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import get from 'lodash/get';
import 'rxjs/add/observable/of';

import { UIService } from 'app/services/UIService';
import { LS_SESSIONID, SS_RETURN_URL } from 'app/services/constants/CustomHttpService.constants';
import { LS_USER_LOGIN } from 'app/services/constants/LoginService.constants';
import cloneDeep from 'lodash/cloneDeep';
import { AppStateInterface } from 'app/store';
import { setAppLoadingState, setModulesSchema } from 'app/actions';
import { Storages } from 'app/utilites/Storages';

@Injectable()
export class RoleGuard implements CanActivate {
    allowedModules = [];
    schema: any;

    constructor(
        private router: Router,
        private store$: Store<AppStateInterface>,
        private uiService: UIService,
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.store$.dispatch(setAppLoadingState(true));

        this.uiService.getStoreModulesSchema().first().subscribe((schema) => {
            this.schema = cloneDeep(schema);
        });
        
        if (this.schema) {
            return this.checkModuleAccess(this.schema, route);
        }

        return this.uiService.getModules().map((modules) => {
            Storages.setModulesSchema(modules.modules);
            this.store$.dispatch(setModulesSchema(modules.modules));
            return this.checkModuleAccess(modules.modules, route);
        });
    }

    private checkModuleAccess(schema, route: ActivatedRouteSnapshot): boolean {
        if (schema) {
            const moduleName = get(route, 'params.entity');
            if (!!sessionStorage.getItem(LS_SESSIONID)) {
                this.allowedModules = schema.map(moduleItem => moduleItem.name);
                if (this.allowedModules) {
                    if (moduleName && this.allowedModules.includes(moduleName)) {
                        this.store$.dispatch(setAppLoadingState(false));
                        return true;
                    }
                    this.router.navigate(['modules/' + this.allowedModules[0]]);
                }
                this.store$.dispatch(setAppLoadingState(false));
                return false;
            }
        }

        sessionStorage.setItem(SS_RETURN_URL, this.router.url);
        sessionStorage.removeItem(LS_SESSIONID);
        sessionStorage.removeItem(LS_USER_LOGIN);
        this.router.navigate(['/']);
        return false;
    }

}
