import { Injectable, NgModuleFactory } from '@angular/core';
import { Router, Route, CanLoad, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import get from 'lodash/get';
import 'rxjs/add/observable/of';

import { UIService } from 'app/services/UIService';
import { LS_SESSIONID, SS_RETURN_URL } from 'app/services/constants/CustomHttpService.constants';
import { LS_USER_LOGIN } from 'app/services/constants/LoginService.constants';
import cloneDeep from 'lodash/cloneDeep';
import { AppStateInterface } from 'app/store';
import { setAppLoadingState, setModulesSchema } from 'app/actions';
import { Storages } from 'app/utilites/Storages';
import * as modules from 'app/pages/protected';
@Injectable()
export class LoadGuard implements CanLoad {
    allowedModules = [];
    schema: any;

    constructor(
        private router: Router,
        private store$: Store<AppStateInterface>,
        private uiService: UIService,
    ) { }

    canLoad(route: Route) {
        // console.log('LOAD')
        // console.log('route');
        // console.log(route);
        // console.log(this.router);
        // // const path = 'app/pages/protected/users/users.module#UsersModule';
        // route.loadChildren = new NgModuleFactory<UsersModule>();
        // console.log('modules')
        // console.log(modules)
        // route.loadChildren = () => Modules;
        // console.log('route');
        // console.log(route);
        // const path = 'app/pages/protected/entity/entity.module#EntityModule';
        return true;
        // return this.permissions.canLoadChildren(this.currentUser, route);
    }


}
