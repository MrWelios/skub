import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

import { LS_SESSIONID } from 'app/services/constants/CustomHttpService.constants';

@Injectable()
export class LoggedGuard implements CanActivate {

    constructor(
        private router: Router,
    ) { }

    canActivate() {
        if (!document.cookie.match(LS_SESSIONID) && !sessionStorage.getItem(LS_SESSIONID)) {
            return true;
        }
        
        this.router.navigate(['/modules']);
        return false;
    }
}
