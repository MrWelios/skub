import { Directive, Input, ElementRef, Renderer, OnInit, Output, EventEmitter } from '@angular/core';

@Directive({
    selector: '[appCurrencyMask]',
    // tslint:disable-next-line:use-host-property-decorator
    host: {
        '(input)': 'onChange($event)',
    },
})

export class VCEMaskDirective implements OnInit {
    @Input() set appCurrencyMask(val: number) {
        this.count = val;
    }
    @Input() set fraction(val: number) {
        this.fractionCount = val;
    }
    @Input() isNegative = false;
    @Input() isWhiteSpace = false;
    @Input() changedValue;

    @Output() changedValueChange = new EventEmitter<number>();

    private count: number;
    private fractionCount: number;
    private vall;
    private oldVal;

    constructor(private _elementRef: ElementRef) { }

    ngOnInit() {
        this.oldVal = this.changedValue;
        this.setVall(this.changedValue);
    }

    onChange(event: Event) {
        this.vall = this.deleteWhiteSpace();
        this.regexp();
    }
    regexp() {
        let fractReg = '';
        if (this.fractionCount) {
            fractReg = '(\\.[0-9]{0,' + this.fractionCount + '})?';
        }
        const regex = new RegExp('^' + (this.isNegative ? '[-]?' : '') + '[1-9]?([0-9]{0,' + (this.count - 1) + '}' + fractReg + ')$');
        if (this.vall && !this.vall.match(regex)) {
            this.oldVal = this.vall;
            this.vall = (this.vall).slice(0, -1);
            this.regexp();
        } else if (this.vall !== this.oldVal) {
            this.oldVal = this.vall;
            this.changedValueChange.emit(this.vall);
            this.setVall(this.vall);
        }

    }

    deleteWhiteSpace() {
        return this._elementRef.nativeElement.value.replace(/\s+/g, '');
    }

    setVall(params) {
        let vall = params;
        if (this.isWhiteSpace) {
            vall = this.setWhiteSpace(params);
        }
        this._elementRef.nativeElement.value = vall;
        const event = new Event('input');
        this._elementRef.nativeElement.dispatchEvent(event);
    }

    setWhiteSpace(vall) {
        if (vall) {
            return vall.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
        }
        return vall;
    }
}
