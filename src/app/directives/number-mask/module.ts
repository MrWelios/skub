import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VCEMaskDirective } from './component';

@NgModule({
    imports: [CommonModule],
    declarations: [VCEMaskDirective],
    exports: [VCEMaskDirective],
})

export class MaskModule {
}
