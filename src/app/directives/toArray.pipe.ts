import { NgModule, Pipe, PipeTransform } from '@angular/core';
import { CommonModule } from '@angular/common';

@Pipe({
    name: 'toArray',
})

export class ToArrayPipe implements PipeTransform {
    transform(value: string, args: any[]) {
        if (!value || typeof value !== 'object') {
            return [];
        }

        return Object.keys(value).map((key) => {
            return {
                prop: key,
                value: value[key],
            };
        });
    }
}

@NgModule({
    imports: [CommonModule],
    declarations: [ToArrayPipe],
    exports: [ToArrayPipe],
})

export class ToArrayModule {}
