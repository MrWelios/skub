import { Directive, Input, HostListener, HostBinding, Renderer2, ElementRef, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { AppStateInterface } from 'app/store';
import { addNotification } from '../../actions';
import { NOTIFICATION_TYPE_ERROR, NOTIFICATION_TYPE_SUCCESS } from '../../constants';
import { TranslateService } from '@ngx-translate/core';

@Directive({
    selector: '[appCopyClick]',
})
export class CopyClickDirective implements OnInit {
    @Input() appCopyClick: string;

    @HostBinding('style.cursor') cursor = 'pointer';

    constructor(
        protected store$: Store<AppStateInterface>,
        private renderer: Renderer2,
        private elementRef: ElementRef,
        private translate: TranslateService,
    ) { }

    @HostListener('click') copyText() {
        const textArea = document.createElement('textarea');
        textArea.style.position = 'fixed';
        textArea.style.top = '-999px';
        textArea.style.left = '-999px';
        textArea.style.width = '2em';
        textArea.style.height = '2em';
        textArea.style.padding = '0';
        textArea.style.border = 'none';
        textArea.style.outline = 'none';
        textArea.style.boxShadow = 'none';
        textArea.style.background = 'transparent';
        textArea.value = this.appCopyClick;
        document.body.appendChild(textArea);
        textArea.select();

        try {
            const successful = document.execCommand('copy');
            if (successful) {
                this.translate.get('TEXT_COPIED').subscribe((value) => {
                    this.store$.dispatch(addNotification(NOTIFICATION_TYPE_SUCCESS, value));
                });
            } else {
                this.translate.get('ERRORS.COPY_ERROR').subscribe((value) => {
                    this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                });
            }
        } catch (err) {
            this.translate.get('ERRORS.UNABLE_COPY').subscribe((value) => {
                this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
            });
        }

        document.body.removeChild(textArea);
    }

    ngOnInit(): void {
        const matIcon = this.renderer.createElement('mat-icon');
        this.renderer.setStyle(matIcon, 'position', 'absolute');
        this.renderer.setStyle(matIcon, 'right', 0);
        this.renderer.setStyle(matIcon, 'top', '5px');
        this.renderer.addClass(matIcon, 'mat-icon');
        this.renderer.addClass(matIcon, 'material-icons');
        this.renderer.appendChild(matIcon, this.renderer.createText('file_copy'));
        this.renderer.appendChild(this.elementRef.nativeElement, matIcon);
    }
}
