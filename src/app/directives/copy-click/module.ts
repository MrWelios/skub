import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CopyClickDirective } from './directive';

@NgModule({
    imports: [CommonModule],
    declarations: [CopyClickDirective],
    exports: [CopyClickDirective],
})

export class CopyClickModule {
}
