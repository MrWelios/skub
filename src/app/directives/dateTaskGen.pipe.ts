import { NgModule, Pipe, PipeTransform } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { MONTH_NAMES } from 'app/constants';

@Pipe({
    name: 'dateTaskGen',
})

export class DateTaskGenPipe implements PipeTransform {

    constructor(
        public translate: TranslateService,
    ) {}

    transform(event: string, args: any[]) {
        if (!event) {
            return '';
        }
        const today = new Date();
        const date = new Date(event);
        if (today.toDateString() === date.toDateString()) {
            return this.translate.instant('TODAY');
        }
        today.setDate(today.getDate() - 1);
        if (today.toDateString() === date.toDateString()) {
            return this.translate.instant('YESTERDAY');
        }

        today.setDate(today.getDate() + 2);
        if (today.toDateString() === date.toDateString()) {
            return this.translate.instant('TOMORROW');
        }

        const returningDate = `${date.getDate()} ${this.translate.instant(`MONTHS_GEN_CASE.${MONTH_NAMES[date.getMonth()]}`)}, ${date.getFullYear()}`;

        return returningDate;
    }
}

@NgModule({
    imports: [CommonModule],
    declarations: [DateTaskGenPipe],
    exports: [DateTaskGenPipe],
})

export class DateTaskGenModule {}
