import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateMaskDirective } from './directive';

@NgModule({
    imports: [CommonModule],
    declarations: [DateMaskDirective],
    exports: [DateMaskDirective],
})

export class DateMaskModule {
}
