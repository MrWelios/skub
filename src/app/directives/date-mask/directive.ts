import {
    Directive,
    Input,
    ElementRef,
    Renderer,
    OnInit,
} from '@angular/core';

@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[vceDateMask]',
    // tslint:disable-next-line:use-host-property-decorator
    host: {
        '(input)': 'onChange($event)',
    },
})
export class DateMaskDirective implements OnInit {
    @Input() set cvmMask(val: number) {
        this.count = val;
    }
    @Input() set fraction(val: number) {
        this.fractionCount = val;
    }
    @Input() isNegative = false;

    private oldLength = 0;
    private count: number;
    private fractionCount: number;
    
    constructor(private _elementRef: ElementRef) { }

    ngOnInit() {
    }

    onChange(event: Event) {
        this.regexp();
    }
    setOld() {
        this.oldLength = this._elementRef.nativeElement.value.length;
    }
    regexp() {
        const regexDay = /^(0[1-9]?|[12][0-9]?|3[01]?)$/;
        const regexMont = /^(0[1-9]|[12][0-9]|3[01])[- /\.](0[1-9]?|1[012]?)$/;
        const regexYear = /^(0[1-9]|[12][0-9]|3[01])[- /\.](0[1-9]|1[012])[- /\.](1|2)\d?\d?\d?$/;

        const regDay = new RegExp(regexDay);
        const regMonth = new RegExp(regexMont);
        const regYear = new RegExp(regexYear);

        if (this._elementRef.nativeElement.value
            && this._elementRef.nativeElement.value.length < 3
            && !this._elementRef.nativeElement.value.match(regDay)) {

            this._elementRef.nativeElement.value = (this._elementRef.nativeElement.value).slice(0, -1);
            const event = new Event('input');
            this._elementRef.nativeElement.dispatchEvent(event);
            this.regexp();

        } else if (this._elementRef.nativeElement.value
            && this._elementRef.nativeElement.value.length === 2
            && this._elementRef.nativeElement.value.match(regDay) && this._elementRef.nativeElement.value.length >= this.oldLength) {

            this._elementRef.nativeElement.value = this._elementRef.nativeElement.value + '.';
            const event = new Event('input');
            this._elementRef.nativeElement.dispatchEvent(event);
            this.regexp();

        } else if (this._elementRef.nativeElement.value
            && this._elementRef.nativeElement.value.length === 3
            && !this._elementRef.nativeElement.value.match(new RegExp(/^(0[1-9]|[12][0-9]|3[01])[- /\.]/))) {

            this._elementRef.nativeElement.value = (this._elementRef.nativeElement.value).slice(0, -1);
            const event = new Event('input');
            this._elementRef.nativeElement.dispatchEvent(event);
            this.regexp();

        } else if (this._elementRef.nativeElement.value
            && this._elementRef.nativeElement.value.length > 3 && this._elementRef.nativeElement.value.length < 6
            && !this._elementRef.nativeElement.value.match(regMonth)) {

            this._elementRef.nativeElement.value = (this._elementRef.nativeElement.value).slice(0, -1);
            const event = new Event('input');
            this._elementRef.nativeElement.dispatchEvent(event);
            this.regexp();

        } else if (this._elementRef.nativeElement.value
            && this._elementRef.nativeElement.value.length === 5
            && this._elementRef.nativeElement.value.match(regMonth) && this._elementRef.nativeElement.value.length >= this.oldLength) {

            this._elementRef.nativeElement.value = this._elementRef.nativeElement.value + '.';
            const event = new Event('input');
            this._elementRef.nativeElement.dispatchEvent(event);
            this.regexp();

        } else if (this._elementRef.nativeElement.value
            && this._elementRef.nativeElement.value.length === 6
            && !this._elementRef.nativeElement.value.match(new RegExp(/^(0[1-9]|[12][0-9]|3[01])[- /\.](0[1-9]|1[012])[- /\.]/))) {

            this._elementRef.nativeElement.value = (this._elementRef.nativeElement.value).slice(0, -1);
            const event = new Event('input');
            this._elementRef.nativeElement.dispatchEvent(event);
            this.regexp();

        } else if (this._elementRef.nativeElement.value
            && this._elementRef.nativeElement.value.length > 6 && this._elementRef.nativeElement.value.length < 11
            && !this._elementRef.nativeElement.value.match(regYear)) {

            this._elementRef.nativeElement.value = (this._elementRef.nativeElement.value).slice(0, -1);
            const event = new Event('input');
            this._elementRef.nativeElement.dispatchEvent(event);
            this.regexp();
        } else if (this._elementRef.nativeElement.value
            && this._elementRef.nativeElement.value.length > 10) {

            this._elementRef.nativeElement.value = (this._elementRef.nativeElement.value).slice(0, -1);
            const event = new Event('input');
            this._elementRef.nativeElement.dispatchEvent(event);
            this.regexp();
        }
        this.setOld();
    }
}
