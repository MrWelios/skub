import {
    Directive,
    ElementRef,
    OnInit,
    OnDestroy, Input,
} from '@angular/core';

@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[vce-tooltip]',
    // tslint:disable-next-line:use-host-property-decorator
    host: {
        '(mouseenter)': 'onMouseEnter($event)',
        '(mouseleave)': 'onMouseLeave($event)',
    },
})

export class TooltiplDirective implements OnInit, OnDestroy {
    @Input() title = '';
    @Input() isEnabled = false;

    constructor() {
    }

    onMouseEnter(e: MouseEvent): void {
        console.log('MOUSE ENTER');
    }

    onMouseLeave(e: MouseEvent): void {
        console.log('MOUSE LEAVE');
    }

    ngOnInit() {
    }

    ngOnDestroy() {
    }
}
