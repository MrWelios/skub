import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VCECardMaskDirective } from './component';

@NgModule({
    imports: [CommonModule],
    declarations: [VCECardMaskDirective],
    exports: [VCECardMaskDirective],
})

export class CardMaskModule {
}
