import { Directive, Input, ElementRef, Renderer, OnInit, Output, EventEmitter } from '@angular/core';

@Directive({
    selector: '[appCardMask]',
    // tslint:disable-next-line:use-host-property-decorator
    host: {
        '(input)': 'onChange($event)',
    },
})
export class VCECardMaskDirective implements OnInit {
    @Input() changedValue;
    @Output() changedValueChange = new EventEmitter<number>();

    private vall;
    private oldVal;

    constructor(private _elementRef: ElementRef) { }

    ngOnInit() {
        this.oldVal = this.changedValue;
        this.setVall(this.changedValue);
    }

    onChange(event: Event) {
        this.vall = this.deleteWhiteSpace();
        this.regexp();
    }
    regexp() {
        const regex = new RegExp('([0-9]{0,16})$');
        if (this.vall && !this.vall.match(regex)) {
            this.oldVal = this.vall;
            this.vall = (this.vall).slice(0, -1);
            this.regexp();
        } else if (this.vall !== this.oldVal) {
            this.oldVal = this.vall;
            this.changedValueChange.emit(this.vall);
            this.setVall(this.vall);
        }

    }

    deleteWhiteSpace() {
        return this._elementRef.nativeElement.value.replace(/\s+/g, '');
    }

    setVall(params) {
        let vall = params;
        vall = this.setWhiteSpace(params);
        this._elementRef.nativeElement.value = vall;
        const event = new Event('input');
        this._elementRef.nativeElement.dispatchEvent(event);
    }

    setWhiteSpace(vall) {
        if (vall) {
            return vall.toString().replace(/\B(?=(\d{4})+(?!\d))/g, ' ');
        }
        return vall;
    }
}
