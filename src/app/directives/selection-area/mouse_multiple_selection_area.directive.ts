import {
    Directive,
    ElementRef,
    Renderer2,
    OnInit,
} from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/distinctUntilChanged';

export interface MouseAreaCoordsType {
    x0: number;
    x1: number;
    y0: number;
    y1: number;
}

const COORDS_INITIAL: MouseAreaCoordsType = { x0: 0, x1: 0, y0: 0, y1: 0 };

@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[mouse-multiple-selection-area]',
    // tslint:disable-next-line:use-host-property-decorator
    host: {
        '(document:mouseup)': 'onMouseUp()',
        '(document:mousemove)': 'onMouseMove($event)',
        '(mousedown)': 'onMouseDown($event)',
    },
})

export class MouseMultipleSelectionAreaDirective implements OnInit {
    mouseAreaCoordsStream$: BehaviorSubject<MouseAreaCoordsType>;
    mouseAreaSelectableItemIsInit$: BehaviorSubject<boolean>;
    private isActive = false;
    private isInit = false;
    private isOnMove = false;

    constructor(
        private renderer: Renderer2,
        private mouseSelectedArea: ElementRef,
    ) { }

    ngOnInit() {
        this.mouseAreaCoordsStream$ = new BehaviorSubject(COORDS_INITIAL);
        this.mouseAreaSelectableItemIsInit$ = new BehaviorSubject(false);
        this.mouseAreaSelectableItemIsInit$
            .distinctUntilChanged()
            .subscribe((selectableItemActivated) => {
                if (selectableItemActivated) {
                    this.mouseSelectedArea = this.renderer.createElement('div');
                    this.renderer.addClass(this.mouseSelectedArea, 'mouse-selected-area');
                    this.renderer.appendChild(window.document.body, this.mouseSelectedArea);
                    this.deactivateSelectedArea();
                    this.mouseAreaCoordsStream$.subscribe((coords: MouseAreaCoordsType) => {
                        this.redrawMouseArea(coords);
                    });
                    this.isInit = true;
                }
            });
    }

    onMouseUp(): void {
        this.isOnMove = false;
        if (!this.isActive || !this.isInit) {
            return;
        }
        this.deactivateSelectedArea();
        this.mouseAreaCoordsStream$.next(COORDS_INITIAL);
    }

    onMouseDown(e: MouseEvent) {
        if (!this.isInit) {
            return;
        }
        this.mouseAreaCoordsStream$.next({
            x0: e.pageX,
            x1: e.pageX,
            y0: e.pageY,
            y1: e.pageY,
        });
        this.activateSelectedArea();
    }

    onMouseMove(e: MouseEvent): void {
        this.isOnMove = true;
        if (!this.isActive || !this.isInit) {
            return;
        }
        const coords = this.mouseAreaCoordsStream$.getValue();
        this.mouseAreaCoordsStream$.next({ ...coords, x1: e.pageX, y1: e.pageY });
    }

    private activateSelectedArea(): void {
        this.isActive = true;
        this.renderer.removeClass(this.mouseSelectedArea, 'mouse-selected-area_hidden');
    }

    private deactivateSelectedArea(): void {
        this.isActive = false;
        this.renderer.addClass(this.mouseSelectedArea, 'mouse-selected-area_hidden');
    }

    private redrawMouseArea(coords: MouseAreaCoordsType): void {
        const { x0, x1, y0, y1 } = coords;
        const top = Math.min(y0, y1);
        const left = Math.min(x0, x1);
        const width = Math.min(Math.abs(x1 - x0), window.document.body.scrollWidth - left);
        const height = Math.min(Math.abs(y1 - y0), window.document.body.scrollHeight - top);

        this.renderer.setStyle(this.mouseSelectedArea, 'top', `${top}px`);
        this.renderer.setStyle(this.mouseSelectedArea, 'left', `${left}px`);
        this.renderer.setStyle(this.mouseSelectedArea, 'width', `${width}px`);
        this.renderer.setStyle(this.mouseSelectedArea, 'height', `${height}px`);
    }

}
