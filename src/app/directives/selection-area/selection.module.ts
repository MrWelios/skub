import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MouseMultipleSelectionAreaDirective } from './mouse_multiple_selection_area.directive';
import { SelectableItemDirective } from './selectable_item.directive';

@NgModule({
    imports: [CommonModule],
    declarations: [MouseMultipleSelectionAreaDirective, SelectableItemDirective],
    exports: [MouseMultipleSelectionAreaDirective, SelectableItemDirective],
})

export class SelectionModule {

}
