import { Directive, ElementRef, OnInit, OnDestroy } from '@angular/core';
import { MouseMultipleSelectionAreaDirective, MouseAreaCoordsType } from './mouse_multiple_selection_area.directive';

@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[mouse-multiple-selection-item]',
})

export class SelectableItemDirective implements OnInit, OnDestroy {
    constructor(
        private mouseArea: MouseMultipleSelectionAreaDirective,
        private selectableItem: ElementRef,
    ) { }

    ngOnInit() {
        const el = this.selectableItem.nativeElement.querySelector('input[type="checkbox"]');
        this.mouseArea.mouseAreaCoordsStream$.subscribe((mouseAreaCoords: MouseAreaCoordsType) => {
            if (this.checkSelectionItemHitting(mouseAreaCoords)) {
                el.setAttribute('checked', true);
            }
        });

        this.mouseArea.mouseAreaSelectableItemIsInit$.next(true);
    }

    ngOnDestroy() {
        // this.mouseArea.mouseAreaCoordsStream$.unsubscribe();
    }

    private checkSelectionItemHitting(mouseAreaCoords: MouseAreaCoordsType) {
        const { top, right, bottom, left } = this.selectableItem.nativeElement.getBoundingClientRect();
        const mouseAreaTop = Math.min(mouseAreaCoords.y0, mouseAreaCoords.y1);
        const mouseAreaRight = Math.max(mouseAreaCoords.x0, mouseAreaCoords.x1);
        const mouseAreaBottom = Math.max(mouseAreaCoords.y0, mouseAreaCoords.y1);
        const mouseAreaLeft = Math.min(mouseAreaCoords.x0, mouseAreaCoords.x1);
        const conditionX = mouseAreaLeft <= left && left <= right || left <= mouseAreaLeft && mouseAreaLeft <= right;
        const conditionY = mouseAreaTop <= top && top <= mouseAreaBottom || top <= mouseAreaTop && mouseAreaLeft <= bottom;

        return conditionX && conditionY;
    }

}
