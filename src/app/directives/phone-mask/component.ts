import { Directive, Input, ElementRef, Renderer, OnInit, Output, EventEmitter } from '@angular/core';

@Directive({
    selector: '[appPhoneMask]',
    // tslint:disable-next-line:use-host-property-decorator
    host: {
        '(input)': 'onChange($event)',
    },
})
export class VCEPhoneMaskDirective implements OnInit {
    @Input() set appPhoneMask(val: number) {
        this.count = val;
    }
    @Input() changedValue;

    @Output() changedValueChange = new EventEmitter<number>();

    private count: number;
    private vall;
    private oldVal;

    constructor(private _elementRef: ElementRef) { }

    ngOnInit() {
        this.oldVal = this.changedValue;
        this.setVall(this.changedValue);
    }

    onChange(event: Event) {
        this.vall = this.deleteWhiteSpace();
        this.regexp();
    }
    regexp() {
        const regex = new RegExp('^[+]?' + '([0-9]{0,' + (this.count) + '})$');
        if (this.vall && !this.vall.match(regex)) {
            this.oldVal = this.vall;
            this.vall = (this.vall).slice(0, -1);
            this.regexp();
        } else if (this.vall !== this.oldVal) {
            this.oldVal = this.vall;
            this.changedValueChange.emit(this.vall);
            this.setVall(this.vall);
        }

    }

    deleteWhiteSpace() {
        return this._elementRef.nativeElement.value.replace(/\s+/g, '');
    }

    setVall(params) {
        this._elementRef.nativeElement.value = params;
        const event = new Event('input');
        this._elementRef.nativeElement.dispatchEvent(event);
    }

}
