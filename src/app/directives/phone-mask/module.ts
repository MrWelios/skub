import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VCEPhoneMaskDirective } from './component';

@NgModule({
    imports: [CommonModule],
    declarations: [VCEPhoneMaskDirective],
    exports: [VCEPhoneMaskDirective],
})

export class PhoneMaskModule {
}
