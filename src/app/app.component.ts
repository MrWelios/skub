import { Component, OnInit, OnDestroy } from '@angular/core';
import { mergeEffects } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { MediaObserver } from '@angular/flex-layout';
import { Router, NavigationEnd } from '@angular/router';
import { ISubscription } from 'rxjs/Subscription';
import get from 'lodash/get';
import cloneDeep from 'lodash/cloneDeep';

import { environment } from 'environments/environment';
import { UIService } from 'app/services/UIService';
import { changeMediaScreen, getModulesSchema, setModulesSchema, setLang, setLocales, getProfileData, getSystemLabelLink } from './actions';
import { DEFAULT_LANG, DEFAULT_LOCALES, SCREEN_MEDIA } from './constants';
import { AppStateInterface, commonReducers } from './store';
import { WebSocketService } from './services/WebSocketService';
import { GlobalEffects } from './$effects';
import { LS_SESSIONID } from './services/constants/CustomHttpService.constants';
import { Storages } from './utilites/Storages';

@Component({
    selector: 'vce-root',
    styles: [`
    .content {
        height: calc(100vh - 60px);
        margin-top: 60px;
    }
    `],
    template: `
    <vce-navbar></vce-navbar>
    <div class="layout">
        <div class="content content__login">
            <vce-loader></vce-loader>
            <vce-notifications></vce-notifications>
            <vce-sidebar>
                <router-outlet></router-outlet>
            </vce-sidebar>
        </div>
    </div>`,
    providers: [GlobalEffects],
})

export class AppComponent implements OnInit, OnDestroy {
    changeOriental = true;
    navbarState = false;
    userRole: string;
    lang: string;

    private navbarStateSubscription: ISubscription;
    private effectsSubscription: ISubscription;
    private userRoleSubscription: ISubscription;
    private userProfileSubscription: ISubscription;

    constructor(
        private uiService: UIService,
        private store$: Store<AppStateInterface>,
        private effects$: GlobalEffects,
        private translate: TranslateService,
        private media: MediaObserver,
        private router: Router,
        private webSocketService: WebSocketService,
    ) {}

    ngOnInit(): void {
        this.effectsSubscription = mergeEffects(this.effects$).subscribe();
        this.router.events.subscribe((evt) => {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            window.scrollTo(0, 0);
        });
        this.webSocketService.start(get(environment, 'api.ws', ''));

        this.uiService.getLangState().subscribe((lang) => {
            if (lang) {
                this.lang = lang;
                Storages.setCurrentLang(lang);
                this.translate.use(lang);
            }
        });

        this.media.media$.subscribe(change => this.updateGrid());
        Object.keys(commonReducers).forEach((key) => {
            this.store$.addReducer(key, commonReducers[key]);
        });
        this.navbarStateSubscription = this.uiService.getIsLogginedState().subscribe((state) => {
            this.navbarState = cloneDeep(state);
        });
        this.store$.select(state => state.userRole).subscribe((role) => {
            this.userRole = cloneDeep(role);
        });

        const storagesModuleSchema = Storages.getModulesSchema();

        if (Storages.getSessionItem(LS_SESSIONID) && !storagesModuleSchema) {
            this.store$.dispatch(getModulesSchema());
        } else if (storagesModuleSchema) {
            this.store$.dispatch(setModulesSchema(storagesModuleSchema));
        }

        if (Storages.getSessionItem(LS_SESSIONID)) {
            this.store$.dispatch(getProfileData());
            this.store$.dispatch(getSystemLabelLink('ichain_language'));
            this.userProfileSubscription = this.store$.select(state => ({ profileData: state.profileData, systemLabelLink: state.systemLabelLink })).subscribe((data) => {
                if (data.profileData && data.systemLabelLink && data.systemLabelLink.ichain_language) {
                    const selectedLang = get(data, 'systemLabelLink.ichain_language.data', []).
                        filter(lang => lang.id === get(data, 'profileData.data.main.fields.language_id.value', null))[0];

                    if (selectedLang && selectedLang.shortName.toLowerCase() !== this.lang) {
                        this.store$.dispatch(setLang(selectedLang.shortName.toLowerCase()));
                    }
                }
            });
        }

        const currentLang = Storages.getCurrentLang();
        this.store$.dispatch(setLang(currentLang ? currentLang : DEFAULT_LANG));
        const locales = Storages.getLocales();
        this.store$.dispatch(setLocales(locales ? locales : DEFAULT_LOCALES));
    }

    ngOnDestroy() {
        this.navbarStateSubscription.unsubscribe();
        this.effectsSubscription.unsubscribe();
        this.userRoleSubscription.unsubscribe();
    }

    updateGrid(): void {
        SCREEN_MEDIA.forEach((screen) => {
            if (this.media.isActive(screen)) {
                this.store$.dispatch(changeMediaScreen(screen));
            }
        });
    }
}
