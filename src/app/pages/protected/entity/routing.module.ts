import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CanDeactivateGuard } from 'app/guards/CanDeactivateGuard';
import { ListComponent } from './components/list/list.component';
import { ViewComponent } from './components/view/view.component';
import { EditComponent } from './components/edit/edit.component';
// import { CanDeactivateGuard } from 'app/guards/CanDeactivateGuard';
// import { UpdateStatusComponent } from './components/updateStatus/updateStatus.component';

const routes: Routes = [
    { path: '', redirectTo: 'list' },
    { path: 'list', component: ListComponent, data: { breadcrumb: 'List' } },
    { path: 'view/:id', component: ViewComponent, data: { breadcrumb: 'View' } },
    { path: 'edit/:id', component: EditComponent, data: { breadcrumb: 'Edit' }, canDeactivate: [CanDeactivateGuard] },
    { path: 'create', component: EditComponent, data: { breadcrumb: 'Create' }, canDeactivate: [CanDeactivateGuard] },
    // { path: 'updateStatus', component: UpdateStatusComponent, data: { breadcrumb: 'Update Status' } },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})

export class RoutingModule { }
