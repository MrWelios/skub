import { AppStateInterface } from 'app/store';

export const GET_DATA = 'GET_DATA';
export const GET_DATA_FINISHED_WITH_SUCCESS = 'GET_DATA_FINISHED_WITH_SUCCESS';
export const GET_PROCESS_DEFINITION_FINISHED_WITH_SUCCESS = 'GET_PROCESS_DEFINITION_FINISHED_WITH_SUCCESS';
export const APPROVE = 'APPROVE';
export const REQUEST_BUTTON = 'REQUEST_BUTTON';
export const DOWNLOAD_BUTTON = 'DOWNLOAD_BUTTON';
export const CLEAR_DATA = 'CLEAR_DATA';

export interface DataInterface {
    id: number;
    linked: any[];
    tasks: any[];
    actions: any[];
    priceList: any[];
    entityName: {
        name: string;
        img:  string;
    };
}

export interface DataStateInterface extends AppStateInterface {
    data: DataInterface;
    linkedList: any;
    tasksList: any;
    changelog: any;
    comments: any;
}
