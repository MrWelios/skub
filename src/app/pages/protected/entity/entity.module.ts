import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import {
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatSelectModule,
    MatDividerModule,
    MatIconModule,
    MatListModule,
    MatTabsModule,
    MatTableModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    MatMenuModule,
} from '@angular/material';

import { RoutingModule } from './routing.module';
import { ListComponent } from './components/list/list.component';
import { TableModule } from 'app/widgets/table/module';
import { MaskModule } from 'app/directives/number-mask/module';
import { ModalModule } from 'app/widgets/modal/module';
import { InputFileModule } from 'app/widgets/inputFile/module';
import { CanDeactivateGuard } from 'app/guards/CanDeactivateGuard';
import { VCEBlockModule } from 'app/widgets/block/module';
import { VCEActionsModule } from 'app/widgets/actions/module';
import { VCEViewBPModule } from 'app/widgets/viewBP/viewBP.module';
import { ViewComponent } from './components/view/view.component';
import { ToArrayModule } from 'app/directives/toArray.pipe';
import { DateTaskGenModule } from 'app/directives/dateTaskGen.pipe';
import { BPComponent } from './components/BP/BP.component';
import { EditComponent } from './components/edit/edit.component';
import { BreadcrumbsModule } from 'app/widgets/breadcrumbs/module';
import { TablePartModule } from 'app/widgets/tablePart/module';
import { ModalV1Module } from 'app/widgets/modal_v1/module';
import { ModalV2Module } from 'app/widgets/modal_v2/module';
import { CommentsComponent } from './components/comments/comments.component';
import { ChangelogComponent } from './components/changelog/changelog.component';
import { TaskComponent } from './components/task/task.component';
import { VCEFiltersModule } from 'app/widgets/filters/module';
import { LinkedListComponent } from './components/linkedList/linkedList.component';
// import { UpdateStatusComponent } from './components/updateStatus/updateStatus.component';

@NgModule({
    imports: [
        BreadcrumbsModule,
        ToArrayModule,
        DateTaskGenModule,
        MaskModule,
        MatCardModule,
        MatButtonModule,
        MatInputModule,
        MatSelectModule,
        MatDividerModule,
        MatIconModule,
        MatListModule,
        MatTabsModule,
        MatTableModule,
        MatTooltipModule,
        MatPaginatorModule,
        MatExpansionModule,
        MatProgressSpinnerModule,
        MatMenuModule,
        ModalModule,
        TableModule,
        CommonModule,
        FormsModule,
        RoutingModule,
        ModalModule,
        InputFileModule,
        VCEBlockModule,
        VCEActionsModule,
        VCEViewBPModule,
        TablePartModule,
        TranslateModule,
        ModalV1Module,
        ModalV2Module,
        VCEFiltersModule,
    ],
    providers: [
        CanDeactivateGuard,
    ],
    declarations: [
        ListComponent,
        ViewComponent,
        EditComponent,
        BPComponent,
        CommentsComponent,
        ChangelogComponent,
        TaskComponent,
        LinkedListComponent,
        // UpdateStatusComponent,
    ],
})

export class EntityModule {}
