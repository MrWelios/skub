import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { ISubscription } from 'rxjs/Subscription';
import { switchMap, catchError } from 'rxjs/operators';
import { mergeEffects } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import get from 'lodash/get';

import { getProcessDefinitionAction } from './actions';
import { BPStateInterface } from './constants';
import { BPEffects } from './$effects';
import { viewBPData } from '../../redusers';
import { Storages } from 'app/utilites/Storages';
import { BPService } from 'app/services/BPService';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { addNotification } from 'app/actions';
import { NOTIFICATION_TYPE_ERROR, NOTIFICATION_TYPE_SUCCESS } from 'app/constants';

@Component({
    selector: 'vce-bp',
    styleUrls: ['./BP.style.scss'],
    templateUrl: './BP.template.html',
    providers: [
        BPEffects,
    ],
})

export class BPComponent implements OnInit, OnDestroy {
    @Input() BPRequestData: any;
    @Input() isEdit: boolean;
    @Input() entityID;
    @Output() onClose = new EventEmitter<void>();

    bpmData: any;

    private effectsSubscription: ISubscription;

    constructor(
        private store$: Store<BPStateInterface>,
        private bpService: BPService,
        private effects$: BPEffects,
        private translate: TranslateService,
    ) {}

    ngOnInit() {
        this.store$.addReducer('viewBPData', viewBPData);

        this.effectsSubscription = mergeEffects(this.effects$).subscribe();
        this.store$.dispatch(getProcessDefinitionAction({
            ...this.BPRequestData,
            isEdit: this.isEdit,
        }));
        this.store$.select(state => state.viewBPData)
            .filter(data => data !== undefined)
            .first()
            .subscribe(data => this.bpmData = data);
            
    }

    ngOnDestroy() {
        this.effectsSubscription.unsubscribe();
        this.store$.removeReducer('viewBPData');
    }

    onUpdate(event) {

        this.bpService.updateBP({ bpmn20Xml: event }, this.entityID).pipe(
            switchMap((response: any[]) => {
                this.translate.get('SUCCESS').subscribe((value) => {
                    this.store$.dispatch(addNotification(NOTIFICATION_TYPE_SUCCESS, value));
                });
                window.location.reload();
                this.onClose.emit();
                return  Observable.of({});
            }),
            catchError((error) => {
                this.translate.get(get(error, 'error.error', 'ERROR')).subscribe((value) => {
                    this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                });
                return Observable.of({});
            }),
        ).subscribe();
    }

    close() {
        this.onClose.emit();
    }

}
