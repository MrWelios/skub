import { DataStateInterface } from '../../constants';

export const GET_PROCESS_DEFINITION = 'GET_PROCESS_DEFINITION';

export interface ViewBPDataInterface {
    id: string;
    bpmn20Xml: string;
    activityId: string[];
}

export interface BPStateInterface extends DataStateInterface {
    viewBPData: ViewBPDataInterface;
}
