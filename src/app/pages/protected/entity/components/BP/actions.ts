import { ActionWithPayloadInterface } from 'app/reducers';
import { GET_PROCESS_DEFINITION } from './constants';
import { GET_PROCESS_DEFINITION_FINISHED_WITH_SUCCESS } from '../../constants';

export function getProcessDefinitionAction(data): ActionWithPayloadInterface {
    return {
        type: GET_PROCESS_DEFINITION,
        payload: data,
    };
}

export function getProcessDefinitionActionFinishedWitchSuccess(data): ActionWithPayloadInterface {
    return {
        type: GET_PROCESS_DEFINITION_FINISHED_WITH_SUCCESS,
        payload: {
            data,
        },
    };
}
