import { Injectable } from '@angular/core';
import { debounceTime, tap, map, switchMap, catchError } from 'rxjs/operators';
import { Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import get from 'lodash/get';

import { EntityGlobalEffects } from '../../$effects';
import { GET_PROCESS_DEFINITION } from './constants';
import { setAppLoadingState, addNotification } from 'app/actions';
import { NOTIFICATION_TYPE_ERROR } from 'app/constants';
import { getProcessDefinitionActionFinishedWitchSuccess } from './actions';
import { ActionWithPayloadInterface } from 'app/reducers';

@Injectable()
export class BPEffects extends EntityGlobalEffects {

    @Effect() getProcessDefinition = this.actions$.pipe(
        ofType(GET_PROCESS_DEFINITION),
        debounceTime(this.loadingDebounce),
        tap(() => this.store$.dispatch(setAppLoadingState(true))),
    ).distinctUntilChanged().pipe(
        switchMap((action: ActionWithPayloadInterface) => {
            const { definition, currentActivityId, isEdit } = action.payload;
            return this.bpService.getViewBP(definition, currentActivityId, isEdit).pipe(
                switchMap((response: any[]) => {
                    const res = {};
                    response.forEach((element) => {
                        Object.assign(res, element);
                    });
                    this.store$.dispatch(getProcessDefinitionActionFinishedWitchSuccess(res));
                    return  Observable.of({});
                }),
                catchError((error) => {
                    this.translate.get(get(error, 'error.error', 'ERROR')).subscribe((value) => {
                        this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                    });
                    return Observable.of({});
                }),
                tap(() => this.store$.dispatch(setAppLoadingState(false))),
            );
        }),
    );
}
