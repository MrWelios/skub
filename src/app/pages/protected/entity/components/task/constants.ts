
export const GET_TASKS_LIST = 'GET_TASKS_LIST';
export const SET_TASKS_LIST = 'SET_TASKS_LIST';
export const CLEAR_TASKS_LIST = 'CLEAR_TASKS_LIST';

export const TASK_TYPES = {
    newBP: 'from_bp',
    newEvent: 'event',
    newTodo: 'todo',
};
