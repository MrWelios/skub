import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { ISubscription } from 'rxjs/Subscription';
import { mergeEffects } from '@ngrx/effects';
import { TranslateService } from '@ngx-translate/core';
import get from 'lodash/get';
import set from 'lodash/set';

import { clearTasksList, getTasksList } from './actions';
import { DataStateInterface } from '../../constants';
import { UIService } from 'app/services/UIService';
import { UiUtilites } from 'app/utilites/UiUtilites';
import { tasksListReducer } from './reducers';
import { EntityService } from 'app/services/EntityService';
import { requestButtonAction } from '../../actions';
import { TasksEffects } from './$effects';
import { NOTIFICATION_TYPE_ERROR, NOTIFICATION_TYPE_SUCCESS } from 'app/constants';
import { addNotification } from 'app/actions';
import { TASK_TYPES } from './constants';

@Component({
    selector: 'vce-task',
    templateUrl: './task.template.html',
    styleUrls: ['./task.style.scss'],
    providers: [TasksEffects],
})
export class TaskComponent implements OnInit, OnDestroy {

    tasks = {
        todo: [],
        event: [],
        from_bp: [],
        newTodo: {},
        newEvent: {},
    };
    invalidFields = {
        newEvent: {},
        newTodo: {},
    };
    canCreate = {
        newBP: false,
        newEvent: false,
        newTodo: false,
    };
    step = {
        todo: null,
        event: null,
        from_bp: null,
    };
    taskModule: any;
    moduleName: string;
    dataID: string;

    taskModuleSectionsOrder = {};
    taskModuleFieldsData = {};

    private tasksListSubscription: ISubscription;
    private effectsSubscription: ISubscription;

    constructor(
        private store$: Store<DataStateInterface>,
        private uiService: UIService,
        private activatedRoute: ActivatedRoute,
        private entityService: EntityService,
        private effects$: TasksEffects,
        public translate: TranslateService,
    ) {}

    ngOnInit() {
        this.effectsSubscription = mergeEffects(this.effects$).subscribe();
        this.store$.addReducer('tasksList', tasksListReducer);

        this.activatedRoute.params.subscribe((params) => {
            if (params.entity && this.moduleName !== params.entity) {
                this.moduleName = params.entity;
            }
        });

        this.uiService.getStoreModulesSchema().subscribe((modules) => {
            if (modules) {
                this.taskModule = modules.filter(element => element.name === 'task')[0];
                this.tasks.newTodo = this.entityService.processEntitySections(this.taskModule, this.generateNewData('todo'));
                this.tasks.newEvent = this.entityService.processEntitySections(this.taskModule, this.generateNewData('event'));
            }
        });

        this.activatedRoute.params.subscribe((params) => {
            if (params.id) {
                this.dataID = params.id;
                this.clearTasks();
                this.store$.dispatch(clearTasksList());
                this.store$.dispatch(getTasksList(this.dataID, this.moduleName));
            }
        });

        this.tasksListSubscription = this.store$.select(state => state.tasksList).subscribe((tasksList) => {
            if (tasksList) {
                this.clearTasks();
                tasksList.forEach((row) => {
                    this.tasks[get(row, 'data.main.fields.type', 'unknown')].push(row);
                });

                this.sortTasks();

                this.taskModuleSectionsOrder = UiUtilites.sortObject(this.taskModule.data, 'order');
                tasksList.forEach((task) => {
                    this.taskModuleFieldsData[task.id] = this.entityService.processEntitySections(this.taskModule, task);
                });
            }
        });

        this.store$.dispatch(getTasksList(this.dataID, this.moduleName));
    }

    sortTasks() {
        Object.keys(TASK_TYPES).map(key => TASK_TYPES[key]).forEach((key) => {
            let openedTask = false;
            this.tasks[key].sort((a, b) => {
                const aType = get(a, 'data.main.fields.status', '');
                const bType = get(b, 'data.main.fields.status', '');
                if (aType !== 'closed' || bType !== 'closed') {
                    openedTask = true;
                }
                if (aType === 'closed' && bType !== 'closed') {
                    return 1;
                }
                if (aType !== 'closed' && bType === 'closed') {
                    return -1;
                }
                return 0;
            });

            if (openedTask) {
                this.step[key] = 0;
            }
        });
    }

    clearTasks() {
        this.tasks = {
            todo: [],
            event: [],
            from_bp: [],
            newTodo: this.entityService.processEntitySections(this.taskModule, this.generateNewData('todo')),
            newEvent: this.entityService.processEntitySections(this.taskModule, this.generateNewData('event')),
        };
    }

    ngOnDestroy() {
        this.tasksListSubscription.unsubscribe();
        this.effectsSubscription.unsubscribe();
        this.store$.removeReducer('tasksList');
    }

    generateNewData(type) {
        const newData = {
            data: {
            },
        };
        Object.keys(this.taskModule.data).forEach((key) => {
            newData.data[key] = { fields: {} };
            Object.keys(this.taskModule.data[key].fields).forEach((field) => {
                if (this.taskModule.data[key].fields[field].display_type === 'section') {
                    newData.data[key].fields[field] = { fields: {} };
                    Object.keys(this.taskModule.data[key].fields[field].fields).forEach((subField) => {
                        if (this.taskModule.data[key].fields[field].fields[subField].create) {
                            newData.data[key].fields[field].fields[subField] = null;
                        }
                    });
                } else {
                    if (this.taskModule.data[key].fields[field].create) {
                        newData.data[key].fields[field] = null;
                    }
                }
            });
        });

        set(newData, 'data.main.fields.type', type);

        return newData;
    }

    sendErrorMessage(obj, type) {
        if (!obj.event) {
            delete this.invalidFields[type][obj.fieldName];
        } else {
            this.invalidFields[type][obj.fieldName] = obj.event;
        }

        this.canCreate[type] = Object.keys(this.invalidFields[type]).length === 0;
    }

    action(item) {
        switch (item.value.type) {
            case 'formActions':
            case 'noFormActions':
                if (item.taskId) {
                    item.value.data = { data: this.entityService.generateSaveObject(this.taskModuleFieldsData[item.taskId]) };
                }
                this.store$.dispatch(requestButtonAction(item.value));
                break;
        }
    }

    create(key) {
        const sendingObj = this.entityService.generateSaveObject(this.tasks[key]);

        sendingObj.main.fields.type = TASK_TYPES[key];
        sendingObj.main.fields.schemaName = this.moduleName;
        sendingObj.main.fields.entityId = this.dataID;

        this.entityService.createTask(sendingObj).subscribe(
            (_) => {
                this.translate.get('SUCCESS').subscribe((value) => {
                    this.store$.dispatch(addNotification(NOTIFICATION_TYPE_SUCCESS, value));
                });
                this.store$.dispatch(getTasksList(this.dataID, this.moduleName));
                // const data = this.generateNewData();
                // this.tasks[key] = this.entityService.processEntitySections(this.taskModule, data);
            }, 
            (error) => {
                this.translate.get(get(error, 'error.error', 'ERROR')).subscribe((value) => {
                    this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                });
            },
        );
    }

    isEditTask(task: any): boolean {
        const status = task.data.main.fields.status;
        return status === 'open' || status === 'processing';
    }

    setStep(key, index) {
        this.step[key] = index;
    }

}
