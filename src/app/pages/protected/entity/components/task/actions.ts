import { ActionWithPayloadInterface } from 'app/reducers';
import { GET_TASKS_LIST, CLEAR_TASKS_LIST, SET_TASKS_LIST } from './constants';

export function getTasksList(id, module): ActionWithPayloadInterface {
    return {
        type: GET_TASKS_LIST,
        payload: {
            id,
            module,
        },
    };
}

export function clearTasksList(): ActionWithPayloadInterface {
    return {
        type: CLEAR_TASKS_LIST,
        payload: {
        },
    };
}

export function getTasksListFinishedWithSuccess(data: any, module): ActionWithPayloadInterface {
    return {
        type: SET_TASKS_LIST,
        payload: {
            data,
            module,
        },
    };
}
