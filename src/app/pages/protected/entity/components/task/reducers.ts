import { ActionWithPayloadInterface } from 'app/reducers';
import { SET_TASKS_LIST, CLEAR_TASKS_LIST } from './constants';

export const tasksListReducer = (state: any = [], action: ActionWithPayloadInterface) => {
    if (action.type === SET_TASKS_LIST) {
        return action.payload.data;
    }
    if (action.type === CLEAR_TASKS_LIST) {
        return [];
    }
    return state;
};
