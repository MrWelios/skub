import { Injectable } from '@angular/core';
import { Effect, ofType } from '@ngrx/effects';
import 'rxjs/add/observable/of';
import { debounceTime, tap, switchMap, catchError, concatMap } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import get from 'lodash/get';

import { EntityGlobalEffects } from '../../$effects';
import { setAppLoadingState, addNotification } from 'app/actions';
import { NOTIFICATION_TYPE_ERROR } from 'app/constants';
import { ActionWithPayloadInterface } from 'app/reducers';
import { GET_TASKS_LIST } from './constants';
import { getTasksListFinishedWithSuccess } from './actions';

@Injectable()
export class TasksEffects extends EntityGlobalEffects {

    @Effect({ dispatch: false }) requestGetTasksList = this.actions$.pipe(
        ofType(GET_TASKS_LIST),
        tap(() => this.store$.dispatch(setAppLoadingState(true))),
    ).distinctUntilChanged().pipe(
        concatMap((action: ActionWithPayloadInterface) => {
            const { id, module } = action.payload;
            this.entityService.moduleName = get(this.activatedRoute, 'snapshot.params.entity', null);
            return this.entityService.getTasksList(id, module).pipe(
                concatMap((response: any) => {
                    const data = response.tasks;
                    this.store$.dispatch(getTasksListFinishedWithSuccess(data, module));
                    return  Observable.of({});
                }),
                catchError((error) => {
                    this.translate.get(get(error, 'error.error', 'ERROR')).subscribe((value) => {
                        this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                    });
                    return Observable.of({});
                }),
                tap(() => this.store$.dispatch(setAppLoadingState(false))),
            );
        }),
    );

}
