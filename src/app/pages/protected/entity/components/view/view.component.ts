import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { mergeEffects } from '@ngrx/effects';
import { ISubscription } from 'rxjs/Subscription';
import { Location } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import cloneDeep from 'lodash/cloneDeep';
import set from 'lodash/set';

import { EntityViewEffects } from './$effects';
import {
    getData,
    requestButtonAction,
    clearData,
    downloadButtonAction,
    getDataFinishedWithSuccess,
} from '../../actions';
import { DataStateInterface } from '../../constants';
import { dataReducer } from '../../redusers';
import { UIService } from 'app/services/UIService';
import { EntityService } from 'app/services/EntityService';
import { UiUtilites } from 'app/utilites/UiUtilites';
import { ModuleSchemaInterface } from 'app/constants';

@Component({
    templateUrl: './view.template.html',
    styleUrls: [
        './view.style.scss',
    ],
    providers: [EntityViewEffects],
})

export class ViewComponent implements OnInit, OnDestroy {
    data: any;
    dataSchema: any;
    moduleName: string;
    fieldsData = {};
    dataID: string;
    isOpenViewBP: boolean;
    isEditBP: boolean;
    isOpenModal: boolean;
    isOpenModalV2: boolean;
    viewBPInputData: any;
    modalData: any;
    modalDataV2: any;
    modulesNames = {};
    Object = Object;

    sectionsOrder = {};

    modules: ModuleSchemaInterface[];

    isLoading$: Observable<boolean>;

    private effectsSubscription: ISubscription;
    private dataSubscription: ISubscription;

    constructor(
        private store$: Store<DataStateInterface>,
        private route: Router,
        private activatedRoute: ActivatedRoute,
        private uiService: UIService,
        private entityService: EntityService,
        private effects$: EntityViewEffects,
        private translate: TranslateService,
        private location: Location,
    ) {}

    ngOnInit() {
        this.initComponent();
        this.uiService.getStoreModulesSchema().subscribe((modules) => {
            this.modules = modules;
            if (modules) {
                modules.forEach((entity) => {
                    this.modulesNames[entity.name] = entity.label;
                });
            }
            this.activatedRoute.params.subscribe((params) => {
                if (params.entity && this.moduleName !== params.entity) {
                    this.removeComponent();
                    this.initComponent();
                    this.moduleName = params.entity;
                    this.dataSchema = null;
                    this.data = null;
                    this.dataSchema = modules.filter(element => element.name === params.entity)[0];
                    this.sectionsOrder = UiUtilites.sortObject(this.dataSchema.data, 'order');
                }
            });
        });

        this.activatedRoute.params.subscribe((params) => {
            if (params.id) {
                this.store$.dispatch(clearData());
                this.dataID = params.id;
                this.store$.dispatch(getData(params.id));
            }
        });

        this.dataSubscription = this.store$.select(state => state.data).subscribe((data) => {
            if (data) {
                this.data = cloneDeep(data);
                this.fieldsData = this.entityService.processEntitySections(this.dataSchema, this.data);
            }
        });
    }

    initComponent() {
        this.store$.addReducer('data', dataReducer);
        this.effectsSubscription = mergeEffects(this.effects$).subscribe();
        this.isLoading$ = this.store$.select(state => state.isLoading);
    }

    removeComponent() {
        this.effectsSubscription.unsubscribe();
        this.store$.dispatch(getDataFinishedWithSuccess(null));
        this.store$.removeReducer('data');
        this.viewBPInputData = null;
    }

    gotoUrl(url) {
        this.route.navigate([url]);
    }

    action(item) {
        switch (item.value.type) {
            case 'download': 
                this.store$.dispatch(downloadButtonAction(item.value));
                break;
            case 'formActions': 
                this.route.navigate([`modules/${this.moduleName}/edit/${this.dataID}`]);
                break;
            case 'viewBP': 
                this.isOpenViewBP = true;
                this.viewBPInputData = item.value.data;
                break;
            case 'modal': 
                this.modalData = cloneDeep(item.value);
                this.isOpenModal = true;
                break;
            case 'modal_v2': 
                this.modalDataV2 = cloneDeep(item.value);
                this.isOpenModalV2 = true;
                break;
            case 'redirectActions':
                this.route.navigate([item.value.url]);
                break;
            case 'editBP':
                this.isEditBP = true;
                this.isOpenViewBP = true;
                this.viewBPInputData = item.value.data;
                break;
        }
    }

    onCloseBp() {
        this.isOpenViewBP = false;
        this.isEditBP = false;
    }

    cancel() {
        this.location.back();
    }

    edit() {
        this.route.navigate(['../../edit', this.dataID], { relativeTo: this.activatedRoute });
    }

    onSaveModal(event) {
        const request = { data: {}, priceList: [] };
        if (event.data) {
            const entityData = cloneDeep(event.data.data);
            Object.keys(entityData).forEach((section) => {
                if (entityData[section].fields) {
                    Object.keys(entityData[section].fields).forEach((field) => {
                        set(request, ['data', section, 'fields', field], entityData[section].fields[field]);
                    });
                }
            });
            request.priceList = event.data.priceList;
        }
        this.store$.dispatch(requestButtonAction({ ...event, ... { data: request } }));
        this.isOpenModal = false;
    }

    onActionModalV2(event) {
        switch (event.action.type) {
            case 'formActions': 
                this.store$.dispatch(requestButtonAction({ ...event.action,  data: event.data }));
                this.isOpenModalV2 = false;
                break;
            case 'noFormActions': 
                this.store$.dispatch(requestButtonAction(event.action));
                this.isOpenModalV2 = false;
                break;
            case 'close': 
                this.isOpenModalV2 = false;
                break;
        }
    }

    ngOnDestroy() {
        this.removeComponent();
        this.dataSubscription.unsubscribe();
    }

}
