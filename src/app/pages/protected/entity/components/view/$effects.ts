import { Injectable } from '@angular/core';
import { Effect } from '@ngrx/effects';

import { EntityGlobalEffects } from '../../$effects';

@Injectable()
export class EntityViewEffects extends EntityGlobalEffects {
    @Effect() requestGetData;

    @Effect() requestButton;
    @Effect() downloadButton;
}
