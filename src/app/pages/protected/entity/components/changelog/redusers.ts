import { ActionWithPayloadInterface } from 'app/reducers';
import { 
    SET_CHANGELOG,
    CLEAR_CHANGELOG,
} from './constants';

export const changelogReducer = (state: any = {}, action: ActionWithPayloadInterface) => {
    if (action.type === SET_CHANGELOG) {
        return action.payload.data;
    }
    if (action.type === CLEAR_CHANGELOG) {
        const refresh = {
            changeLog: [],
            itemsPerPage: 10,
            page: 1,
            total: 0,
        };
        return refresh;
    }
    return state;
};
