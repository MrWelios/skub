import { Injectable } from '@angular/core';
import { Effect, ofType } from '@ngrx/effects';
import 'rxjs/add/observable/of';
import { debounceTime, tap, switchMap, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import get from 'lodash/get';

import { EntityGlobalEffects } from '../../$effects';
import { setAppLoadingState, addNotification } from 'app/actions';
import { NOTIFICATION_TYPE_ERROR } from 'app/constants';
import { GET_CHANGELOG } from './constants';
import { setChangelog } from './actions';
import { ActionWithPayloadInterface } from 'app/reducers';

@Injectable()
export class ChangelogEffects extends EntityGlobalEffects {
    @Effect() requestGetChangelog = this.actions$.pipe(
        ofType(GET_CHANGELOG),
        debounceTime(this.loadingDebounce),
        tap(() => this.store$.dispatch(setAppLoadingState(true))),
    ).distinctUntilChanged().pipe(
        switchMap((action: ActionWithPayloadInterface) => {
            const { id, body } = action.payload;
            this.entityService.moduleName = get(this.activatedRoute, 'snapshot.params.entity', null);
            return this.entityService.getChangelog(id, body).pipe(
                switchMap((response: any) => {
                    this.store$.dispatch(setChangelog(response));
                    return  Observable.of({});
                }),
                catchError((error) => {
                    this.translate.get(get(error, 'error.error', 'ERROR')).subscribe((value) => {
                        this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                    });
                    return Observable.of({});
                }),
                tap(() => this.store$.dispatch(setAppLoadingState(false))),
            );
        }),
    );

}
