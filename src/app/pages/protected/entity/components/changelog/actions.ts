import { ActionWithPayloadInterface } from 'app/reducers';
import { GET_CHANGELOG, SET_CHANGELOG, CLEAR_CHANGELOG } from './constants';
import { BodyGetChange } from 'app/services/constants/EntityService.constants';

export function getChangelog(id: number, body: BodyGetChange): ActionWithPayloadInterface {
    return {
        type: GET_CHANGELOG,
        payload: {
            id,
            body,
        },
    };
}

export function setChangelog(data: any): ActionWithPayloadInterface {
    return {
        type: SET_CHANGELOG,
        payload: {
            data,
        },
    };
}

export function clearChangelog(): ActionWithPayloadInterface {
    return {
        type: CLEAR_CHANGELOG,
        payload: {
        },
    };
}
