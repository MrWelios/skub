import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { ISubscription } from 'rxjs/Subscription';
import { mergeEffects } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';

import { ChangelogRow } from './constants';
import { UIService } from 'app/services/UIService';
import { DataStateInterface } from '../../constants';
import { changelogReducer } from './redusers';
import { ChangelogEffects } from './$effects';
import { getChangelog, clearChangelog } from './actions';

@Component({
    selector: 'vce-changelog',
    templateUrl: './changelog.template.html',
    styleUrls: ['./changelog.style.scss'],
    providers: [ChangelogEffects],
})
export class ChangelogComponent implements OnInit, OnDestroy {

    @Input() moduleName: string;

    entityID: number;
    dataSource: ChangelogRow[];
    step: number;
    page: number;
    total: number;
    itemsPerPage: number;
    private changelogSubscription: ISubscription;
    private effectsSubscription: ISubscription;

    constructor(
        private store$: Store<DataStateInterface>,
        private uiService: UIService,
        private activatedRoute: ActivatedRoute,
        private effects$: ChangelogEffects,
    ) {}

    ngOnInit() { 
        this.effectsSubscription = mergeEffects(this.effects$).subscribe();
        this.activatedRoute.params.subscribe((params) => {
            if (params.entity && this.moduleName !== params.entity) {
                this.store$.dispatch(clearChangelog());
                this.total = 1;
                this.step = undefined;
                this.itemsPerPage = 10;
            }
        });
        this.activatedRoute.params.subscribe((params) => {
            if (params.id) {
                this.entityID = params.id;
                const initStartPagination = { page: 1, itemsPerPage: 10 };
                this.store$.dispatch(getChangelog(+this.entityID, initStartPagination));
            }
        });
        this.store$.addReducer('changelog', changelogReducer);
        this.changelogSubscription = this.store$.select(state => state.changelog).subscribe((data) => {
            if (data) {
                const { changeLog, itemsPerPage, total, page } = data;
                if (changeLog) {
                    this.initData(changeLog.length ? changeLog : []);
                }
                this.total = total;
                this.page = page !== 0 ? page - 1  : page;
                this.itemsPerPage = itemsPerPage;
            }
        });
    }

    initData(changelog: any[]) {
        const rows: ChangelogRow[] = [];
        changelog.forEach((element: any) => {
            rows.push(this.buildRow(element));
        });
        this.dataSource = rows;
    }

    buildRow(elementFromApi: any): ChangelogRow {
        return {
            whoDid: elementFromApi.whoDidName,
            changedOn: this.uiService.setDateTimeFormat(new Date(elementFromApi.changedOn)),
            type: elementFromApi.type,
            details: elementFromApi.details.map(row => ({
                ...row,
                ...{ fieldName: `${this.moduleName.toLowerCase()}_${row.fieldName.split('.').join('_')}` },
            })),
        };
    }

    onPageChanged(e: any): void {
        const body = { page: e.pageIndex + 1, itemsPerPage: e.pageSize };
        this.store$.dispatch(getChangelog(+this.entityID, body));
    }

    setStep(step: number) {
        this.step = step;
    } 

    ngOnDestroy() {
        this.effectsSubscription.unsubscribe();
        this.changelogSubscription.unsubscribe();
        this.store$.removeReducer('changelog');
    }
}
