export const GET_CHANGELOG = 'GET_CHANGELOG';
export const SET_CHANGELOG = 'SET_CHANGELOG';
export const CLEAR_CHANGELOG = 'CLEAR_CHANGELOG';

export interface ChangelogRow {
    whoDid: number;
    changedOn: string;
    type: string;
    details: any;
}
