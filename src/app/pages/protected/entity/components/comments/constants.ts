export const GET_COMMENTS = 'GET_COMMENTS';
export const SET_COMMENTS = 'SET_COMMENTS';
export const CLEAR_COMMENTS = 'CLEAR_COMMENTS';

export interface CommentsRow {
    id: number;
    content: string;
    shortContent: string;
    createdBy: string;
    createdImg: string;
    created: string;
    commentData: any;
    commentsList: any[];
    answersCount: number;
}
