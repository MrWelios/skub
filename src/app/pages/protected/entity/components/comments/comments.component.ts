import { Component, ViewChild, OnInit, OnDestroy, QueryList, ElementRef, ViewChildren } from '@angular/core';
import { MatPaginator } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import get from 'lodash/get';
import set from 'lodash/set';
import { mergeEffects } from '@ngrx/effects';
import { merge } from 'rxjs';
import { ISubscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import cloneDeep from 'lodash/cloneDeep';
import { trigger, state, style, transition, animate } from '@angular/animations';

import { CommentsRow } from './constants';
import { EntityService } from 'app/services/EntityService';
import { addNotification } from 'app/actions';
import { NOTIFICATION_TYPE_SUCCESS, NOTIFICATION_TYPE_ERROR } from 'app/constants';
import { DataStateInterface } from '../../constants';
import { UIService } from 'app/services/UIService';
import { commentsReducer } from './redusers';
import { CommentsEffects } from './$effects';
import { getComments, clearComments } from './actions';

@Component({
    selector: 'vce-comments',
    templateUrl: './comments.template.html',
    styleUrls: ['./comments.style.scss'],
    providers: [CommentsEffects],
    animations: [
        trigger('slideInOut', [
            state('in', style({
                height: '*',
            })),
            state('out', style({
                opacity: '0',
                overflow: 'hidden',
                height: '0px',
            })),
            transition('in => out', animate('400ms ease-in-out')),
            transition('out => in', animate('400ms ease-in-out')),
        ]),
    ],
})
export class CommentsComponent implements OnInit, OnDestroy {

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChildren('answerarea') areas: QueryList<ElementRef>;
    entityID: number;
    moduleName: string;
    moduleSchema: any;
    dataSource: CommentsRow[];
    commentData: any;
    page = 1;
    itemsPerPage = 5;
    total: number;
    step: number;
    commentAuthors = [];
    childLoading = false;
    private commentsSubscription: ISubscription;
    private effectsSubscription: ISubscription;
    private paginationSubscription: ISubscription;

    constructor(
        private translate: TranslateService,
        private entityService: EntityService,
        private store$: Store<DataStateInterface>,
        private activatedRoute: ActivatedRoute,
        private uiService: UIService,
        private effects$: CommentsEffects,
    ) {}

    ngOnInit() {
        this.effectsSubscription = mergeEffects(this.effects$).subscribe();
        this.uiService.getStoreModulesSchema().subscribe((modules) => {
            if (modules) {
                this.moduleSchema = modules.filter(element => element.name === 'comment')[0];
                this.commentData = cloneDeep(this.moduleSchema);
                set(this.commentData, ['data', 'main', 'fields', 'content', 'value'], '');
                set(this.commentData, ['data', 'main', 'fields', 'parentCommentId', 'value'], null);
            }
            this.activatedRoute.params.subscribe((params) => {
                if (params.entity && this.moduleName !== params.entity) {
                    this.moduleName = params.entity;
                }
            });
        });

        this.activatedRoute.params.subscribe((params) => {
            if (params.id) {
                this.entityID = params.id;
                this.store$.dispatch(clearComments());
                this.getComments();
            }
        });

        this.store$.addReducer('comments', commentsReducer);
        this.commentsSubscription = this.store$.select(st => st.comments).subscribe((data) => {
            if (data) {
                this.total = data.total;
                this.itemsPerPage = data.itemsPerPage;
                this.page = data.page;
                this.paginator.pageIndex = data.page - 1;
                this.initData(data.comments && data.comments.length ? data.comments : []);
            }
        });

        this.paginationSubscription = merge(this.paginator.page).subscribe(() => {
            this.getComments();
        });
    }        

    initData(comments: any[]) {
        const rows: CommentsRow[] = [];
        comments.forEach((element: any) => {
            const row = this.buildRow(element);
            set(row.commentData, ['data', 'main', 'fields', 'content', 'value'], '');
            set(row.commentData, ['data', 'main', 'fields', 'parentCommentId', 'value'], row.id);
            row.commentData.defaultParentId = row.id;
            rows.push(row);
        });
        this.dataSource = rows;
    }

    buildRow(elementFromApi: any): CommentsRow {
        return {
            id: elementFromApi.id,
            content: elementFromApi.data.content,
            shortContent: elementFromApi.data.shortContent,
            createdBy: elementFromApi.whoDidName,
            createdImg: elementFromApi.whoDidImage,
            created: elementFromApi.data.created,
            commentData: cloneDeep(this.moduleSchema),
            commentsList: null,
            answersCount: elementFromApi.answersCount,
        };
    }

    submitComment(commentData: any) {
        const data = { data: this.entityService.generateSaveObject(commentData.data), priceList: [] };
        set(data.data, ['main', 'fields', 'entityId'], this.entityID);
        set(data.data, ['main', 'fields', 'entityName'], this.moduleName);
        this.entityService.moduleName = 'comment';
        this.entityService.updateData(data).subscribe(
            (response) => {
                this.translate.get('SUCCESS').subscribe((value) => {
                    this.store$.dispatch(addNotification(NOTIFICATION_TYPE_SUCCESS, value));
                });
                if (commentData.defaultParentId) {
                    this.loadChildComments(this.dataSource.filter(element => element.id === commentData.defaultParentId)[0]);
                } else {
                    this.getComments();
                }
                set(commentData.data, ['main', 'fields', 'content', 'value'], '');
                this.cleanAnswer(commentData);
            }, 
            (error) => {
                this.translate.get(get(error, 'error.error', 'ERROR')).subscribe((value) => {
                    this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                });
            });
    }

    prepareAnswer(commentData: any, target: any) {
        set(commentData.data, ['main', 'fields', 'parentCommentId', 'value'], target.id);
        commentData.whom = target.whoDidName;

        const index = this.dataSource.findIndex(element => element.id === commentData.defaultParentId);
        if (index >= 0) {
            const area = this.areas.toArray()[index];
            const htmlArea = area.nativeElement as HTMLElement;
            htmlArea.focus();
            htmlArea.scrollIntoView({ block: 'nearest', behavior: 'smooth' });
        }
    }

    cleanAnswer(commentData: any) {
        set(commentData.data, ['main', 'fields', 'parentCommentId', 'value'], commentData.defaultParentId);
        commentData.whom = null;
    }

    toggleAnswers(comment: any) {
        comment.showAnswers = comment.showAnswers === 'in' ? 'out' : 'in';
    }

    setStep(step: number) {
        this.step = step;
        if (step !== null) {
            const comment = this.dataSource[step];
            if (comment.commentsList === null) {
                this.loadChildComments(comment);
            }
        }
    } 

    ngOnDestroy() {
        this.effectsSubscription.unsubscribe();
        this.store$.removeReducer('comments');
        this.commentsSubscription.unsubscribe();
        this.paginationSubscription.unsubscribe();
    }

    private getComments() {
        const data = {
            entityId: +this.entityID,
            page: this.paginator.pageIndex !== null ? this.paginator.pageIndex + 1 : this.page,
            itemsPerPage: this.paginator.pageSize ? this.paginator.pageSize : this.itemsPerPage,
        };
        this.store$.dispatch(getComments(data));
        this.setStep(null);
    }

    private loadChildComments(comment: any) {
        this.childLoading = true;
        this.entityService.getChildComments(comment.id).subscribe((response) => {
            comment.commentsList = response.comments;
            comment.commentsList.forEach((element) => {
                element.showAnswers = 'out';
                this.commentAuthors[element.id] = element.whoDidName;
                if (element.data.childComments && element.data.childComments.length) {
                    element.data.childComments.forEach((childElement) => {
                        this.commentAuthors[childElement.id] = childElement.whoDidName;
                    });
                }
            });
            comment.answersCount = comment.commentsList.length;
            this.childLoading = false;
        });
    }
}
