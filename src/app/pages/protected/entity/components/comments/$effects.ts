import { Injectable } from '@angular/core';
import { Effect, ofType } from '@ngrx/effects';
import 'rxjs/add/observable/of';
import { debounceTime, tap, switchMap, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import get from 'lodash/get';

import { EntityGlobalEffects } from '../../$effects';
import { setAppLoadingState, addNotification } from 'app/actions';
import { NOTIFICATION_TYPE_ERROR } from 'app/constants';
import { GET_COMMENTS } from './constants';
import { setComments } from './actions';

@Injectable()
export class CommentsEffects extends EntityGlobalEffects {
    @Effect() requestGetComments = this.actions$.pipe(
        ofType(GET_COMMENTS),
        debounceTime(this.loadingDebounce),
        tap(() => this.store$.dispatch(setAppLoadingState(true))),
    ).distinctUntilChanged().pipe(
        switchMap((action: any) => {
            const { data } = action.payload;
            this.entityService.moduleName = get(this.activatedRoute, 'snapshot.params.entity', null);
            return this.entityService.getComments(data).pipe(
                switchMap((response: any) => {
                    this.store$.dispatch(setComments(response));
                    return  Observable.of({});
                }),
                catchError((error) => {
                    this.translate.get(get(error, 'error.error', 'ERROR')).subscribe((value) => {
                        this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                    });
                    return Observable.of({});
                }),
                tap(() => this.store$.dispatch(setAppLoadingState(false))),
            );
        }),
    );

}
