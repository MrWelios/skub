import { ActionWithPayloadInterface } from 'app/reducers';
import { GET_COMMENTS, SET_COMMENTS, CLEAR_COMMENTS } from './constants';

export function getComments(data: any): ActionWithPayloadInterface {
    return {
        type: GET_COMMENTS,
        payload: {
            data,
        },
    };
}

export function setComments(data: any): ActionWithPayloadInterface {
    return {
        type: SET_COMMENTS,
        payload: {
            data,
        },
    };
}

export function clearComments(): ActionWithPayloadInterface {
    return {
        type: CLEAR_COMMENTS,
        payload: {
        },
    };
}