import { ActionWithPayloadInterface } from 'app/reducers';
import { 
    SET_COMMENTS,
    CLEAR_COMMENTS,
} from './constants';

export const commentsReducer = (state: any = {}, action: ActionWithPayloadInterface) => {
    if (action.type === SET_COMMENTS) {
        return action.payload.data;
    }
    if (action.type === CLEAR_COMMENTS) {
        return {};
    }
    return state;
};
