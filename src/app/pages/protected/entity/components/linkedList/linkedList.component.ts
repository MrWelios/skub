import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import { mergeEffects } from '@ngrx/effects';
import { ISubscription } from 'rxjs/Subscription';
import { Router, ActivatedRoute } from '@angular/router';
import cloneDeep from 'lodash/cloneDeep';

import { DataStateInterface } from '../../constants';
import { LinkedListEffects } from './$effects';
import { getLinkedList, clearLinkedList } from './actions';
import { downloadButtonAction, requestButtonAction } from '../../actions';
import { Storages } from 'app/utilites/Storages';
import { ModuleSchemaInterface } from 'app/constants';
import { UiUtilites } from 'app/utilites/UiUtilites';
import { linkedListReducer } from './redusers';

@Component({
    selector: 'vce-linked-list',
    templateUrl: './linkedList.template.html',
    styleUrls: ['./linkedList.style.scss'],
    providers: [LinkedListEffects],
})
export class LinkedListComponent implements OnInit, OnDestroy {

    @Input() linkedData: any;
    @Input() dataID: string;
    @Input() modules: ModuleSchemaInterface[];
    isOpenModal: boolean;
    isOpenModalV2: boolean;
    modalData: any;
    modalDataV2: any;
    linked: any = {};
    Object = Object;

    header = [];
    displayHeader = [];
    filteredRow = [];
    approvedHeader = {};
    labels = {};
    filterData = [];

    private effectsSubscription: ISubscription;
    private linkedListSubscription: ISubscription;

    constructor(
        private translate: TranslateService,
        private store$: Store<DataStateInterface>,
        private activatedRoute: ActivatedRoute,
        private route: Router,
        private effects$: LinkedListEffects,
    ) {}

    ngOnInit() {
        this.effectsSubscription = mergeEffects(this.effects$).subscribe();
        this.store$.addReducer('linkedList', linkedListReducer);

        this.activatedRoute.params.subscribe((params) => {
            if (params.id) {
                this.store$.dispatch(clearLinkedList());
                this.linked = {};
            }
        });

        this.linkedData.entitySchema = this.modules.filter(element => element.name === this.linkedData.entity)[0];

        this.linkedListSubscription = this.store$.select(state => state.linkedList).subscribe((linkedLists) => {
            if (linkedLists && linkedLists[this.linkedData.entitySchema.name]) {
                this.linked = {
                    ...this.linked,
                    data: linkedLists[this.linkedData.entitySchema.name],
                };
            }
        });

        this.linked = {
            ...this.linked,
            ...this.linkedData,
            filters: {
                ...Storages.getQueryList(''),
                related: {
                    key: this.linkedData.field,
                    id: this.dataID,
                },
            },
        };
        this.header = [];
        this.approvedHeader = {};
        this.labels = {};

        const targetFields = UiUtilites.sortArray(this.linkedData.entitySchema.customView.fields, 'order');
        const filterData = cloneDeep(targetFields);
        this.filterData = [];
        filterData.forEach((element) => {
            this.filterData[element.key] = element;
        });
        targetFields.forEach((field) => {
            this.header.push(field.key);
            this.labels[field.key] = field.label;
            this.approvedHeader[field.key] = true;
        });
        this.header.push('actions');
        this.approvedHeader['actions'] = true;
        this.filteredRow = cloneDeep(this.header);
        this.displayHeader = cloneDeep(this.header);
        this.initList();
    }

    rowAction(event, moduleName) {
        switch (event.type) {
            case 'download':
                this.store$.dispatch(downloadButtonAction(event));
                break;
            case 'noFormActions':
                this.store$.dispatch(requestButtonAction(event));
                break;
            case 'formActions':
                this.route.navigate([`modules/${moduleName}/edit/${event.rowID}`]);
                break;
            case 'modal':
                this.modalData = cloneDeep(event);
                this.isOpenModal = true;
                break;
            case 'modal_v2':
                this.modalDataV2 = cloneDeep(event);
                this.isOpenModalV2 = true;
                break;
            case 'redirectActions':
                this.route.navigate([event.url]);
                break;
        }
    }
    
    pageAction(event, moduleName, related) {
        this.store$.dispatch(getLinkedList({ ...event, related } , moduleName));
    }

    ngOnDestroy() {
        this.effectsSubscription.unsubscribe();
        this.store$.removeReducer('linkedList');
        this.linkedListSubscription.unsubscribe();
    }

    private initList() {
        this.store$.dispatch(getLinkedList(this.linked.filters, this.linked.entity));
    }
}
