import { Injectable } from '@angular/core';
import { Effect, ofType } from '@ngrx/effects';
import 'rxjs/add/observable/of';
import { tap, catchError, concatMap } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import get from 'lodash/get';

import { EntityGlobalEffects } from '../../$effects';
import { setAppLoadingState, addNotification } from 'app/actions';
import { NOTIFICATION_TYPE_ERROR } from 'app/constants';
import { GET_LINKED_LIST } from './constants';
import { getLinkedListFinishedWithSuccess } from './actions';
import { ActionWithPayloadInterface } from 'app/reducers';

@Injectable()
export class LinkedListEffects extends EntityGlobalEffects {
    @Effect({ dispatch: false }) requestGetLinkedList = this.actions$.pipe(
        ofType(GET_LINKED_LIST),
        tap(() => this.store$.dispatch(setAppLoadingState(true))),
    ).distinctUntilChanged().pipe(
        concatMap((action: ActionWithPayloadInterface) => {
            const { data, linkedModule } = action.payload;
            this.entityService.moduleName = get(this.activatedRoute, 'snapshot.params.entity', null);
            return this.entityService.getLinkList(data, linkedModule).pipe(
                concatMap((response: any) => {
                    this.store$.dispatch(getLinkedListFinishedWithSuccess(response, linkedModule));
                    return  Observable.of({});
                }),
                catchError((error) => {
                    this.translate.get(get(error, 'error.error', 'ERROR')).subscribe((value) => {
                        this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                    });
                    return Observable.of({});
                }),
                tap(() => this.store$.dispatch(setAppLoadingState(false))),
            );
        }),
    );

}
