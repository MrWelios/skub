import { ActionWithPayloadInterface } from 'app/reducers';
import { 
    SET_LINKED_LIST,
    CLEAR_LINKED_LIST,
} from './constants';

export const linkedListReducer = (state: any = {}, action: ActionWithPayloadInterface) => {
    if (action.type === SET_LINKED_LIST) {
        return {
            ...state,
            [action.payload.linkedModule]: action.payload.data,
        };
    }
    if (action.type === CLEAR_LINKED_LIST) {
        return {};
    }
    return state;
};