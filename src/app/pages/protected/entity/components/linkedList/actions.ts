import { ActionWithPayloadInterface } from 'app/reducers';
import { GET_LINKED_LIST, CLEAR_LINKED_LIST, SET_LINKED_LIST } from './constants';

export function getLinkedList(data: any, linkedModule): ActionWithPayloadInterface {
    return {
        type: GET_LINKED_LIST,
        payload: {
            data,
            linkedModule,
        },
    };
}

export function clearLinkedList(): ActionWithPayloadInterface {
    return {
        type: CLEAR_LINKED_LIST,
        payload: {
        },
    };
}

export function getLinkedListFinishedWithSuccess(data: any, linkedModule): ActionWithPayloadInterface {
    return {
        type: SET_LINKED_LIST,
        payload: {
            data,
            linkedModule,
        },
    };
}