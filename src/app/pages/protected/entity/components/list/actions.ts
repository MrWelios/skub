import { ActionWithPayloadInterface } from 'app/reducers';
import { 
    GET_LIST, 
    DataListRequestInterface, 
    GET_LIST_FINIDHED_WITH_SUCCESS,
    CLEAR_LIST, 
    DataListInterface,
} from './constants';

export function getList(data: DataListRequestInterface): ActionWithPayloadInterface {
    return {
        type: GET_LIST,
        payload: {
            data,
        },
    };
}
export function clearList(): ActionWithPayloadInterface {
    return {
        type: CLEAR_LIST,
    };
}
export function getListFinishedWithSuccess(data: DataListInterface): ActionWithPayloadInterface {
    return {
        type: GET_LIST_FINIDHED_WITH_SUCCESS,
        payload: {
            data,
        },
    };
}
