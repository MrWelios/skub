import { Component, OnInit, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { mergeEffects } from '@ngrx/effects';
import { ISubscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import cloneDeep from 'lodash/cloneDeep';
import set from 'lodash/set';

import { DataListInterface, LIST } from './constants';
import { dataListReducer } from './reducers';
import { getList, clearList } from './actions';
import { ListEffects } from './$effects';
import { Storages } from 'app/utilites/Storages';
import { UIService } from 'app/services/UIService';
import { requestButtonAction, downloadButtonAction } from '../../actions';
import { ModuleSchemaInterface } from 'app/constants';
import { CustomViewService } from 'app/services/CustomViewService';
import { UiUtilites } from 'app/utilites/UiUtilites';

@Component({
    templateUrl: './list.template.html',
    styleUrls: [
        './list.style.scss',
    ],
    providers: [ListEffects],
})

export class ListComponent implements OnInit, OnDestroy {

    data: any = {};
    isLoading$: Observable<boolean>;
    initListFilterData = Storages.getQueryList(LIST);
    moduleName: string;
    moduleSchema: ModuleSchemaInterface;

    isOpenModal: boolean;
    isOpenModalV2: boolean;
    modalData: any;
    modalDataV2: any;
    isReload: boolean;
    checked = [];
    actions = [];
    breadcrumbsActions = [];

    header: string[] = [];
    displayHeader: string[] = [];
    filteredRow: string[] = [];
    approvedHeader = {};
    labels = {};

    filterData = {};

    private effectsSubscription: ISubscription;
    private isLoadingSubscription: ISubscription;
    private mediaScreenSubscription: ISubscription;

    constructor(
        private store$: Store<DataListInterface>,
        private effects$: ListEffects,
        private route: Router,
        private activatedRoute: ActivatedRoute,
        private uiService: UIService,
        private customViewService: CustomViewService,
        public translate: TranslateService,
    ) {}

    ngOnInit() {
        this.store$.addReducer('dataList', dataListReducer);
        this.effectsSubscription = mergeEffects(this.effects$).subscribe();
        this.store$.select(state => state.dataList).subscribe((data) => {
            this.actions = [];
            this.breadcrumbsActions = [];
            if (data) {
                this.data = cloneDeep(data);
                if (this.data.actions) {
                    Object.keys(this.data.actions).forEach((action) => {
                        const obj = this.data.actions[action];
                        if (
                            obj.type === 'listFormActions' || 
                            (
                                obj.actions !== undefined && 
                                Object.keys(obj.actions).filter(element => obj.actions[element].type === 'listFormActions').length > 0
                            )
                        ) {
                            this.actions.push(obj);
                        } else {
                            this.breadcrumbsActions.push(obj);
                        }
                    });
                }
            } else {
                this.data = null;
            }
        });
        this.isLoading$ = this.store$.select(state => state.isLoading);

        this.activatedRoute.params.subscribe((params) => {
            if (params.entity) {
                this.moduleName = cloneDeep(params.entity);
                this.uiService.getStoreModulesSchema().subscribe((modules) => {
                    this.moduleSchema = modules.filter(element => element.name === params.entity)[0];
                });
                this.store$.dispatch(clearList());
                this.store$.dispatch(getList(this.initListFilterData));
            }
        });
    }

    ngOnDestroy() {
        this.store$.removeReducer('dataList');
        this.effectsSubscription.unsubscribe();
        if (this.mediaScreenSubscription) {
            this.mediaScreenSubscription.unsubscribe();
        }
    }

    buttonAction(event) {
        switch (event.type) {
            case 'noFormActions':
                this.store$.dispatch(requestButtonAction(event));
                break;
            case 'listFormActions':
                set(event, ['data', 'entityIds'], cloneDeep(this.checked));
                this.store$.dispatch(requestButtonAction(event));
                this.isLoadingSubscription = this.isLoading$.subscribe((data) => {
                    if (data === true) {
                        this.isReload = true;
                    } else if (this.isReload === true) {
                        this.unsubscribeIsLoading();
                        this.store$.dispatch(getList(Storages.getQueryList(LIST)));
                    }
                });
                break;
            case 'download':
                this.store$.dispatch(downloadButtonAction(event));
                break;
            case 'formActions':
                this.route.navigate(['../create'], { relativeTo: this.activatedRoute });
                break;
            case 'modal':
                this.modalData = cloneDeep(event);
                this.isOpenModal = true;
                break;
            case 'modal_v2':
                this.modalDataV2 = cloneDeep(event);
                this.isOpenModalV2 = true;
                break;
        }
    }

    pageAction(event) {
        Storages.setQueryList(LIST, event);
        this.store$.dispatch(getList(event));
    }

    rowAction(event) {
        switch (event.type) {
            case 'download':
                this.store$.dispatch(downloadButtonAction(event));
                break;
            case 'noFormActions':
                this.store$.dispatch(requestButtonAction(event));
                break;
            case 'formActions':
                this.route.navigate([`modules/${this.moduleName}/edit/${event.rowID}`]);
                break;
            case 'modal':
                this.modalData = cloneDeep(event);
                this.isOpenModal = true;
                break;
            case 'modal_v2':
                this.modalDataV2 = cloneDeep(event);
                this.isOpenModalV2 = true;
                break;
            case 'redirectActions':
                this.route.navigate([event.url]);
                break;
        }
    }

    onSaveModal(event) {
        const request = {};
        if (event.data) {
            Object.keys(event.data).forEach((key) => {
                request[key] = event.data[key].value;
            });
        }
        this.store$.dispatch(requestButtonAction({ ...event, ... { data: request } }));
        this.isOpenModal = false;
    }

    onActionModalV2(event) {
        switch (event.action.type) {
            case 'formActions':
                this.store$.dispatch(requestButtonAction({ ...event.action, data: event.data }));
                this.isLoadingSubscription = this.isLoading$.subscribe((data) => {
                    if (data === true) {
                        this.isReload = true;
                    } else if (this.isReload === true) {
                        this.unsubscribeIsLoading();
                        this.store$.dispatch(getList(Storages.getQueryList(LIST)));
                    }
                });
                this.isOpenModalV2 = false;
                break;
            case 'noFormActions':
            case 'listFormActions':
                if (event.action.type === 'listFormActions') {
                    set(event.action, ['data', 'entityIds'], cloneDeep(this.checked));
                }
                this.store$.dispatch(requestButtonAction(event.action));
                this.isLoadingSubscription = this.isLoading$.subscribe((data) => {
                    if (data === true) {
                        this.isReload = true;
                    } else if (this.isReload === true) {
                        this.unsubscribeIsLoading();
                        this.store$.dispatch(getList(Storages.getQueryList(LIST)));
                    }
                });
                this.isOpenModalV2 = false;
                break;
            case 'close':
                this.isOpenModalV2 = false;
                break;
        }
    }

    filterFields(event) {
        const filterData = cloneDeep(event.filter.fields);
        this.filterData = {};
        filterData.forEach((element) => {
            this.filterData[element.key] = element;
        });
        if (!event.refresh) {
            this.processHeader(event.filter.fields);
        } else {
            this.customViewService.moduleName = this.moduleName;
            this.customViewService.setFilter(event.filter).subscribe((response) => {
                this.processHeader(event.filter.fields);
                this.store$.dispatch(clearList());
                this.store$.dispatch(getList(this.initListFilterData));
            });
        }
    }

    unsubscribeIsLoading() {
        this.isLoadingSubscription.unsubscribe();
    }

    private processHeader(fields) {
        this.header = [];
        this.approvedHeader = {};
        this.labels = {};
        this.header.push('checked');
        this.approvedHeader['checked'] = true;

        const targetFields = UiUtilites.sortArray(fields, 'order');
        targetFields.forEach((field) => {
            this.header.push(field.key);
            this.labels[field.key] = field.label;
            this.approvedHeader[field.key] = true;
        });
        this.header.push('actions');
        this.approvedHeader['actions'] = true;
        this.filteredRow = cloneDeep(this.header);
        this.displayHeader = cloneDeep(this.header);
        this.setCountColumn();
    }

    private setCountColumn() {
        this.mediaScreenSubscription = this.store$.select(state => state.mediaScreen).subscribe((screen) => {
            switch (screen) {
                case 'xs': {
                    this.showColumn(2);
                    break;
                }
                case 'sm': {
                    this.showColumn(4);
                    break;
                }
                default: {
                    this.showColumn();
                    break;
                }
            }
        });
    }

    private showColumn(count?: number) {
        let k = 0;
        Object.keys(this.approvedHeader).forEach((key) => {
            k += 1;
            if (!count || k <= count || key === 'actions') {
                this.approvedHeader[key] = true;
            } else {
                this.approvedHeader[key] = false;
            }
        });
        this.setDefDisplayHeader();
        this.filteredRow = cloneDeep(this.displayHeader);
    }

    private setDefDisplayHeader() {
        this.displayHeader = this.header.filter(vall => this.approvedHeader[vall]);
    }

}
