import { GET_LIST_FINIDHED_WITH_SUCCESS, CLEAR_LIST } from './constants';
import { ActionWithPayloadInterface } from 'app/reducers';

export const dataListReducer = (state: any, action: ActionWithPayloadInterface) => {
    switch (action.type) {
        case GET_LIST_FINIDHED_WITH_SUCCESS: {
            return action.payload.data;
        }
        case CLEAR_LIST: {
            return null;
        }
    }

    return state;
};
