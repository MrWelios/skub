import { Injectable } from '@angular/core';
import { debounceTime, tap, map, switchMap, catchError } from 'rxjs/operators';
import { Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import get from 'lodash/get';

import { GET_LIST } from './constants';
import { setAppLoadingState, addNotification } from 'app/actions';
import { getListFinishedWithSuccess } from './actions';
import { NOTIFICATION_TYPE_ERROR } from 'app/constants';
import { EntityGlobalEffects } from '../../$effects';
import { ActionWithPayloadInterface } from 'app/reducers';

@Injectable()
export class ListEffects extends EntityGlobalEffects {
    @Effect() requestButton;
    @Effect() downloadButton;
    
    @Effect() requestGetList = this.actions$.pipe(
        ofType(GET_LIST),
        debounceTime(this.loadingDebounce),
        tap(() => this.store$.dispatch(setAppLoadingState(true))),
    ).distinctUntilChanged().pipe(
        switchMap((action: ActionWithPayloadInterface) => {
            const { data } = action.payload;
            this.entityService.moduleName = get(this.activatedRoute, 'snapshot.params.entity', null);
            return this.entityService.getList(data).pipe(
                switchMap((response: any) => {
                    this.store$.dispatch(getListFinishedWithSuccess(response));
                    return  Observable.of({});
                }),
                catchError((error) => {
                    this.translate.get(get(error, 'error.error', 'ERROR')).subscribe((value) => {
                        this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                    });
                    return Observable.of({});
                }),
                tap(() => this.store$.dispatch(setAppLoadingState(false))),
            );
        }),
    );

}
