import { AppStateInterface } from 'app/store';

export const GET_LIST_FINIDHED_WITH_SUCCESS = 'GET_LIST_FINIDHED_WITH_SUCCESS';
export const GET_LIST = 'GET_LIST';
export const LIST = 'LIST';
export const CLEAR_LIST = 'CLEAR_LIST';

export interface DataInterface {
    id: string;
    // Contacts_id: string;
    // Users_id: string;
    // Policies_id: string;
    // type: string;
    // currency: string;
    // status: string;
    // cost: string;
    // docs: string;
    // description: string;
    actions: {
        view: boolean;
        update: boolean;
        delete: boolean;
    };
}

export interface DataListInterface extends AppStateInterface {
    dataList: {
        total: number;
        itemsPerPage: number;
        page: number;
        data: DataInterface[];
    };
}

export interface DataListRequestInterface {
    page: number;
    itemsPerPage: number;
    sort: {
        field?: string;
        type?: 'asc' | 'desc';
    };
    filter: any;
}
