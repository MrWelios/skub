import { Injectable } from '@angular/core';
import { Effect, ofType } from '@ngrx/effects';
import 'rxjs/add/observable/of';

import { EntityGlobalEffects } from '../../$effects';
import { SAVE_DATA } from './constants';
import { debounceTime, tap, switchMap, catchError } from 'rxjs/operators';
import get from 'lodash/get';
import { setAppLoadingState, addNotification } from 'app/actions';
import { Observable } from 'rxjs/Observable';
import { NOTIFICATION_TYPE_ERROR, NOTIFICATION_TYPE_SUCCESS } from 'app/constants';
import { ActionWithPayloadInterface } from 'app/reducers';

@Injectable()
export class EntityEditEffects extends EntityGlobalEffects {
    @Effect() requestGetData;

    @Effect() requestButton;

    @Effect() requestSaveData = this.actions$.pipe(
        ofType(SAVE_DATA),
        debounceTime(this.loadingDebounce),
        tap(() => this.store$.dispatch(setAppLoadingState(true))),
    ).distinctUntilChanged().pipe(
        switchMap((action: ActionWithPayloadInterface) => {
            this.entityService.moduleName = get(this.activatedRoute, 'snapshot.params.entity', null);
            const { data } = action.payload;
            return this.entityService.updateData(data).pipe(
                switchMap((response: any) => {
                    this.translate.get('SUCCESS').subscribe((value) => {
                        this.store$.dispatch(addNotification(NOTIFICATION_TYPE_SUCCESS, value));
                    });
                    if (!data.id) {
                        this.route.navigate(['../list'], { relativeTo: this.activatedRoute });
                    } else {
                        this.route.navigate([`../../view/${data.id}`], { relativeTo: this.activatedRoute });
                    }
                    return  Observable.of({});
                }),
                catchError((error) => {
                    this.translate.get(get(error, 'error.error', 'ERROR')).subscribe((value) => {
                        this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                    });
                    return Observable.of({});
                }),
                tap(() => this.store$.dispatch(setAppLoadingState(false))),
            );
        }),
    );

}
