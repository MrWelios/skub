import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { mergeEffects } from '@ngrx/effects';
import { Location } from '@angular/common';
import { Store } from '@ngrx/store';
import cloneDeep from 'lodash/cloneDeep';
import { ISubscription } from 'rxjs/Subscription';
import { TranslateService } from '@ngx-translate/core';
import get from 'lodash/get';
import { Observable } from 'rxjs';

import { uploadFileClearing, addNotification } from 'app/actions';
import { UIService } from 'app/services/UIService';
import { ComponentCanDeactivate } from 'app/utilites/AbstractCanDeactivateComponent';
import { getData, getDataFinishedWithSuccess } from '../../actions';
import { dataReducer } from '../../redusers';
import { EntityEditEffects } from './$effects';
import { saveData, initNewData } from './actions';
import { EntityService } from 'app/services/EntityService';
import { UiUtilites } from 'app/utilites/UiUtilites';
import { NOTIFICATION_TYPE_ERROR } from 'app/constants';

@Component({
    templateUrl: './edit.template.html',
    styleUrls: [
        './edit.style.scss',
    ],
    providers: [EntityEditEffects],
})

export class EditComponent extends ComponentCanDeactivate implements OnInit, OnDestroy  {
    data: any;
    dataSchema: any;
    isCreate = false;
    fieldsData = {};
    sectionsOrder = {};
    canSave: boolean;
    invalidFields = {};
    isLoading$: Observable<boolean>;

    hasUnsavedData: boolean;
    private effectsSubscription: ISubscription;
    private dataSubscription: ISubscription;
    private schemaSubscription: ISubscription;

    constructor(
        private activatedRoute: ActivatedRoute,
        private route: Router,
        private store$: Store<any>,
        private effects$: EntityEditEffects,
        private uiService: UIService,
        private entityService: EntityService,
        private translate: TranslateService,
        private location: Location,
    ) {
        super();
    }

    ngOnInit() {
        this.store$.addReducer('data', dataReducer);
        this.isLoading$ = this.store$.select(state => state.isLoading);
        this.effectsSubscription = mergeEffects(this.effects$).subscribe();
        this.schemaSubscription = this.uiService.getStoreModulesSchema().subscribe((modules) => {
            this.dataSchema = modules.filter(element => element.name === get(this.activatedRoute, 'snapshot.params.entity', null))[0];
            this.sectionsOrder = UiUtilites.sortObject(this.dataSchema.data, 'order');
            this.activatedRoute.params.subscribe((params) => {
                if (params.id) {
                    this.store$.dispatch(getData(params.id));
                } else {
                    this.isCreate = true;
                    this.store$.dispatch(initNewData(this.generateNewData()));
                }
            });
        });

        this.dataSubscription = this.store$.select(state => state.data).subscribe((data) => {
            if (data) {
                this.data = cloneDeep(data);
                this.fieldsData = this.entityService.processEntitySections(this.dataSchema, this.data);
            }
        });

        this.canSave = Object.keys(this.invalidFields).length === 0;
    }

    ngOnDestroy() {
        this.store$.dispatch(getDataFinishedWithSuccess(null));
        this.store$.dispatch(uploadFileClearing());
        this.store$.removeReducer('data');
        this.effectsSubscription.unsubscribe();
        this.dataSubscription.unsubscribe();
        this.schemaSubscription.unsubscribe();
        this.dataSchema = null;
        this.data = null;
        this.isCreate = false;
    }

    sendErrorMessage(obj: any): void {
        if (!obj.event) {
            delete this.invalidFields[obj.fieldName];
        } else {
            this.invalidFields[obj.fieldName] = obj.event;
        }

        this.canSave = Object.keys(this.invalidFields).length === 0;
    }

    generateNewData() {
        const newData = {
            data: {},
            priceList: [],
        };
        Object.keys(this.dataSchema.data).forEach((key) => {
            newData.data[key] = { fields: {} };
            Object.keys(this.dataSchema.data[key].fields).forEach((field) => {
                if (this.dataSchema.data[key].fields[field].display_type === 'section') {
                    newData.data[key].fields[field] = { fields: {} };
                    Object.keys(this.dataSchema.data[key].fields[field].fields).forEach((subField) => {
                        if (this.dataSchema.data[key].fields[field].fields[subField].create) {
                            newData.data[key].fields[field].fields[subField] = null;
                        }
                    });
                } else {
                    if (this.dataSchema.data[key].fields[field].create) {
                        newData.data[key].fields[field] = null;
                    }
                }
            });
        });

        return newData;
    }

    save() {
        const sendingObj = this.entityService.generateSaveObject(this.fieldsData);
        const priceListData = this.generatePriceListData();

        if (priceListData.errors) {
            this.translate.get('ERRORS.TABLE_PART_VALIDATION').subscribe((value) => {
                this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
            });
        } else {
            this.store$.dispatch(saveData({ id: this.data.id, data: sendingObj, priceList: priceListData.data }));
            this.hasUnsavedData = false;
        }
    }

    validateTablePartValue(value: any): boolean {
        return value.entityId && value.quantity !== null && value.price !== null; 
    }

    cancel() {
        this.location.back();
    }

    private generatePriceListData(): any {
        const priceListData = { data: [], errors: false };
        if (this.data.priceList) {
            priceListData.data = this.data.priceList.filter(price => price.values.length).map((price) => {
                const priceValues = price.values.filter((value) => {
                    if (this.validateTablePartValue(value)) {
                        return true;
                    } 
                    priceListData.errors = true;
                    return false;
                }).map(value => ({
                    entityId: get(value, ['entityId', 'value'], value.entityId),
                    quantity: value.quantity,
                    price: value.price,
                    sequenceNo: value.sequenceNo,
                }));
                return {
                    moduleName: price.moduleName,
                    priceTableGroup: price.priceTableGroup,
                    values: priceValues,
                };
            });
        }
        return priceListData;
    }

}
