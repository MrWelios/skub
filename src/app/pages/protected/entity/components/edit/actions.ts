import { ActionWithPayloadInterface } from 'app/reducers';
import { SAVE_DATA, INIT_NEW_DATA } from './constants';

export function saveData(data: any): ActionWithPayloadInterface {
    return {
        type: SAVE_DATA,
        payload: {
            data,
        },
    };
}

export function initNewData(newData): ActionWithPayloadInterface {
    return {
        type: INIT_NEW_DATA,
        payload: {
            data: newData,
        },
    };
}
