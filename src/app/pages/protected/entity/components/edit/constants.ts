
export const SAVE_DATA = 'SAVE_DATA';
export const INIT_NEW_DATA = 'INIT_NEW_DATA';

export const newData = {
    EntitiesSchema_id: 2,
    email: null,
    firstname: null,
    lastname: null,
    password: null,
    data: {
        birthday: null,
        email: null,
        firstname: null,
        gender: null,
        lastname: null,
        new_claim_notification_email: null,
        new_claim_notification_sms: null,
        new_claim_notification_telegram: null,
        password: null,
        phone: null,
        position: null,
        role: null,
        telegram_username: null,
        tg_chatid: null,
    },
};
