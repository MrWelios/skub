import { ActionWithPayloadInterface } from 'app/reducers';
import { 
    GET_DATA, 
    GET_DATA_FINISHED_WITH_SUCCESS, 
    APPROVE, 
    REQUEST_BUTTON,
    CLEAR_DATA, 
    DOWNLOAD_BUTTON,
} from './constants';

export function getData(id: any): ActionWithPayloadInterface {
    return {
        type: GET_DATA,
        payload: {
            id,
        },
    };
}

export function getDataFinishedWithSuccess(data: any): ActionWithPayloadInterface {
    return {
        type: GET_DATA_FINISHED_WITH_SUCCESS,
        payload: {
            data,
        },
    };
}

export function clearData(): ActionWithPayloadInterface {
    return {
        type: CLEAR_DATA,
    };
}

export function approved(data, id?): ActionWithPayloadInterface {
    return {
        type: APPROVE,
        payload: {
            data,
            id,
        },
    };
}

export function requestButtonAction(data): ActionWithPayloadInterface {
    return {
        type: REQUEST_BUTTON,
        payload: {
            data,
        },
    };
}

export function downloadButtonAction(data): ActionWithPayloadInterface {
    return {
        type: DOWNLOAD_BUTTON,
        payload: {
            data,
        },
    };
}
