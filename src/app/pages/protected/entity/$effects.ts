import { Injectable } from '@angular/core';
import { debounceTime, tap, map, switchMap, concatMap, catchError } from 'rxjs/operators';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import 'rxjs/add/observable/of';
import get from 'lodash/get';
import { saveAs } from 'file-saver';
import { TranslateService } from '@ngx-translate/core';

import { GET_DATA, REQUEST_BUTTON, DOWNLOAD_BUTTON } from './constants';
import { AppStateInterface } from 'app/store';
import { setAppLoadingState, addNotification } from 'app/actions';
import { getDataFinishedWithSuccess } from './actions';
import { NOTIFICATION_TYPE_ERROR, NOTIFICATION_TYPE_SUCCESS } from 'app/constants';
import { BPService } from 'app/services/BPService';
import { EntityService } from '../../../services/EntityService';
import { ActionWithPayloadInterface } from 'app/reducers';

const LOADING_DEBOUNCE = 500;

@Injectable()
export class EntityGlobalEffects {
    
    loadingDebounce = LOADING_DEBOUNCE;
    @Effect() requestGetData = this.actions$.pipe(
        ofType(GET_DATA),
        debounceTime(this.loadingDebounce),
        tap(() => this.store$.dispatch(setAppLoadingState(true))),
    ).distinctUntilChanged().pipe(
        switchMap((action: ActionWithPayloadInterface) => {
            this.entityService.moduleName = get(this.activatedRoute, 'snapshot.params.entity', null);
            const { id } = action.payload;
            return this.entityService.getData(id).pipe(
                switchMap((response: any) => {
                    this.store$.dispatch(getDataFinishedWithSuccess(response));
                    return  Observable.of({});
                }),
                catchError((error) => {
                    this.translate.get(get(error, 'error.error', 'ERROR')).subscribe((value) => {
                        this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                    });
                    return Observable.of({});
                }),
                tap(() => this.store$.dispatch(setAppLoadingState(false))),
            );
        }),
    );

    @Effect() downloadButton = this.actions$.pipe(
        ofType(DOWNLOAD_BUTTON),
        debounceTime(this.loadingDebounce),
        tap(() => this.store$.dispatch(setAppLoadingState(true))),
    ).distinctUntilChanged().pipe(
        switchMap((action: ActionWithPayloadInterface) => {
            const { data } = action.payload;
            return this.entityService.downloadButton(data).pipe(
                switchMap((resp: any) => {
                    const blob = new Blob([resp.body], { type: resp.body.type });
                    if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window['MSStream']) {
                        const reader = new FileReader();
                        reader.onload = () => {
                            window.location.href = (reader.result as string);
                        };
                        reader.readAsDataURL(blob);
                    } else {
                        saveAs(blob, this.entityService.getFilename(resp.headers.get('content-disposition')), true);
                    }
                    return  Observable.of({});
                }),
                catchError((error) => {
                    const blb    = new Blob([error.error], { type: error.error.type });
                    const reader = new FileReader();

                    // This fires after the blob has been read/loaded.
                    reader.addEventListener('loadend', (e) => {
                        const text = get(e, 'srcElement.result', '');
                        this.translate.get(get(JSON.parse(text), 'error', 'ERROR')).subscribe((value) => {
                            this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                        });
                    });

                    // Start reading the blob as text.
                    reader.readAsText(blb);
                    
                    return Observable.of({});
                }),
                tap(() => this.store$.dispatch(setAppLoadingState(false))),
            );
        }),
    );

    @Effect() requestButton = this.actions$.pipe(
        ofType(REQUEST_BUTTON),
        debounceTime(this.loadingDebounce),
        tap(() => this.store$.dispatch(setAppLoadingState(true))),
    ).distinctUntilChanged().pipe(
        switchMap((action: ActionWithPayloadInterface) => {
            const { data } = action.payload;
            return this.entityService.requestButton(data).pipe(
                switchMap((response: any) => {
                    this.translate.get('SUCCESS').subscribe((value) => {
                        this.store$.dispatch(addNotification(NOTIFICATION_TYPE_SUCCESS, value));
                    });
                    if (data.isRedirect) {
                        if (data.redirectModule) {
                            if (response && response.id) {
                                this.route.navigate([`/modules/${data.redirectModule}/view/${response.id}`]);
                            } else {
                                this.route.navigate([`/modules/${data.redirectModule}/list/`]);
                            }
                        } else if (this.activatedRoute.snapshot.routeConfig.path === 'list') {
                            if (response && response.id) {
                                this.route.navigate([`../view/${response.id}`], { relativeTo: this.activatedRoute });
                            } else {
                                location.href = location.href;
                            }
                        } else {
                            this.route.navigate(['../../list'], { relativeTo: this.activatedRoute });
                        }
                    }
                    return  Observable.of({});
                }),
                catchError((error) => {
                    this.translate.get(get(error, 'error.error', 'ERROR')).subscribe((value) => {
                        this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                    });
                    return Observable.of({});
                }),
                tap(() => this.store$.dispatch(setAppLoadingState(false))),
            );
        }),
    );

    constructor(
        protected store$: Store<AppStateInterface>,
        protected actions$: Actions,
        protected entityService: EntityService,
        protected bpService: BPService,
        protected route: Router,
        protected activatedRoute: ActivatedRoute,
        protected translate: TranslateService,
    ) {}
}
