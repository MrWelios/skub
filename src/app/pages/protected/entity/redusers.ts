import { ActionWithPayloadInterface } from 'app/reducers';
import { 
    GET_DATA_FINISHED_WITH_SUCCESS, 
    GET_PROCESS_DEFINITION_FINISHED_WITH_SUCCESS, 
    CLEAR_DATA,
} from './constants';
import { INIT_NEW_DATA } from './components/edit/constants';

export const dataReducer = (state: any, action: ActionWithPayloadInterface) => {
    if (action.type === GET_DATA_FINISHED_WITH_SUCCESS || action.type === INIT_NEW_DATA) {
        return action.payload.data;
    }
    return state;
};

export const viewBPData = (state: any, action: ActionWithPayloadInterface) => {
    if (action.type === GET_PROCESS_DEFINITION_FINISHED_WITH_SUCCESS) {
        return action.payload.data;
    }

    if (action.type === CLEAR_DATA) {
        return {};
    }

    return state;
};
