import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MatFormFieldModule, MatSelectModule, MatButtonModule } from '@angular/material';
import { FormsModule } from '@angular/forms';

import { ProfileComponent } from './component';
import { ProfileRoutingModule } from './routing.module';
import { VCEBlockModule } from 'app/widgets/block/module';
import { VCEActionsModule } from 'app/widgets/actions/module';
import { BreadcrumbsModule } from 'app/widgets/breadcrumbs/module';

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        TranslateModule,
        ProfileRoutingModule,
        MatSelectModule,
        MatFormFieldModule,
        MatButtonModule,
        FormsModule,
        VCEBlockModule,
        VCEActionsModule,
        BreadcrumbsModule,
    ],
    declarations: [
        ProfileComponent,
    ],
    exports: [ProfileComponent],
})

export class ProfileModule { }
