import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';
import get from 'lodash/get';
import cloneDeep from 'lodash/cloneDeep';

import { AppStateInterface } from 'app/store';
import { UIService } from 'app/services/UIService';
import { NOTIFICATION_TYPE_SUCCESS } from 'app/constants';
import { ISubscription } from 'rxjs/Subscription';
import { UiUtilites } from 'app/utilites/UiUtilites';
import { EntityService } from 'app/services/EntityService';
import { updateProfileData } from 'app/actions';

@Component({
    styleUrls: ['./style.scss'],
    templateUrl: './template.html',
})

export class ProfileComponent implements OnInit, OnDestroy {
    
    lang: string;
    locales: string[] = [];
    data: any;
    dataSchema: any;
    sectionsOrder = {};
    fieldsData = {};
    canSave: boolean;
    invalidFields = {};

    private effectsSubscription: ISubscription;
    private schemaSubscription: ISubscription;
    private dataSubscription: ISubscription;

    constructor(
        private store$: Store<AppStateInterface>,
        private uiService: UIService,
        private entityService: EntityService,
        private translate: TranslateService,
        private location: Location,
    ) {}

    ngOnInit() {
        this.uiService.getLangState().subscribe((lang) => {
            this.lang = lang;
        });
        this.uiService.getLocalesState().subscribe((locales) => {
            this.locales = locales;
        });
        this.schemaSubscription = this.uiService.getStoreModulesSchema().subscribe((modules) => {
            this.dataSchema = modules.filter(element => element.name === 'users')[0];
            this.sectionsOrder = UiUtilites.sortObject(this.dataSchema.data, 'order');
        });

        this.dataSubscription = this.store$.select(state => state.profileData).subscribe((data) => {
            if (data) {
                this.data = cloneDeep(data);
                this.fieldsData = this.entityService.processEntitySections(this.dataSchema, this.data);
            }
        });

    }

    ngOnDestroy() {
        this.schemaSubscription.unsubscribe();
        this.dataSubscription.unsubscribe();
    }

    sendErrorMessage(obj: any): void {
        if (!obj.event) {
            delete this.invalidFields[obj.fieldName];
        } else {
            this.invalidFields[obj.fieldName] = obj.event;
        }

        this.canSave = Object.keys(this.invalidFields).length === 0;
    }

    generateNewData() {
        const newData = {
            data: {},
        };
        Object.keys(this.dataSchema.data).forEach((key) => {
            newData.data[key] = { fields: {} };
            Object.keys(this.dataSchema.data[key].fields).forEach((field) => {
                if (this.dataSchema.data[key].fields[field].display_type === 'section') {
                    newData.data[key].fields[field] = { fields: {} };
                    Object.keys(this.dataSchema.data[key].fields[field].fields).forEach((subField) => {
                        if (this.dataSchema.data[key].fields[field].fields[subField].create) {
                            newData.data[key].fields[field].fields[subField] = null;
                        }
                    });
                } else {
                    if (this.dataSchema.data[key].fields[field].create) {
                        newData.data[key].fields[field] = null;
                    }
                }
            });
        });

        return newData;
    }

    save() {
        const sendingObj = this.entityService.generateSaveObject(this.fieldsData);
        
        this.store$.dispatch(updateProfileData(sendingObj));
        // this.hasUnsavedData = false;
    }

    cancel() {
        this.location.back();
    }

}
