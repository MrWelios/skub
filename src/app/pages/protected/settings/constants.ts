import { AppStateInterface } from 'app/store';

export interface DelaySettingsDataInterface {
    id: number;
    EntitiesSchema_id: string;
    created: string;
    updated: string;
    data: {
        delay_in_seconds: number;
    };
}

export interface PermissionsSettingsDataInterface {
    EntitiesSchema_id: string;
    data: PermissionInterface[];
}

export interface PermissionInterface {
    role:  string;
    module: string;
    actions: {
        list: boolean;
        create: boolean;
        read: boolean;
        update: boolean;
        delete: boolean;
    };
}

export interface SettingsInterface extends AppStateInterface {
    delaySettings: DelaySettingsDataInterface;
    permisionsSettings: PermissionsSettingsDataInterface;
}

export const steppingBurn = {
    CONFIRM: 1,
    CONFIRM_EMAIL: 2,
    BURN: 3,
};
