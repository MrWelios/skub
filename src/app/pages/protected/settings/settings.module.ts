import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { 
    MatSelectModule, 
    MatSidenavModule, 
    MatIconModule,
    MatButtonModule, 
    MatInputModule,
    MatExpansionModule,
    MatButtonToggleModule,
    MatProgressSpinnerModule,
    MatSlideToggleModule,
} from '@angular/material';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { AccordionSettingsComponent } from './component';
import { SettingsService } from 'app/services/pages/SettingsService';
import { ToArrayModule } from 'app/directives/toArray.pipe';
import { ModalModule } from 'app/widgets/modal/module';
import { VCEBlockModule } from 'app/widgets/block/module';
import { VCEFormsModule } from 'app/widgets/forms/module';
import { VCEViewFormsModule } from 'app/widgets/view-forms/module';

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,
        ToArrayModule,
        TranslateModule,
        
        MatButtonModule,
        MatInputModule,
        MatSelectModule,
        MatSidenavModule,
        MatIconModule,
        MatExpansionModule,
        MatButtonToggleModule,
        MatProgressSpinnerModule,
        MatSlideToggleModule,
        ModalModule,
        
        VCEBlockModule,
        VCEFormsModule,
        VCEViewFormsModule,
    ],
    declarations: [
        AccordionSettingsComponent,
    ],
    providers: [SettingsService],
    exports: [AccordionSettingsComponent],
})

export class SettingsModule {}
