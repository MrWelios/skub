import { Component, OnDestroy, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Store } from '@ngrx/store';
import { ISubscription } from 'rxjs/Subscription';
import get from 'lodash/get';
import { TranslateService } from '@ngx-translate/core';

import { UIService } from 'app/services/UIService';
import { SettingsInterface } from './constants';
import { SettingsService } from 'app/services/pages/SettingsService';
import { Storages } from 'app/utilites/Storages';
import { NOTIFICATION_TYPE_SUCCESS } from 'app/constants';
import { addNotification } from 'app/actions';

@Component({
    selector: 'vce-admin-settings',
    styleUrls: ['./style.scss'],
    templateUrl: './template.html',
})

export class AccordionSettingsComponent implements OnInit, OnDestroy {

    @ViewChild('sidenav') sidenav: any;
    logined: boolean;
    isOpenAllSettings: boolean;
    allowedSettings: any;
    states = {
    };
    settingsSchema = {};

    private uiServiceSubscription: ISubscription;
    
    constructor(
        private uiService: UIService,
        private store$: Store<SettingsInterface>,
        private cdRef: ChangeDetectorRef,
        private settingsService: SettingsService,
        private translate: TranslateService,
    ) {}

    ngOnInit() {
        this.uiServiceSubscription = this.uiService.methodOpenSettingsObservable.subscribe((event) => {
            if (event) {
                this.sidenav.open();
            } else {
                this.sidenav.close();
            }
        });

        this.store$.select(state => state.modulesSchema)
            .filter(schema => schema !== undefined)
            .subscribe((schema) => { 
                this.logined = Object.keys(schema).length > 0;
                if (this.logined) {
                    this.settingsSchema = schema.filter(data => data.moduleType === 'settings')[0];
                    this.settingsService.getAllowedSetting({
                        page: 1,
                        itemsPerPage: 25,
                        sort: {
                        },
                        filter: {
                        },
                    })
                        .first()
                        .subscribe((response) => {
                            if (response.data) {
                                
                                // this.allowedSettings = response.data.map((element) => {
                                //     return {
                                //         data: {
                                //             id: element.id,
                                //             needParams: element.data,
                                //             data: {
                                //                 value: element.data.value,
                                //             },
                                //         },
                                //         schema: {
                                //             create: false,
                                //             display_type: element.data.valueType,
                                //             edit: get(this.settingsSchema, 'data.fields.value.edit', false),
                                //             format: element.data.valueType,
                                //             label: element.data.name,
                                //             required: get(this.settingsSchema, 'data.fields.value.required', false),
                                //             type: element.data.valueType,
                                //             view: get(this.settingsSchema, 'data.fields.value.view', false),
                                //         },
                                //     };
                                // });
                                this.allowedSettings = response.data.map((element) => {
                                    return {
                                        id: element.id,
                                        create: false,
                                        key: element.data.systemName,
                                        display_type: element.data.valueType,
                                        edit: get(this.settingsSchema, 'data.main.fields.value.edit', false),
                                        format: element.data.valueType,
                                        required: get(this.settingsSchema, 'data.main.fields.value.required', false),
                                        type: element.data.valueType,
                                        label: element.data.name,
                                        view: get(this.settingsSchema, 'data.main.fields.value.view', false),
                                        value: element.data.value,
                                        actions: element.actions,
                                    };
                                });
                            }
                        });
                }
            });
    }

    sendErrorMessage(key, event) {
    }

    ngOnDestroy() {
        this.uiServiceSubscription.unsubscribe();
    }

    save(event) {
        this.settingsService.updateData(event.id, { value: event.value }).first()
        .subscribe((response) => {
            this.translate.get('SUCCESS').subscribe((value) => {
                this.store$.dispatch(addNotification(NOTIFICATION_TYPE_SUCCESS, value));
            });
        });
        // event.needParams.value = event.data.value;
        // this.settingsService.updateData(event.id, event.needParams).first()
        // .subscribe((response) => {
        //     this.store$.dispatch(addNotification(NOTIFICATION_TYPE_SUCCESS, 'Success'));
        // });
    }

}
