import { ActionWithPayloadInterface } from 'app/reducers';
import { SET_TWO_FACTOR } from './constants';

export const isTwoFactor = (state: boolean, action: ActionWithPayloadInterface) => {
    if (action.type === SET_TWO_FACTOR) {
        return action.payload;
    }

    return state;
};
