import { AppStateInterface } from 'app/store';

export const GET_DATA = 'LOGIN_GET_DATA';
export const GET_DATA_FINISHED = 'LOGIN_GET_DATA_FINISHED';
export const SET_TWO_FACTOR = 'SET_TWO_FACTOR';
export const CONFIRM_LOGIN = 'CONFIRM_LOGIN';

export interface QueryParams {
    params: any;
    url: string;
}

export interface ReturnData {
    data: any;
}

export interface LoginStateInterface extends AppStateInterface {
    isTwoFactor: boolean;
}
