import { ActionWithPayloadInterface } from 'app/reducers';
import { GET_DATA, GET_DATA_FINISHED, QueryParams, ReturnData, SET_TWO_FACTOR, CONFIRM_LOGIN } from './constants';

export function getData(queryParams: QueryParams): ActionWithPayloadInterface {
    return {
        type: GET_DATA,
        payload: queryParams,
    };
}

export function requestFinished(data: ReturnData): ActionWithPayloadInterface {
    return {
        type: GET_DATA_FINISHED,
        payload: data,
    };
}

export function setTwoFactor(data: boolean): ActionWithPayloadInterface {
    return {
        type: SET_TWO_FACTOR,
        payload: data,
    };
}

export function confirmEmail(data: any): ActionWithPayloadInterface {
    return {
        type: CONFIRM_LOGIN,
        payload: {
            params: data,
        },
    };
}
