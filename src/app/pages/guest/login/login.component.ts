import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { mergeEffects } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs/Observable';
import { ISubscription } from 'rxjs/Subscription';

import { getData, setTwoFactor, confirmEmail } from './actions';
import { LoginEffects } from './effects$';
import { SS_RETURN_URL } from 'app/services/constants/CustomHttpService.constants';
import { UIService } from 'app/services/UIService';
import { AppStateInterface } from 'app/store';
import { isTwoFactor } from './reducers';
import { LoginStateInterface } from './constants';

@Component({
    selector: 'vce-login',
    templateUrl: './template.html',
    styleUrls: [
        '../../../../assets/css/template/style.css',
        '../../../../assets/css/template/style-magnific-popup.css',
        '../../../../assets/css/template/bootstrap.css',
        './style.scss',
    ],
    providers: [LoginEffects],
})

export class LoginComponent implements OnInit, OnDestroy {
    passwordHide = true;
    // isMailValid = false;
    isMailValid = true;
    location = '';
    loginData = {
        email: '',
        password: '',
        rememberme: false,
        confirmCode: '',
    };

    isLoading$: Observable<boolean>;
    isTwoFactor$: Observable<boolean>;
    errors$: Observable<string[]>;

    private effectsSubscription: ISubscription;

    constructor(
        private activatedRoute: ActivatedRoute,
        private store$: Store<LoginStateInterface>,
        private translate: TranslateService,
        private uiService: UIService,
        private effects$: LoginEffects,
    ) { }

    ngOnInit(): void {
        this.store$.addReducer('isTwoFactor', isTwoFactor);
        this.activatedRoute.queryParams.subscribe((params) => {
            const returnUrl = sessionStorage.getItem(SS_RETURN_URL);
            this.location = returnUrl ? returnUrl : '/';
        });

        this.effectsSubscription = mergeEffects(this.effects$).subscribe();
        this.isLoading$ = this.store$.select(state => state.isLoading);
        this.isTwoFactor$ = this.store$.select(state => state.isTwoFactor);
        this.errors$ = this.store$.select(state => state.errors);
    }

    mailValid() {
        const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!this.loginData.email || !regex.test(this.loginData.email)) {
            this.isMailValid = false;
        } else {
            this.isMailValid = true;
        }
    }

    login(): void {
        this.store$.dispatch(getData({
            params: this.loginData,
            url: this.location,
        }));
    }

    ngOnDestroy(): void {
        this.store$.removeReducer('isTwoFactor');
        if (this.effectsSubscription) {
            this.effectsSubscription.unsubscribe();
        }
    }

    cancelConfirmAction() {
        this.store$.dispatch(setTwoFactor(false));
    }

    confirmEmail() {
        this.store$.dispatch(confirmEmail(this.loginData));
    }
}
