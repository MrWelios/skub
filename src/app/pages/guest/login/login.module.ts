import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { 
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
} from '@angular/material';

import { LoginRoutingModule } from './routing.module';
import { LoginComponent } from './login.component';
import { DateMaskModule } from 'app/directives/date-mask/module';
import { LandingHeaderModule } from 'app/widgets/header/header.module';
import { LandingFooterModule } from 'app/widgets/footer/footer.module';
import { ModalModule } from 'app/widgets/modal/module';

@NgModule({
    imports: [
        ModalModule,
        MatButtonModule,
        MatIconModule,
        MatInputModule,
        MatFormFieldModule,
        LoginRoutingModule,
        LandingHeaderModule,
        LandingFooterModule,
        DateMaskModule,
        CommonModule,
        FormsModule,
        TranslateModule,
    ],
    providers: [
    ],
    declarations: [
        LoginComponent,
    ],
})

export class LoginModule {}
