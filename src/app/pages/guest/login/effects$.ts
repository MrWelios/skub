import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, debounceTime, switchMap, tap } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import get from 'lodash/get';
import 'rxjs/add/observable/of';
import { TranslateService } from '@ngx-translate/core';

import { addNotification, setAppLoadingState, setUserRole } from 'app/actions';
import { NOTIFICATION_TYPE_ERROR } from 'app/constants';
import { LoginService } from 'app/services/pages/LoginService';
import { AppStateInterface } from 'app/store';
import { requestFinished, setTwoFactor } from './actions';
import { GET_DATA, CONFIRM_LOGIN } from './constants';
import { ActionWithPayloadInterface } from 'app/reducers';

const LOADING_DEBOUNCE = 500;

@Injectable()
export class LoginEffects {
    @Effect() requestLogin = this.actions$.pipe(
        ofType(GET_DATA),
        debounceTime(LOADING_DEBOUNCE),
        tap(() => this.store$.dispatch(setAppLoadingState(true))),
    ).distinctUntilChanged().pipe(
        switchMap((action: ActionWithPayloadInterface) => {
            const { params, url } = action.payload;
            return this.loginService.obtainAccessToken(params).pipe(
                switchMap((data: any) => {
                    if (data['two-factor']) {
                        this.store$.dispatch(setTwoFactor(true));
                        return Observable.of({});
                    }
                    this.store$.dispatch(setUserRole(data.role));
                    this.loginService.saveToken(data, params.rememberme, url);
                    this.store$.dispatch(requestFinished(data));
                    return Observable.of({});
                }),
                catchError((error) => {
                    this.translate.get(get(error, 'error.error', 'ERRORS.INVALID_EMAIL_PASSWORD')).subscribe((value) => {
                        this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                    });
                    return Observable.of({});
                }),
                tap(() => this.store$.dispatch(setAppLoadingState(false))),
            );
        }),

    );

    @Effect() requestConfirmLogin = this.actions$.pipe(
        ofType(CONFIRM_LOGIN),
        debounceTime(LOADING_DEBOUNCE),
        tap(() => this.store$.dispatch(setAppLoadingState(true))),
    ).distinctUntilChanged().pipe(
        switchMap((action: ActionWithPayloadInterface) => {
            const { params } = action.payload;
            return this.loginService.continueAuth(params).pipe(
                switchMap((data: any) => {
                    this.store$.dispatch(setUserRole(data.role));
                    this.loginService.saveToken(data, params.rememberme, '');
                    this.store$.dispatch(requestFinished(data));
                    return Observable.of({});
                }),
                catchError((error) => {
                    this.translate.get(get(error, 'error.error', 'ERRORS.INVALID_EMAIL_PASSWORD')).subscribe((value) => {
                        this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                    });
                    return Observable.of({});
                }),
                tap(() => this.store$.dispatch(setAppLoadingState(false))),
            );
        }),

    );

    constructor(
        private store$: Store<AppStateInterface>,
        private actions$: Actions,
        private loginService: LoginService,
        private translate: TranslateService,
    ) { }
}
