import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
    templateUrl: './template.html',
    styleUrls: ['./style.scss'],
})

export class ErrorPageComponent implements OnInit {

    constructor(
        private translate: TranslateService,
    ) { }

    ngOnInit(): void { }
}
