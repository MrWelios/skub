import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material';

import { ModalComponent } from './component';

@NgModule({
    imports: [
        CommonModule,
        MatIconModule,
    ],
    declarations: [ModalComponent],
    exports: [ModalComponent],
})

export class ModalModule {}
