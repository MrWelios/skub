import { Component, OnInit, EventEmitter, Input, Output, ViewChild, OnDestroy, HostBinding } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { trigger, state, style, animate, transition, group } from '@angular/animations';

@Component({
    selector: 'vce-modal',
    templateUrl: './template.html',
    styleUrls: ['./style.scss'],
    animations: [
        trigger('dialog', [
            transition('void => *', [
                style({ opacity: 0 }),
                animate('.25s ease-in-out', style({ opacity: 1 })),
            ]),
            transition('* => void', [
                animate('.25s ease-out', style({ opacity: 0 })),
            ]),
        ]),
    ],
})

export class ModalComponent implements OnInit, OnDestroy {
    @Input() modalWidth = 700;
    @Input() backgroundColor = 'default';
    @Output() closeAction = new EventEmitter<void>();
   
    visible = true;
    
    ngOnInit(): void {
        const scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        document.body.style.position = 'fixed';
        document.body.setAttribute('data-pos', scrollTop.toString());
        document.body.style.top = '-' + scrollTop + 'px';
        document.body.style.left = '0';
        document.body.style.right = '0';
    }

    closeDialog() {
        this.visible = false;
        setTimeout(
            () => {
                this.closeAction.emit();
            },
            250,
        );
    }

    ngOnDestroy(): void {
        document.body.style.position = null;
        document.body.style['overflow-y'] = null;
        document.body.style.left = null;
        document.body.style.right = null;

        scrollTo(0, parseInt(document.body.getAttribute('data-pos'), 10));
    }

}
