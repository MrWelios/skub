import { Component, OnInit, OnDestroy } from '@angular/core';
import { ISubscription } from 'rxjs/Subscription';
import { Store } from '@ngrx/store';
import { AppStateInterface } from 'app/store';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'vce-landing-header',
    templateUrl: './header.template.html',
    styleUrls: [
        './header.style.scss',
        '../../../assets/css/template/style.css',
        '../../../assets/css/template/style-magnific-popup.css',
        '../../../assets/css/template/bootstrap.css',
    ],
})

export class LandingHeaderComponent {

    constructor() { }

}
