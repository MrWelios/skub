import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatToolbarModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material';

import { LandingHeaderComponent } from './header.component';

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        MatButtonModule,
        MatToolbarModule,
    ],
    declarations: [LandingHeaderComponent],
    exports: [LandingHeaderComponent],
})

export class LandingHeaderModule {}
