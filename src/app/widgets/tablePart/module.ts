import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { 
    MatButtonModule,
    MatCardModule,
    MatTableModule,
    MatIconModule,
    MatInputModule,
    MatAutocompleteModule,
    MatTooltipModule,
} from '@angular/material';

import { TablePartViewComponent } from './components/view/component';
import { TablePartEditComponent } from './components/edit/component';

@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        CommonModule,
        MatButtonModule,
        MatCardModule,
        MatTableModule,
        MatIconModule,
        MatInputModule,
        MatAutocompleteModule,
        MatTooltipModule,
    ],
    declarations: [TablePartViewComponent, TablePartEditComponent],
    exports: [TablePartViewComponent, TablePartEditComponent],
})

export class TablePartModule {}
