import { Component, OnDestroy, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import { MatTableDataSource } from '@angular/material';

import { AppStateInterface } from 'app/store';

@Component({
    selector: 'vce-table-part-view',
    templateUrl: './template.html',
    styleUrls: ['./style.scss'],
})
export class TablePartViewComponent implements OnInit, OnDestroy {

    @Input() data: any[];
    displayedColumns = ['item', 'quantity', 'price'];

    constructor(
        private translate: TranslateService,
    ) {}

    ngOnInit() {
        this.data.forEach((element) => {
            element.dataSource = new MatTableDataSource<any>(element.values);
        });
    }

    ngOnDestroy() {}
}
