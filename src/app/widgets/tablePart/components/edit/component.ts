import { Component, OnDestroy, OnInit, Input, Renderer } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import { FormControl } from '@angular/forms';
import { MatTableDataSource } from '@angular/material';

import { AppStateInterface } from 'app/store';
import { EntityService } from 'app/services/EntityService';

@Component({
    selector: 'vce-table-part-edit',
    templateUrl: './template.html',
    styleUrls: ['./style.scss'],
})
export class TablePartEditComponent implements OnInit, OnDestroy {

    @Input() data: any[];
    @Input() dataSchema: any;
    clickListener: Function;
    displayedColumns = ['item', 'quantity', 'price', 'sequenceNo', 'removeBtn'];

    constructor(
        private translate: TranslateService,
        private entityService: EntityService,
        private renderer: Renderer,
    ) {}

    ngOnInit() {
        this.clickListener = this.renderer.listenGlobal(
            'document',
            'click',
            (event: MouseEvent) => this.handleGlobalClick(event),
        );
        
        this.dataSchema.priceTables.forEach((price) => {
            let priceData = this.data.filter(element => element.moduleName === price.module && element.priceTableGroup === price.priceTableGroup)[0];

            if (priceData && priceData.values) {
                priceData.values.forEach((value) => {
                    value.selectedText = new FormControl();
                    value.selectedText.setValue(value.entityId.label);
                    value.currentSelectedText = value.selectedText.value;
                    value.loadedItems = [];
                    value.selectedText.valueChanges.debounceTime(300).subscribe(() => this.detectValueChanges(value, price.module));
                });
            } else {
                priceData = {
                    moduleName: price.module,
                    priceTableGroup: price.priceTableGroup,
                    values: [],
                };
                this.data.push(priceData);
            }
            price.values = priceData.values;
            price.dataSource = new MatTableDataSource<any>(price.values);
        });

    }

    ngOnDestroy() {
        if (this.clickListener) {
            this.clickListener();
        }
    }

    addRow(blockIndex: number) {
        const blockData = this.dataSchema.priceTables[blockIndex];
        const newValue = {
            entityId: null,
            sequenceNo: null,
            quantity: 1,
            price: null,
            selectedText: new FormControl(),
            currentSelectedText: '',
            loadedItems: [],
        };
        newValue.selectedText.valueChanges.debounceTime(300).subscribe(() => this.detectValueChanges(newValue, blockData.module));
        blockData.values.push(newValue);
        blockData.dataSource = new MatTableDataSource<any>(blockData.values);
    }

    removeRow(blockIndex: number, rowIndex: number) {
        const blockData = this.dataSchema.priceTables[blockIndex];
        blockData.values.splice(rowIndex, 1);
        blockData.dataSource = new MatTableDataSource<any>(blockData.values);
    }

    loadData(value: any, moduleName: string, str: string) {
        this.entityService.moduleName = moduleName;
        this.entityService.loadPriceItems({ 
            sourceSchemaName: this.dataSchema.name,
            nameToFind: str, 
            maxResultLimit: 5,
        }).subscribe((response) => {
            value.loadedItems = response.data;
        });
    }

    onSelectionChange(value: any, item: any) {
        value.entityId = {
            value: item.id,
            label: item.name,
            enabled: true,
        };
        value.price = item.price;
        value.currentSelectedText = item.name;
    }

    resetValue(value: any, isBtn: boolean) {
        value.entityId = null;
        value.quantity = 1;
        value.loadedItems = [];
        if (isBtn) {
            value.price = null;
            value.selectedText.setValue('');
            value.currentSelectedText = value.selectedText.value;
        }
    }

    handleGlobalClick(event: MouseEvent): void {
        const element = event.target as HTMLElement;
        if (!element.classList.contains('mat-option-text')) {
            this.dataSchema.priceTables.filter(price => price.values).forEach((price) => {
                price.values.forEach((value) => {
                    value.selectedText.setValue(value.currentSelectedText);
                });
            });
        }
    }

    detectValueChanges(value: any, moduleName: string) {
        if (value.selectedText.value) {
            if (value.selectedText.value.length >= 3 && value.selectedText.value !== value.currentSelectedText) {
                this.loadData(value, moduleName, value.selectedText.value);
            } else {
                value.loadedItems = [];
            }
        } else {
            this.resetValue(value, false);
        }
    }
}
