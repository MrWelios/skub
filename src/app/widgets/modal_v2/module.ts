import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
} from '@angular/material';

import { ModalV2Component } from './modal_v2.component';
import { VCEBlockModule } from '../block/module';
import { VCEActionsModule } from '../actions/module';
import { ModalModule } from '../modal/module';

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FlexLayoutModule,
        MatToolbarModule,
        MatIconModule,
        MatButtonModule,
        MatMenuModule,
        TranslateModule,
        VCEBlockModule,
        VCEActionsModule,
        ModalModule,
    ],
    declarations: [ModalV2Component],
    exports: [ModalV2Component],
})
export class ModalV2Module {}
