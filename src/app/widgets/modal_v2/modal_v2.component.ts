import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import set from 'lodash/set';

import { UiUtilites } from 'app/utilites/UiUtilites';
import { EntityService } from 'app/services/EntityService';

@Component({
    selector: 'vce-entity-modal-v2',
    styleUrls: ['./modal_v2.style.scss'],
    templateUrl: './modal_v2.template.html',
    providers: [
        // BPEffects,
    ],
})

export class ModalV2Component implements OnInit, OnDestroy {
    @Input() modalData: any;
    
    @Output() onAction = new EventEmitter<void>();
    @Output() onClose = new EventEmitter<void>();

    fieldsData: any = {};
    sendingObj: any = {};

    constructor(
        private entityService: EntityService,
    ) {}
    
    close() {
        this.onClose.emit();
    }

    cancel() {
        this.close();
    }

    ngOnInit() {
        this.modalData.sectionsOrder = UiUtilites.sortObject(this.modalData.data, 'order');
        Object.keys(this.modalData.data).forEach((section) => {
            this.modalData.data[section].sortOrder = UiUtilites.sortObject(this.modalData.data[section].fields, 'order');
            Object.keys(this.modalData.data[section].fields).forEach((field) => {
                const modalField = this.modalData.data[section].fields[field];
                set(this.fieldsData, [section, field], modalField);
                if (modalField.display_type === 'section') {
                    this.fieldsData[section][field].sortOrder = UiUtilites.sortObject(modalField.fields, 'order');
                } else if (modalField.display_type === 'field_map') {
                    this.fieldsData[section][field].sortOrder = UiUtilites.sortObject(modalField.value.field_info, 'order');
                }
            });
        });
    }

    ngOnDestroy() {

    }

    onActions(event) {
        this.sendingObj.data = {
            data: this.entityService.generateSaveObject(this.fieldsData),
            priceList: [],
        };
        this.sendingObj.action = event.value;
        this.onAction.emit(this.sendingObj);
    }

}
