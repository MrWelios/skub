import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[cvmMaskDraft]',
    // tslint:disable-next-line:use-host-property-decorator
    host: {
        '(input)': 'onChange($event)',
    },
})
export class CVMDraftMaskDirective implements OnInit {
    @Input() set cvmMask(val: number) {
        this.count = val;
    }
    @Input() set fraction(val: number) {
        this.fractionCount = val;
    }
    @Input() isNegative = false;
    @Input() draft = '';
        
    private oldLength = 0;
    private count: number;
    private fractionCount: number;

    private delimeter = {};
    private reg = '';

    constructor(private _elementRef: ElementRef) { }

    ngOnInit() {
        const regSymbols = new RegExp('[^0-9a-zA-Z]');

        this.reg = '';
        for (let i = 0; i < this.draft.length; i = i + 1) {
            if (this.draft[i] === '9') {
                this.reg += '([0-9]|$)';
            } else if (this.draft[i] === 'Z') {
                this.reg += '([a-zA-Z]|$)';
            } else if (this.draft[i].match(regSymbols)) {
                this.reg += ('([- /' + this.draft[i] + ']|$)');
                this.delimeter[i] = this.draft[i];
            }
        }
    }

    onChange(event: Event) {
        this.regexp();
    }

    setOld() {
        this.oldLength = this._elementRef.nativeElement.value.length;
    }

    inputBackspace() {
        this._elementRef.nativeElement.value = (this._elementRef.nativeElement.value).slice(0, -1);
    }

    regexp() {
        const regSt = new RegExp(this.reg);

        if (this._elementRef.nativeElement.value.match(regSt)[0] !== 
        this._elementRef.nativeElement.value.match(regSt).input) {
            this.inputBackspace();
        }
        
        if (this.delimeter[this._elementRef.nativeElement.value.length]) {
            if (this._elementRef.nativeElement.value.length >= this.oldLength) {
                this._elementRef.nativeElement.value = this._elementRef.nativeElement.value + 
                    this.delimeter[this._elementRef.nativeElement.value.length];
            } else {
                this.inputBackspace();
            }
        }
        this.setOld();
    }
}
