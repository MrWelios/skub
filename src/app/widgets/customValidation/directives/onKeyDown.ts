import { Directive, ElementRef, HostListener, OnInit } from '@angular/core';

@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[cvmOnKeyDown]',
})
export class CVMOnKeyDownDirective implements OnInit {
    constructor(private _elementRef: ElementRef) {}

    ngOnInit() {}

    @HostListener('keydown', ['$event']) onkeydown(e: any) {
        if (e.keyCode === 13 || e.which === 13) {
            e.preventDefault();
            if (e.srcElement.nextElementSibling) {
                e.srcElement.nextElementSibling.focus();
            }
        }
    }

}
