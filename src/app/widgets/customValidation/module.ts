import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CVMMaskDirective, CVMOnKeyDownDirective, CVMDateMaskDirective, CVMDraftMaskDirective } from './directives';

@NgModule({
    declarations: [CVMMaskDirective, CVMOnKeyDownDirective, CVMDateMaskDirective, CVMDraftMaskDirective],
    imports: [CommonModule],
    exports: [CVMMaskDirective, CVMOnKeyDownDirective, CVMDateMaskDirective, CVMDraftMaskDirective],
    bootstrap: [],
})

export class CustomValidationModule {
}
