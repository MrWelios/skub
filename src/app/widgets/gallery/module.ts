import { NgModule } from '@angular/core';
import { GalleryComponent } from './component';
import { CommonModule } from '@angular/common';

@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [GalleryComponent],
    exports: [GalleryComponent],
})

export class GalleryModule {}
