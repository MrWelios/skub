import { Component, Input, HostListener } from '@angular/core';
import get from 'lodash/get';

@Component({
    selector: 'vce-gallery',
    templateUrl: './template.html',
    styleUrls: ['./style.scss'],
})

export class GalleryComponent {
    @Input() images = [];
    @Input() height: number;

    isFullScreen = false;
    activeImg: number = null;

    private startX: number = null;
    private startY: number = null;

    constructor() {}

    @HostListener('touchstart', ['$event'])
    touchStartEvent(event): void {
        if (this.isFullScreen) {
            this.startX = get(event, ['changedTouches', 0, 'screenX'], null);
            this.startY = get(event, ['changedTouches', 0, 'screenY'], null);
        }
    }

    @HostListener('touchend', ['$event'])
    @HostListener('touchcancel', ['$event'])
    touchEndEvent(event): void {
        if (this.isFullScreen && this.startX) {
            const differenceX = this.startX - get(event, ['changedTouches', 0, 'screenX'], null);
            const differenceY = this.startY - get(event, ['changedTouches', 0, 'screenY'], null);
            const isMoveX = Math.abs(differenceX) > Math.abs(differenceY);

            if (isMoveX && differenceX > 50) {
                this.next();
            } else if (isMoveX && differenceX < -50) {
                this.prew();
            } else if (!isMoveX && differenceY > 50) {
                this.close();
            }

            this.startX = null;
            this.startY = null;
        }
    }

    @HostListener('touchmove', ['$event'])
    touchMoveEvent(event): void {
        if (this.isFullScreen) {
            // TODO add moving image before change image
        }
    }

    @HostListener('document:keyup', ['$event'])
    handleKeyboardEvent(event: KeyboardEvent): void { 
        if (this.isFullScreen) {
            switch (event.key) {
                case 'Escape': {
                    this.close();
                    break;
                }
                case 'ArrowRight': {
                    this.next();
                    break;
                }
                case 'ArrowLeft': {
                    this.prew();
                    break;
                }
            }
        }
    }

    openFulScreen(index): void {
        this.isFullScreen = true;
        this.activeImg = index;
    }

    prew(): void {
        if (this.activeImg > 0) {
            this.activeImg -= 1;
        }
    }
    
    next(): void {
        if (this.activeImg < (this.images.length - 1)) {
            this.activeImg += 1;
        }
    }

    close(): void {
        this.isFullScreen = false;
        this.activeImg = null;
    }
}
