import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router, PRIMARY_OUTLET } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ISubscription } from 'rxjs/Subscription';
import { Location } from '@angular/common';
import { Store } from '@ngrx/store';
import get from 'lodash/get';
import cloneDeep from 'lodash/cloneDeep';
import 'rxjs/add/operator/filter';

import { UIService } from 'app/services/UIService';
import { AppStateInterface } from 'app/store';
import { BreadcrumbItem } from 'app/constants';

@Component({
    selector: 'vce-breadcrumbs',
    styleUrls: ['./style.scss'],
    templateUrl: './template.html',
})

export class BreadcrumbsComponent implements OnInit, OnDestroy {
    @Input() actions;
    @Input() entityName;
    @Output() actionEvent = new EventEmitter<any>();

    breadcrumbs: BreadcrumbItem[] = [];
    navbarState = false;
    userRole: string;
    modulesSchema: any;
    isOpenSettings = false;

    private navbarStateSubscription: ISubscription;
    private userRoleSubscription: ISubscription;
    private modulesSchemaSubscription: ISubscription;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private translate: TranslateService,
        private uiService: UIService,
        private store$: Store<AppStateInterface>,
        private location: Location,
    ) { }

    ngOnInit(): void {
        this.navbarStateSubscription = this.uiService.getIsLogginedState().subscribe((state) => {
            this.navbarState = cloneDeep(state);
        });
        this.router.events
            .filter(event => event instanceof NavigationEnd)
            .subscribe(() => {
                if (this.modulesSchema) {
                    const root: ActivatedRoute = this.activatedRoute.root;
                    this.breadcrumbs = this.castBreadcrumbs(root);
                }
            });

        this.modulesSchemaSubscription = this.store$.select(state => state.modulesSchema).subscribe((modulesSchema) => {
            this.modulesSchema = cloneDeep(modulesSchema);
            if (modulesSchema) {
                const root: ActivatedRoute = this.activatedRoute.root;
                this.breadcrumbs = this.castBreadcrumbs(root);
            }
        });

        this.userRoleSubscription = this.store$.select(state => state.userRole).subscribe((role) => {
            this.userRole = cloneDeep(role);
        });
    }

    ngOnDestroy() {
        this.navbarStateSubscription.unsubscribe();
        this.userRoleSubscription.unsubscribe();
        this.modulesSchemaSubscription.unsubscribe();
    }

    castBreadcrumbs(route: ActivatedRoute, url: string = '', breadcrumbs: BreadcrumbItem[] = []) {
        let uri: string = cloneDeep(url);
        const children: ActivatedRoute[] = route.children;
        if (children.length === 0) {
            return breadcrumbs;
        }
        children.forEach((child: ActivatedRoute) => {
            if (child.outlet !== PRIMARY_OUTLET) {
                return;
            }
            if (!child.snapshot.data.hasOwnProperty('breadcrumb') || !child.snapshot.url.length) {
                return this.castBreadcrumbs(child, uri, breadcrumbs);
            }
            const routeUrl: string = child.snapshot.url.map(segment => segment.path).join('/');
            uri += `/${routeUrl}`;
            let breadcrumbItem: BreadcrumbItem = null;
            if (get(child, 'snapshot.data.breadcrumb', '') === 'entity') {
                breadcrumbItem = {
                    url: uri,
                    label: get(this.modulesSchema.filter(module => module.name === get(child, 'snapshot.params.entity', ''))[0], 'label', 'Entity'),
                    params: get(child, 'snapshot.params', ''),
                };
            } else {
                breadcrumbItem = {
                    url: uri,
                    label: get(child, 'snapshot.data.breadcrumb', '').toUpperCase(),
                    params: get(child, 'snapshot.params', ''),
                };
            }
            breadcrumbs.push(breadcrumbItem);

            return this.castBreadcrumbs(child, uri, breadcrumbs);
        });

        return breadcrumbs;
    }

    openSettings() {
        this.isOpenSettings = !this.isOpenSettings;
        this.uiService.updateSettingsVisible(this.isOpenSettings);
    }

    goBack() {
        this.location.back();
    }

    actionOnClick(event) {
        this.actionEvent.emit(event);
    }
}
