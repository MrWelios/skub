import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ISubscription } from 'rxjs/Subscription';
import { Store } from '@ngrx/store';
import cloneDeep from 'lodash/cloneDeep';
import 'rxjs/add/operator/filter';

import { UIService } from 'app/services/UIService';
import { AppStateInterface } from 'app/store';
import { BreadcrumbItem } from 'app/constants';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'vce-breadcrumbs-list',
    styleUrls: ['./style.scss'],
    templateUrl: './template.html',
})

export class BreadcrumbsListComponent implements OnInit, OnDestroy {
    @Input() actions;
    @Output() actionEvent = new EventEmitter<any>();

    breadcrumb: BreadcrumbItem;
    navbarState = false;
    userRole: string;

    private navbarStateSubscription: ISubscription;
    private userRoleSubscription: ISubscription;

    constructor(
        private translate: TranslateService,
        private uiService: UIService,
        private store$: Store<AppStateInterface>,
        private activatedRoute: ActivatedRoute,
    ) {}

    ngOnInit(): void {
        this.navbarStateSubscription = this.uiService.getIsLogginedState().subscribe((state) => {
            this.navbarState = cloneDeep(state);
        });
        
        this.activatedRoute.params.subscribe((params) => {
            if (params.entity) {
                this.breadcrumb = {
                    url: `/modules/${params.entity}/list`,
                    label: `${params.entity}_extra_fields`,
                    params: [],
                };
            }
        });
        
        this.userRoleSubscription = this.store$.select(state => state.userRole).subscribe((role) => {
            this.userRole = cloneDeep(role);
        });
    }

    ngOnDestroy() {
        this.navbarStateSubscription.unsubscribe();
        this.userRoleSubscription.unsubscribe();
    }

    actionOnClick(event) {
        this.actionEvent.emit(event);
    }
}
