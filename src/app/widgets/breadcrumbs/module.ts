import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { 
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatTooltipModule, 
    MatMenuModule,
 } from '@angular/material';

import { ToArrayModule } from 'app/directives/toArray.pipe';
import { BreadcrumbsComponent } from './components/entity/component';
import { BreadcrumbsListComponent } from './components/list/component';

@NgModule({
    imports: [
        ToArrayModule,
        RouterModule,
        TranslateModule,
        CommonModule,
        MatToolbarModule,
        MatIconModule,
        MatTooltipModule,
        MatButtonModule,
        MatMenuModule,
    ],
    declarations: [BreadcrumbsComponent, BreadcrumbsListComponent],
    exports: [BreadcrumbsComponent, BreadcrumbsListComponent],
})

export class BreadcrumbsModule {}
