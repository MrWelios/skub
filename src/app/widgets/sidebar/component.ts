import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ISubscription } from 'rxjs/Subscription';
import { TranslateService } from '@ngx-translate/core';
import cloneDeep from 'lodash/cloneDeep';
import { Storages } from 'app/utilites/Storages';

import { UIService } from 'app/services/UIService';

@Component({
    selector: 'vce-sidebar',
    styleUrls: ['./style.scss'],
    templateUrl: './template.html',
})

export class SidebarComponent implements OnInit, OnDestroy {
    @ViewChild('sidebar') sidebar: any;
    allowedModules = this.getSidebarMenu();
    isOpened: boolean;
    isLoginedState = false;

    private uiServiceSubscription: ISubscription;
    private menuListSubscription: ISubscription;
    private isLoginedStateSubscription: ISubscription;

    constructor(
        private uiService: UIService,
        private translate: TranslateService,
    ) {}

    ngOnInit() {
        this.uiServiceSubscription = this.uiService.methodOpenSidebarObservable.subscribe((event) => {
            if (event) {
                this.isOpened = true;
            } else {
                this.isOpened = false;
            }
        });

        this.isLoginedStateSubscription = this.uiService.getIsLogginedState().subscribe((state) => {
            this.isLoginedState = cloneDeep(state);
        });

        if (this.isLoginedState && !this.allowedModules) {
            this.menuListSubscription = this.uiService.getMenuListRequest().subscribe((state) => {
                if (state) {
                    Storages.setSidebarMenu(state);
                    this.allowedModules = state.map(k => ({
                        label: k.label,
                        value: k.module,
                        icon: k.icon,
                    }));
                }
            });
        }

    }

    ngOnDestroy() {
        this.uiServiceSubscription.unsubscribe();
        if (this.menuListSubscription) {
            this.menuListSubscription.unsubscribe();
        }
        this.isLoginedStateSubscription.unsubscribe();
    }

    private getSidebarMenu() {
        const modules = Storages.getSidebarMenu();
        if (modules) {
            return modules.map(k => ({
                label: k.label,
                value: k.module,
                icon: k.icon,
            }));
        }
        return null;
    }

}
