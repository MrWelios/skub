import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs/Observable';
// import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

import { DataInterface, DataSchemaInterface } from './constants';

// @AutoUnsubscribe()
@Component({
    selector: 'vce-block',
    templateUrl: './template.html',
    styleUrls: ['./style.scss'],
})
export class VCEBlockComponent implements OnInit, OnDestroy {
    @Input() data: DataInterface;
    // @Input() set data(data: DataInterface) {
    //     // this.listData = data;
    //     this.initData(data);
    // }
    @Input() dataSchema: DataSchemaInterface;
    @Input() canEdit = true;
    @Input() loading = false;
    @Input() isCreate = false;
    @Input() isFullWidth = false;
    @Input() actions: any[];
    @Input() title: string;
    @Input() moduleName: string;
    @Input() isNotCard = false;
    @Input() isOnlyFieldMap = false;
    // @Input() fieldsData: any;
    @Input() set fieldsData(data: DataInterface) {
        // this.listData = data;
        this.initData(data);
    }
    @Input() taskId: number;
    @Input() isShort: boolean;
    @Input() isShortWidth: boolean;
    @Output() errorMessage = new EventEmitter<any>();
    @Output() actionEvent = new EventEmitter<any>();
    isLoading$: Observable<boolean>;
    width: number;
    listData: DataInterface;

    ngOnInit(): void {
        this.width = !this.isFullWidth && this.dataSchema && this.dataSchema.width ? this.dataSchema.width : 100;
    }

    sendErrorMessage(obj: any): void {
        this.errorMessage.emit(obj);
    }

    action(item: any) {
        item.taskId = this.taskId;
        this.actionEvent.emit(item);
    }

    ngOnDestroy(): void {
    }

    initData(data) {
        Object.keys(data).forEach((key) => {
            if (data[key].depends) {
                if (data[key].depends.create) {
                    let globCreate = false;
                    data[key].depends.create.forEach((properties) => {
                        let create = true;
                        Object.keys(properties).forEach((propertiesKey) => {
                            let globProperty = false;
                            properties[propertiesKey].forEach((property) => {
                                if (data[propertiesKey].value === property) {
                                    globProperty = true;
                                }
                            });

                            create = globProperty;
                        });
                        globCreate = create;
                    });
                    data[key].create = globCreate;
                }

                if (data[key].depends.edit) {
                    let globEdit = false;
                    data[key].depends.edit.forEach((properties) => {
                        let edit = true;
                        Object.keys(properties).forEach((propertiesKey) => {
                            let globProperty = false;
                            properties[propertiesKey].forEach((property) => {
                                if (data[propertiesKey].value === property) {
                                    globProperty = true;
                                }
                            });

                            edit = globProperty;
                        });
                        globEdit = edit;
                    });
                    data[key].edit = globEdit;
                }

                if (data[key].depends.view) {
                    let globView = false;
                    data[key].depends.view.forEach((properties) => {
                        let view = true;
                        Object.keys(properties).forEach((propertiesKey) => {
                            let globProperty = false;
                            properties[propertiesKey].forEach((property) => {
                                if (data[propertiesKey].value === property) {
                                    globProperty = true;
                                }
                            });

                            view = globProperty;
                        });
                        globView = view;
                    });
                    data[key].view = globView;
                }

                if (data[key].depends.required) {
                    let globRequired = false;
                    data[key].depends.required.forEach((properties) => {
                        let required = true;
                        Object.keys(properties).forEach((propertiesKey) => {
                            let globProperty = false;
                            properties[propertiesKey].forEach((property) => {
                                if (data[propertiesKey].value === property) {
                                    globProperty = true;
                                }
                            });

                            required = globProperty;
                        });
                        globRequired = required;
                    });
                    data[key].required = globRequired;
                }

                if (data[key].depends.show_in_short_view) {
                    let globShowInShortView = false;
                    data[key].depends.show_in_short_view.forEach((properties) => {
                        let show_in_short_view = true;
                        Object.keys(properties).forEach((propertiesKey) => {
                            let globProperty = false;
                            properties[propertiesKey].forEach((property) => {
                                if (data[propertiesKey].value === property) {
                                    globProperty = true;
                                }
                            });

                            show_in_short_view = globProperty;
                        });
                        globShowInShortView = show_in_short_view;
                    });
                    data[key].show_in_short_view = globShowInShortView;
                }
            }
        });
        this.listData = data;
    }
}
