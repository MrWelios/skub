import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule, MatIconModule, MatButtonModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { VCEBlockComponent } from './component';
import { VCEFormsModule } from 'app/widgets/forms/module';
import { VCEViewFormsModule } from 'app/widgets/view-forms/module';
import { ToArrayModule } from 'app/directives/toArray.pipe';
import { VCEActionsModule } from '../actions/module';

@NgModule({
    imports: [
        MatIconModule,
        MatButtonModule,
        CommonModule,
        FormsModule,
        TranslateModule,
        MatCardModule,
        VCEFormsModule,
        VCEViewFormsModule,
        VCEActionsModule,
        ToArrayModule,
    ],
    declarations: [VCEBlockComponent],
    exports: [VCEBlockComponent],
})

export class VCEBlockModule {}
