export interface DataInterface {
    [fieldName: string]: any;
}

export interface DataSchemaInterface {
    label: string;
    order: number;
    width: number;
    fields: {
        [field_name: string]: {
            display_type: string;
            view: boolean;
            edit: boolean;
            canCopy: boolean;
            required: boolean;
            label: string;
            order: number;
            width: number;
            list_data?: {
                label: string;
                value: string;
            }[];
        };
    };
}

export const BLOCK_WIDTHS = [
    'bw-1-1',
    'bw-1-2',
];
