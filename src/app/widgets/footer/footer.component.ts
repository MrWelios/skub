import { Component } from '@angular/core';

@Component({
    selector: 'vce-landing-footer',
    templateUrl: './footer.template.html',
    styleUrls: [
        './footer.style.scss',
        '../../../assets/css/template/style.css',
        '../../../assets/css/template/style-magnific-popup.css',
        '../../../assets/css/template/bootstrap.css',
    ],
})

export class LandingFooterComponent {
    currentYear = (new Date()).getFullYear();
}
