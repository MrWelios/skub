import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { LandingFooterComponent } from './footer.component';


@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
    ],
    declarations: [LandingFooterComponent],
    exports: [LandingFooterComponent],
})

export class LandingFooterModule {}
