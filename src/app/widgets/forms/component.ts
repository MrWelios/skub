import {
    AfterViewInit,
    Component,
    EventEmitter,
    Input,
    OnDestroy,
    OnInit,
    Output,
    ViewChildren,
    ChangeDetectorRef,
    Renderer,
    ElementRef,
    ViewChild,
} from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { FormControl } from '@angular/forms';
import get from 'lodash/get';
import cloneDeep from 'lodash/cloneDeep';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

import { addNotification, getSystemLabelLink } from 'app/actions';
import {
    FILL_FORM_CORRECT_LABEL,
    FILL_REQUIRED_LABEL,
    FORM_TYPES,
    NOTIFICATION_TYPE_ERROR,
} from 'app/constants';
import { UIService } from 'app/services/UIService';
import { AppStateInterface } from 'app/store';
import { FORM_TYPES_WITH_LISTS, DISPLAY_TYPE_LINK } from './constants';
import { EntityService } from 'app/services/EntityService';
import { ISubscription } from 'rxjs/Subscription';

// @AutoUnsubscribe()
@Component({
    selector: 'vce-forms',
    templateUrl: './template.html',
    styleUrls: ['./style.scss'],
})
export class VCEFormsComponent implements OnInit, OnDestroy, AfterViewInit {
    @Input() set field(field: any) {
        this.fieldData = field;
        this.initField();
    }
    @Input() isFullWidth = false;
    @Input() fieldName: string;
    @Input() isCreate = false;
    @Output() errorMessage = new EventEmitter<any>();
    @ViewChildren('f') el;
    @ViewChild('chipAutocompleteInput') chipAutocompleteInput: ElementRef;
    // @ViewChildren('auto') autocomplete;
    
    validationErrors: string[];
    floatMask = '';
    floatLen = 0;
    floatBeforeComma = 4;
    floatAfterComma = 3;
    isOpenMarkdown = false;
    isErrorMarkdown = false;
    width: number;
    autocompleteResult: any;
    selectedText = new FormControl();
    currentSelectedText = '';
    filteredOptions: Observable<any[]>;
    dependsInit = false;
    separatorKeysCodes: number[] = [ENTER, COMMA];
    addOnBlur = false;
    removable = true;
    selectable = true;
    isOpenLinkModal = false;
    linkSchema: any;
    linkData = [];
    fieldData: any;

    clickListener: Function;

    private systemLabelLinkSubscription: ISubscription;
    private profileDataSubscription: ISubscription;

    constructor(
        protected store$: Store<AppStateInterface>,
        private translate: TranslateService,
        private uiService: UIService,
        private entityService: EntityService,
        private cdRef: ChangeDetectorRef,
        private renderer: Renderer,         
        private elementRef: ElementRef,
    ) {}

    ngOnInit(): void { 
        if (this.isCreate && this.fieldData.create) {
            this.fieldData.edit = true;
        }
        
        this.validationErrors = this.validate(this.fieldData);
        setTimeout(() => {
            if (this.validationErrors.length !== 0) {
                this.validationErrors.forEach((element) => {
                    this.translate.get(element).subscribe((value) => {
                        this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                    });
                });
            }
        });
        
        if (this.fieldData.required &&
            ((this.fieldData.edit && !this.fieldData.value && 
                this.fieldData.display_type !== 'chips' && this.fieldData.display_type !== 'radio') ||
                (this.fieldData.display_type === 'chips' && (!this.fieldData.list_data || this.fieldData.list_data.length === 0)))) {
            this.errorMessage.emit({ fieldName: this.fieldName, event: FILL_REQUIRED_LABEL });
        } 

        if (this.fieldData.display_type === 'checkbox') {
            this.checkboxChange();
        }
    }

    ngAfterViewInit(): void {
        this.el.changes.subscribe((q) => {
            if (this.fieldData.display_type === 'markdown' && !this.fieldData.value) {
                setTimeout(() => this.isErrorMarkdown = true, 0);
                this.errorMessage.emit({ fieldName: this.fieldName, event: FILL_REQUIRED_LABEL });
            } else {
                setTimeout(() => this.isErrorMarkdown = false, 0);
                this.errorMessage.emit({ fieldName: this.fieldName, event: null });
            }
            if (this.el.last) {
                if (this.el.last.errors) {
                    setTimeout(() => this.el.last.control.markAsTouched(true));
                    this.errorMessage.emit({ fieldName: this.fieldName, event: FILL_FORM_CORRECT_LABEL });
                } else if (this.fieldData.display_type === 'chips' && this.fieldData.required && this.fieldData.list_data.length === 0) {
                    setTimeout(() => this.el.last.control.markAsTouched(true));
                    this.errorMessage.emit({ fieldName: this.fieldName, event: FILL_REQUIRED_LABEL });
                } else {
                    this.errorMessage.emit({ fieldName: this.fieldName, event: null });
                }
            }
        });

        if (this.fieldData.dependentField) {
            this.el.last.control.valueChanges.subscribe((value) => {
                if (this.dependsInit) {
                    this.fieldData.dependentField.value = null;
                    this.fieldData.dependentField.selectedText.setValue('');
                } else {
                    this.dependsInit = true;
                }
                if (value) {
                    this.fieldData.dependentField.module = value;
                } else {
                    this.fieldData.dependentField.module = '';
                }
            });
        }

        if (
            DISPLAY_TYPE_LINK.indexOf(this.fieldData.display_type) !== -1 && 
            this.fieldData.edit
        ) {
            if (this.selectedText) {
                this.selectedText.valueChanges.debounceTime(300).subscribe((q) => {
                    if (this.selectedText.value) {
                        if (this.selectedText.value.length >= 3 && this.selectedText.value !== this.currentSelectedText) {
                            this.autocompleteSearch();
                        } else {
                            this.autocompleteResult = [];
                        }
                    } else {
                        this.resetValue();
                    }
                });
            }
        }
    }

    /* Autocomplete block */

    autocompleteSearch(): void {
        this.entityService.moduleName = this.fieldData.module;
        // this.fieldData.display_type = 'system_label_link';
        if (this.fieldData.display_type === 'system_label_link') {
            this.entityService.searchIchane({ nameToFind: this.selectedText.value, maxResultLimit: 5 })
                .subscribe((response) => {
                    this.autocompleteResult = response.data;
                });
        } else {
            this.entityService.search({ nameToFind: this.selectedText.value, maxResultLimit: 5 })
                .subscribe((response) => {
                    this.autocompleteResult = response.data;
                });
        }

    }

    resetValue() {
        if (this.fieldData.display_type === 'multi_label_link') {
            this.fieldData.value = [];
        } else {
            this.fieldData.value = null;
        }

        if (this.selectedText.value) {
            this.selectedText.setValue('');
        }
        this.currentSelectedText = this.selectedText.value;
        this.autocompleteResult = [];
    }

    onMultipleRemove(item) {
        this.fieldData.value = this.fieldData.value.filter(row => row.value !== item.value);
    }

    onMultipleSelected(item) {
        let dontSelect = false;
        this.fieldData.value.map((row) => {
            if (item.id === row.value) {
                dontSelect = true;
            }
        });
        if (!dontSelect) {
            this.fieldData.value.push({
                label: item.name,
                value: item.id,
            });
        }
        this.chipAutocompleteInput.nativeElement.value = '';
    }

    onSelectionChange(item) {
        this.currentSelectedText = item.name;
        this.fieldData.value = item.id;
    }

    selectElement(event) {
        this.selectedText.setValue(event.name);
        this.currentSelectedText = this.selectedText.value;
        this.fieldData.value = event.id;
    }

    /* --- */

    handleGlobalClick(event: MouseEvent): void {
        const element = event.target as HTMLElement;
        // TODO: change logic for click element in dropdown
        // const withinElement = this.autocomplete.first._elementRef.nativeElement.contains(event.target);
        if (!element.classList.contains('mat-option-text')) {
            this.selectedText.setValue(this.currentSelectedText);
        }
    }

    checkboxChange(): void {
        if (this.fieldData.required && this.fieldData.edit && !this.fieldData.value) {
            this.errorMessage.emit({ fieldName: this.fieldName, event: FILL_REQUIRED_LABEL });
        } else {
            this.errorMessage.emit({ fieldName: this.fieldName, event: null });
        }
    }

    numberFilter(event): void {
        const reg = new RegExp('^\\d+$');
        return reg.test(event.key) ? event.key : false;
    }

    commonSymbolsFilter(event): void {
        const reg = new RegExp('^(\\d|\\w)+$');
        return reg.test(event.key) ? event.key : false;
    }

    addChip(event: MatChipInputEvent): void {
        const input = event.input;
        const value = event.value;

        if ((value || '').trim() && !this.fieldData.list_data.includes(value)) {
            this.fieldData.list_data.push(value.trim());
        }

        if (input) {
            input.value = '';
        }
    }
    
    removeChip(item: any): void {
        const index = this.fieldData.list_data.indexOf(item);

        if (index >= 0) {
            this.fieldData.list_data.splice(index, 1);
        }
    }

    validate(field: any): string[] {
        const errors = [];
        let fieldLabel = field.label;
        if (fieldLabel) {
            this.translate.get(fieldLabel).subscribe((value) => {
                fieldLabel = value;
            });
        }
        if (!field.display_type || !FORM_TYPES.includes(field.display_type)) {
            this.translate.get('ERRORS.INVALID_TYPE_FIELD', { label: fieldLabel }).subscribe((value) => {
                errors.push(value);
            });
        }

        if (!fieldLabel) {
            this.translate.get('ERRORS.INVALID_LABEL_FIELD').subscribe((value) => {
                errors.push(value);
            });
        }

        if (typeof field.required === 'undefined') {
            this.translate.get('ERRORS.INVALID_REQUIRED_FIELD', { label: fieldLabel }).subscribe((value) => {
                errors.push(value);
            });
        }

        if (typeof field.edit === 'undefined') {
            this.translate.get('ERRORS.INVALID_EDITABLE_FIELD', { label: fieldLabel }).subscribe((value) => {
                errors.push(value);
            });
        }

        if (FORM_TYPES_WITH_LISTS.includes(field.display_type) && 
        (!field.list_data || field.list_data.length === 0)) {
            this.translate.get('ERRORS.INVALID_LIST_PROPERTY', { label: fieldLabel }).subscribe((value) => {
                errors.push(value);
            });
        }

        return errors;
    }

    updateDate(event) {
        if (event) {
            this.fieldData.value = this.fieldData.display_type === 'date' 
                ? this.uiService.setDateFormat(new Date(event))
                : new Date(event).toISOString();
            this.errorMessage.emit({ fieldName: this.fieldName, event: null });
        } else if (this.fieldData.required) {
            this.errorMessage.emit({ fieldName: this.fieldName, event: FILL_REQUIRED_LABEL });
        }
    }

    openMarkdown () {
        this.isOpenMarkdown = !this.isOpenMarkdown;
    }

    closeMarkdown() {
        this.isOpenMarkdown = false;
    }

    ngOnDestroy(): void {
        if (this.fieldData.display_type === 'label_link' && this.clickListener) {
            this.clickListener();
        }
        if (this.fieldData.display_type === 'system_label_link') {
            this.systemLabelLinkSubscription.unsubscribe();
        }
        if (this.fieldData.display_type === 'price') {
            this.profileDataSubscription.unsubscribe();
        }
    }

    private filter(name: string): any[] {
        const filterValue = name.toLowerCase();
    
        return this.fieldData.list_data ? this.fieldData.list_data.filter(option => option.name.toLowerCase().indexOf(filterValue) === 0) : [];
    }

    private initField() {
        if (this.fieldData.display_type === 'dependent_label_link') {
            this.fieldData.selectedText = this.selectedText;
        }

        if ((this.fieldData.display_type === 'label_link' || this.fieldData.display_type === 'dependent_label_link') && this.fieldData.edit) {
            this.clickListener = this.renderer.listenGlobal(
                'document',
                'click',
                (event: MouseEvent) => this.handleGlobalClick(event),
            );
            this.selectedText.setValue(get(this.fieldData, ['value', 'label'], ''));
            this.currentSelectedText = this.selectedText.value;
        }
        if (this.fieldData.valueLabel) {
            this.selectedText.setValue(this.fieldData.valueLabel.trim());
            this.currentSelectedText = this.selectedText.value;
        }

        if (this.fieldData.display_type === 'multi_label_link') {
            this.selectedText.setValue(get(this.fieldData, ['value'], []));
            this.currentSelectedText = this.selectedText.value;
            this.fieldData.value = this.fieldData.value || [];

        }
        if (this.fieldData.display_type === 'system_label_link') {
            this.selectedText.setValue(get(this.fieldData, ['value', 'label'], ''));
            this.currentSelectedText = this.selectedText.value;
            this.fieldData.value = this.fieldData.value || {};
            this.store$.dispatch(getSystemLabelLink(this.fieldData.module));
            this.systemLabelLinkSubscription = this.store$.select(state => state.systemLabelLink).subscribe((data) => {
                this.fieldData.list_data = cloneDeep(get(data, [this.fieldData.module, 'data'], []));
            });

            this.filteredOptions = this.selectedText.valueChanges.pipe(
                startWith(''),
                map(value => typeof value === 'string' ? value : value.name),
                map(name => name ? this.filter(name) : this.fieldData.list_data ? this.fieldData.list_data.slice() : []),
            );
        }

        if (this.fieldData.display_type === 'price') {
            this.profileDataSubscription = this.store$.select(state => state.profileData).subscribe((data) => {
                const currencyId = cloneDeep(get(data, ['data', 'main', 'fields', 'currency_id', 'value'], null));
                if (currencyId) {
                    this.store$.dispatch(getSystemLabelLink('ichain_currencies'));
                    this.systemLabelLinkSubscription = this.store$.select(state => state.systemLabelLink).subscribe((systemData) => {
                        const currency = get(systemData, ['ichain_currencies', 'data'], []).filter(element => element.id === currencyId)[0];
                        if (currency) {
                            this.fieldData.shortLabel = currency.shortName;
                        }
                    });
                }
            });
        }

        this.width = !this.isFullWidth ? (this.fieldData.width ? this.fieldData.width : 25) : 100;

        if (this.fieldData.edit) {
            if (this.fieldData.display_type === 'date') {
                this.fieldData.value = this.uiService.setDateFormat(new Date(this.fieldData.value));
            }
    
            if (this.fieldData.display_type === 'datetime') {
                this.fieldData.value = this.uiService.setDateTimeFormat(new Date(this.fieldData.value));
            }
    
            if (this.fieldData.display_type === 'radio' && (this.fieldData.value === undefined || this.fieldData.value === null)) {
                this.fieldData.value = this.fieldData.list_data[0].value;
            }
    
            if (this.fieldData.display_type === 'float') {
                this.floatBeforeComma = this.fieldData.beforeComma || 4;
                this.floatAfterComma = this.fieldData.afterComma || 3;
                // this.floatMask = Array(beforeComma + 1).join('9') + '.' + Array(afterComma + 1).join('9');
                // this.floatLen = beforeComma + afterComma + 1;
            }
    
            if (this.fieldData.list_data === null) {
                this.fieldData.list_data = [];
            }
    
            if (this.fieldData.display_type === 'textarea' && !this.fieldData.length) {
                this.fieldData.length = 100;
            }
        }
    }
}
