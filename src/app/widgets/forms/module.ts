import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    DateAdapter,
    MAT_DATE_FORMATS,
    MatButtonModule,
    MatButtonToggleModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatFormFieldModule,
    MatAutocompleteModule,
} from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';

import { VCE_DATE_FORMATS } from 'app/constants';
import { VCEDateAdapter } from 'app/utilites/VCEDateAdapter';
import { CustomValidationModule } from 'app/widgets/customValidation/module';
import { VCEFormsComponent } from './component';
import { DatepickerModule } from 'app/widgets/datepicker/module';
import { InputFileModule } from 'app/widgets/inputFile/module';
import { MarkdownModule } from 'app/widgets/markdown/module';
import { ModalModule } from 'app/widgets/modal/module';
import { TableModule } from '../table/module';
import { LinkModalModule } from '../linkModal/module';

@NgModule({
    imports: [
        InputFileModule,
        CustomValidationModule,
        MatButtonToggleModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatCheckboxModule,
        TranslateModule,
        MatButtonModule,
        MatRippleModule,
        MatSelectModule,
        MatInputModule,
        MatRadioModule,
        MatChipsModule,
        MatIconModule,
        MatFormFieldModule,
        MatAutocompleteModule,
        DatepickerModule,
        CommonModule,
        FormsModule,
        MarkdownModule,
        ModalModule,
        ReactiveFormsModule,
        TableModule,
        LinkModalModule,
    ],
    declarations: [VCEFormsComponent],
    exports: [VCEFormsComponent],
    providers: [
        {
            provide: DateAdapter, 
            useClass: VCEDateAdapter,
        },
        {
            provide: MAT_DATE_FORMATS, 
            useValue: VCE_DATE_FORMATS,
        },
    ],
})

export class VCEFormsModule {}
