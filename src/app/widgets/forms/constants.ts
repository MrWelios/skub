export const FORM_TYPES_WITH_LISTS = [
    'select', 'multiselect', 'radio',
];

export const DISPLAY_TYPE_LINK = [
    'label_link',
    'system_label_link',
    'dependent_label_link',
    'multi_label_link',
];
