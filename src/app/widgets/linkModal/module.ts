import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LinkModalComponent } from './component';
import { MatIconModule, MatButtonModule, MatProgressSpinnerModule } from '@angular/material';
import { TableModule } from '../table/module';
import { ModalModule } from '../modal/module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        MatButtonModule,
        MatIconModule,
        TableModule,
        ModalModule,
        TranslateModule,
        MatProgressSpinnerModule,
    ],
    declarations: [LinkModalComponent],
    exports: [LinkModalComponent],
})

export class LinkModalModule {}
