import { Component, OnInit, OnDestroy, Input, EventEmitter, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import cloneDeep from 'lodash/cloneDeep';

import { UIService } from 'app/services/UIService';
import { EntityService } from 'app/services/EntityService';
import { UiUtilites } from 'app/utilites/UiUtilites';

@Component({
    selector: 'vce-link-modal',
    templateUrl: './template.html',
    styleUrls: ['./style.scss'],
})

export class LinkModalComponent implements OnInit, OnDestroy {
    
    @Input() field: any;
    @Output() selectElementAction = new EventEmitter<any>();

    isOpenLinkModal = false;
    linkSchema: any;
    linkData: any = {};
    isLoading = false;

    header: string[] = [];
    displayHeader: string[] = [];
    filteredRow: string[] = [];
    approvedHeader = {};
    labels = {};

    constructor(
        private translate: TranslateService,
        private uiService: UIService,
        private entityService: EntityService,
    ) {}

    ngOnInit(): void {  
    }

    openLinkModal() {
        this.uiService.getStoreModulesSchema().subscribe((modules) => {
            this.isOpenLinkModal = true;
            this.isLoading = true;
            this.linkSchema = modules.filter(element => element.name === this.field.module)[0];
            const fields = this.linkSchema.customView.fields;
            this.header = [];
            this.approvedHeader = {};
            this.labels = {};
            const targetFields = UiUtilites.sortArray(fields, 'order');
            targetFields.forEach((field) => {
                this.header.push(field.key);
                this.labels[field.key] = field.label;
                this.approvedHeader[field.key] = true;
            });
            this.displayHeader = cloneDeep(this.header);
            this.entityService.moduleName = this.field.module;
            const data = {
                sort: {},
                page: 1,
                itemsPerPage: 10,
                filter: {},
            };
            this.loadData(data);
        });
    }
    
    closeLinkModal() {
        this.linkData = {};
        this.isOpenLinkModal = false;
    }

    clickRow(element) {
        this.selectElementAction.emit({ id: element.id, name: element.name });
        this.closeLinkModal();
    }

    changePage(data) {
        this.loadData(data);
    }

    ngOnDestroy(): void { 
    }

    private loadData(data) {
        this.entityService.getList(data).subscribe((response) => {
            this.linkData = response;
            this.isLoading = false;
        });
    }

}
