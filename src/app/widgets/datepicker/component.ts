import {
    Component, ElementRef, EventEmitter, Input, OnChanges,
    OnInit, Output, Renderer, SimpleChange, OnDestroy,
} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { Calendar } from './calendar';
import * as moment from 'moment';
import cloneDeep from 'lodash/cloneDeep';
import { TranslateService } from '@ngx-translate/core';

interface DateFormatFunction {
    // tslint:disable-next-line:callable-types
    (date: Date): string;
}

interface ValidationResult {
    [key: string]: boolean;
}

@Component({
    selector: 'vce-datepicker',
    animations: [
        // trigger('calendarAnimation', [
        //     transition('* => left', [
        //         // animate(180, keyframes([
        //         //     style({ transform: 'translateX(105%)', offset: 0.5 }),
        //         //     style({ transform: 'translateX(-130%)', offset: 0.51 }),
        //         //     style({ transform: 'translateX(0)', offset: 1 }),
        //         // ])),
        //     ]),
        //     transition('* => right', [
        //         // animate(180, keyframes([
        //         //     style({ transform: 'translateX(-105%)', offset: 0.5 }),
        //         //     style({ transform: 'translateX(130%)', offset: 0.51 }),
        //         //     style({ transform: 'translateX(0)', offset: 1 }),
        //         // ])),
        //     ]),
        // ]),
    ],
    styleUrls: ['./style.scss'],
    templateUrl: './template.html',
})
export class DatepickerComponent implements OnInit, OnChanges, OnDestroy {
    // two way bindings
    @Output() dateChanged = new EventEmitter<Date>();
    @Output() dateValid = new EventEmitter<boolean>();

    @Input() get date() { return this.dateVal; }

    set date(val) {
        this.dateVal = typeof val === 'string' ? new Date(val) : val;
        this.dateChanged.emit(this.dateVal);
    }
    @Input() set endBlockedDate(val: Date) {
        this.endBlockDateVal = cloneDeep(val);
        if (val.valueOf() >= this.dateVal.valueOf()) {
            setTimeout(() => {
                const newDate = new Date(val.setDate(val.getDate() + 1));
                this.date = newDate;
                this.onSelect.emit(newDate);
            },         0);
        }
    }
    // api bindings
    @Input() isDisabeInputDate: boolean;
    @Input() isFullWidth = false;
    @Input() isError: boolean;
    @Input() readonly: boolean;
    @Input() required: boolean;
    @Input() disabled: boolean;
    @Input() accentColor: string;
    @Input() altInputStyle: boolean;
    @Input() dateFormat: string | DateFormatFunction;
    @Input() fontFamily: string;
    @Input() rangeStart: Date;
    @Input() rangeEnd: Date;
    // data
    @Input() placeholder = this.translate.instant('SELECT_A_DATE');
    @Input() inputText: string;
    // view logic
    @Input() showCalendar: boolean;
    @Input() cancelText = this.translate.instant('CANCEL');
    @Input() weekStart = 1;
    // events
    @Output() onSelect = new EventEmitter<Date>();
    // time
    @Input() calendarDays: number[];
    @Input() currentMonth: string;
    @Input() dayNames: string[] = 
        ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'].map(day => this.translate.instant(`WIDGETS.DATEPICKER.${day}`)); // Default order: firstDayOfTheWeek = 0
    @Input() hoveredDay: Date;
    @Input() months: string[];
    @Input() openToTop: Boolean = false;
    dayNamesOrdered: String[];
    calendar: Calendar;
    positionTop: {
        [fieldName: string]: string;
    };
    currentMonthNumber: number;
    currentYear: number;
    // animation
    animate: string;
    // colors
    colors: { [id: string]: string };
    // listeners
    clickListener: Function;
    // forms
    yearControl: FormControl;

    private readonly DEFAULT_FORMAT = 'YYYY-MM-DD';

    private dateVal: Date;
    private endBlockDateVal: Date;

    constructor(
        private renderer: Renderer, 
        private elementRef: ElementRef,
        private translate: TranslateService,
    ) {
        this.dateFormat = this.DEFAULT_FORMAT;
        // view logic
        this.showCalendar = false;
        // colors
        this.colors = {
            none: '',
            black: '#063958',
            blue: '#00B2E4',
            lightGrey: 'rgba(241, 241, 241, 0.5)',
            white: '#ffffff',
            'white-disabled': 'rgba(255, 255, 255, 0.5)',
        };
        this.positionTop = {
            top: '0px',
        };
        this.accentColor = this.colors['blue'];
        this.altInputStyle = false;
        // time
        this.updateDayNames();

        this.months = [
            'JANUARY',
            'FEBRUARY',
            'MARCH',
            'APRIL',
            'MAY',
            'JUNE',
            'JULY',
            'AUGUST',
            'SEPTEMBER',
            'OCTOBER',
            'NOVEMBER',
            'DECEMBER',
        ].map(month => this.translate.instant(`WIDGETS.DATEPICKER.${month}`));
        // listeners
        this.clickListener = renderer.listenGlobal(
            'document',
            'click',
            (event: MouseEvent) => this.handleGlobalClick(event),
        );
        // form controls
        this.yearControl = new FormControl('', Validators.compose([
            Validators.required,
            Validators.maxLength(4),
            this.yearValidator,
            this.inRangeValidator.bind(this),
        ]));
    }

    ngOnInit() {
        this.updateDayNames();
        this.syncVisualsWithDate();
    }

    ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
        if ((changes['date'] || changes['dateFormat'])) {
            this.syncVisualsWithDate();
        }
        if (changes['firstDayOfTheWeek'] || changes['dayNames']) {
            this.updateDayNames();
        }
    }

    ngOnDestroy() {
        this.clickListener();
    }

    // -------------------------------------------------------------------------------- //
    // -------------------------------- State Management ------------------------------ //
    // -------------------------------------------------------------------------------- //
    /**
     * Closes the calendar and syncs visual components with selected or current date.
     * This way if a user opens the calendar with this month, scrolls to two months from now,
     * closes the calendar, then reopens it, it will open with the current month
     * or month associated with the selected date
     */
    closeCalendar(): void {
        this.showCalendar = false;
        this.syncVisualsWithDate();  
    }

    changeInputText() {
        const regex = new RegExp(/^(0[1-9]|[12][0-9]|3[01])[- /\.](0[1-9]|1[012])[- /\.]([1|2]\d\d\d)$/);
        const res = this.inputText.match(regex);
        if (res) {
            this.date = new Date(`${res[3]}-${res[2]}-${res[1]}`);
            this.dateValid.emit(true);
        } else {
            this.date = null;
            this.inputText = '';
            this.dateValid.emit(false);
        }
        this.syncVisualsWithDate();
    }

    checkDisabeDay(day: Date) {
        if (!!this.endBlockDateVal) {
            return day.valueOf() <= this.endBlockDateVal.valueOf();
        } 
        return false;
    }

    /**
     * Visually syncs calendar and input to selected date or current day
     */
    syncVisualsWithDate(): void {
        if (this.date) {
            this.setInputText(this.date);
            this.setCurrentValues(this.date);
        } else {
            this.inputText = '';
            this.setCurrentValues(new Date());
        }
    }

    /**
     * Sets the currentMonth and creates new calendar days for the given month
     */
    setCurrentMonth(monthNumber: number): void {
        this.currentMonth = this.months[monthNumber];
        const calendarArray = this.calendar.monthDays(this.currentYear, this.currentMonthNumber);
        this.calendarDays = [].concat.apply([], calendarArray);
        this.calendarDays = this.filterInvalidDays(this.calendarDays);
    }

    /**
     * Sets the currentYear and FormControl value associated with the year
     */
    setCurrentYear(year: number): void {
        this.currentYear = year;
        this.yearControl.setValue(year);
    }

    /**
     * Sets the visible input text
     */
    setInputText(date: Date): void {
        let inputText = '';
        const dateFormat: string | DateFormatFunction = this.dateFormat;
        if (dateFormat === undefined || dateFormat === null) {
            inputText = moment(date).format(this.DEFAULT_FORMAT);
        } else if (typeof dateFormat === 'string') {
            inputText = moment(date).format(dateFormat);
        } else if (typeof dateFormat === 'function') {
            inputText = dateFormat(date);
        }
        this.inputText = inputText;
    }

    // -------------------------------------------------------------------------------- //
    // --------------------------------- Click Handlers ------------------------------- //
    // -------------------------------------------------------------------------------- //
    /**
     * Sets the date values associated with the calendar.
     * Triggers animation if the month changes
     */
    onArrowClick(direction: string): void {
        const currentMonth: number = this.currentMonthNumber;
        let newYear: number = this.currentYear;
        let newMonth: number;
        // sets the newMonth
        // changes newYear is necessary
        if (direction === 'left') {
            if (currentMonth === 0) {
                newYear = this.currentYear - 1;
                newMonth = 11;
            } else {
                newMonth = currentMonth - 1;
            }
        } else if (direction === 'right') {
            if (currentMonth === 11) {
                newYear = this.currentYear + 1;
                newMonth = 0;
            } else {
                newMonth = currentMonth + 1;
            }
        }
        // check if new date would be within range
        const newDate = new Date(newYear, newMonth);
        let newDateValid: boolean;
        if (direction === 'left') {
            newDateValid = !this.rangeStart || newDate.getTime() >= this.rangeStart.getTime();
        } else if (direction === 'right') {
            newDateValid = !this.rangeEnd || newDate.getTime() <= this.rangeEnd.getTime();
        }

        if (newDateValid) {
            this.setCurrentYear(newYear);
            this.currentMonthNumber = newMonth;
            this.setCurrentMonth(newMonth);
            this.triggerAnimation(direction);
        }
    }

    /**
     * Check if a date is within the range.
     * @param date The date to check.
     * @return true if the date is within the range, false if not.
     */
    isDateValid(date: Date): boolean {
        return (!this.rangeStart || date.getTime() >= this.rangeStart.getTime()) &&
            (!this.rangeEnd || date.getTime() <= this.rangeEnd.getTime());
    }

    /**
     * Filter out the days that are not in the date range.
     * @param calendarDays The calendar days
     * @return {Array} The input with the invalid days replaced by 0
     */
    filterInvalidDays(calendarDays: number[]): number[] {
        const newCalendarDays = [];
        calendarDays.forEach((day: number | Date) => {
            if (day === 0 || !this.isDateValid(<Date>day)) {
                newCalendarDays.push(0);
            } else {
                newCalendarDays.push(day);
            }
        });
        return newCalendarDays;
    }

    /**
     * Closes the calendar when the cancel button is clicked
     */
    onCancel(): void {
        this.closeCalendar();
    }

    /**
     * Toggles the calendar when the date input is clicked
     */
    onInputClick(event, isApproveOpen): void {

        if (event && event.target) {
            const findParentEl = event.path.find(k => k.className === 'ng-star-inserted');
            const parent = findParentEl ? findParentEl.getBoundingClientRect() : event.target;
            const rangeY = event.clientY;
            const position = rangeY + 340 > document.body.clientHeight;
            this.positionTop = position 
                ? { bottom: `${document.body.clientHeight - parent.bottom}px` }
                : { top: `${parent.top + 24}px` } ; 
        } 
        if (!this.disabled && !this.readonly && isApproveOpen) {
            this.showCalendar = !this.showCalendar;
        }
    }

    /**
     * Returns the font color for a day
     */
    onSelectDay(day: Date, isDisabe): void {
        if (this.isDateValid(day) && !isDisabe) {
            this.date = day;
            this.onSelect.emit(day);
            this.showCalendar = !this.showCalendar;
        }
    }

    /**
     * Sets the current year and current month if the year from
     * yearControl is valid
     */
    onYearSubmit(): void {
        if (this.yearControl.valid && +this.yearControl.value !== this.currentYear) {
            this.setCurrentYear(+this.yearControl.value);
            this.setCurrentMonth(this.currentMonthNumber);
        } else {
            this.yearControl.setValue(this.currentYear);
        }
    }

    // -------------------------------------------------------------------------------- //
    // ----------------------------------- Listeners ---------------------------------- //
    // -------------------------------------------------------------------------------- //
    /**
     * Closes the calendar if a click is not within the datepicker component
     */
    handleGlobalClick(event: MouseEvent): void {
        const withinElement = this.elementRef.nativeElement.contains(event.target);
        if (!withinElement) {
            this.closeCalendar();
        }
    }

    // -------------------------------------------------------------------------------- //
    // ----------------------------------- Helpers ------------------------------------ //
    // -------------------------------------------------------------------------------- //
    /**
     * Returns the background color for a day
     */
    getDayBackgroundColor(day: Date): string {
        let color = this.colors['none'];
        if (this.isChosenDay(day)) {
            color = this.accentColor;
        } else if (this.isCurrentDay(day)) {
            color = this.colors['lightGrey'];
        }
        return color;
    }

    /**
     * Returns the font color for a day
     */
    getDayFontColor(day: Date): string {
        let color = this.colors['black'];
        if (this.isChosenDay(day)) {
            color = this.colors['white'];
        }
        return color;
    }

    /**
     * Returns whether a day is the chosen day
     */
    isChosenDay(day: Date): boolean {
        if (day) {
            return this.date ? day.toDateString() === this.date.toDateString() : false;
        }
        return false;
    }

    /**
     * Returns whether a day is the current calendar day
     */
    isCurrentDay(day: Date): boolean {
        if (day) {
            return day.toDateString() === new Date().toDateString();
        }
        return false;
    }

    /**
     * Returns whether a day is the day currently being hovered
     */
    isHoveredDay(day: Date): boolean {
        return this.hoveredDay ? this.hoveredDay === day && !this.isChosenDay(day) : false;
    }

    /**
     * Triggers an animation and resets to initial state after the duration of the animation
     */
    triggerAnimation(direction: string): void {
        this.animate = direction;
        setTimeout(() => this.animate = 'reset', 185);
    }

    // -------------------------------------------------------------------------------- //
    // ---------------------------------- Validators ---------------------------------- //
    // -------------------------------------------------------------------------------- //
    /**
     * Validates that a value is within the 'rangeStart' and/or 'rangeEnd' if specified
     */
    inRangeValidator(control: FormControl): ValidationResult {
        const value = control.value;

        if (this.currentMonthNumber || this.currentMonthNumber === 0) {
            const tentativeDate = new Date(+value, this.currentMonthNumber);
            if (this.rangeStart && tentativeDate.getTime() < this.rangeStart.getTime()) {
                return { yearBeforeRangeStart: true };
            }
            if (this.rangeEnd && tentativeDate.getTime() > this.rangeEnd.getTime()) {
                return { yearAfterRangeEnd: true };
            }
            return null;
        }

        return { currentMonthMissing: true };
    }

    /**
     * Validates that a value is a number greater than or equal to 1970
     */
    yearValidator(control: FormControl): ValidationResult {
        const value = control.value;
        const valid = !isNaN(value) && value >= 1970 && Math.floor(value) === +value;
        if (valid) {
            return null;
        }
        return { invalidYear: true };
    }

    /**
     * Sets the date values associated with the ui
     */
    private setCurrentValues(date: Date) {
        this.currentMonthNumber = date.getMonth();
        this.currentMonth = this.months[this.currentMonthNumber];

        this.currentYear = date.getFullYear();
        this.yearControl.setValue(this.currentYear);

        const calendarArray = this.calendar.monthDays(this.currentYear, this.currentMonthNumber);
        this.calendarDays = [].concat.apply([], calendarArray);
        this.calendarDays = this.filterInvalidDays(this.calendarDays);
    }

    /**
     * Update the day names order. The order can be modified with the firstDayOfTheWeek input, while 0 means that the
     * first day will be sunday.
     */
    private updateDayNames() {
        this.dayNamesOrdered = this.dayNames.slice(); // Copy DayNames with default value (weekStart = 0)
        if (this.weekStart < 0 || this.weekStart >= this.dayNamesOrdered.length) {
            // Out of range
            throw Error(`The weekStart is not in range between ${0} and ${this.dayNamesOrdered.length - 1}`);
        } else {
            this.calendar = new Calendar(this.weekStart);
            this.dayNamesOrdered = this.dayNamesOrdered.slice(this.weekStart, this.dayNamesOrdered.length)
                .concat(this.dayNamesOrdered.slice(0, this.weekStart)); // Append beginning to end
        }
    }
}
