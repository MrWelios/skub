import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { MyDatePickerModule } from 'mydatepicker';
import { DatepickerComponent } from './component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { DateMaskModule } from 'app/directives/date-mask/module';
import { MatDatepickerModule, MatInputModule, MatFormFieldModule } from '@angular/material';

@NgModule({
    imports: [
        CommonModule, 
        FormsModule, 
        ReactiveFormsModule, 
        MyDatePickerModule, 
        DateMaskModule, 
        MatFormFieldModule, 
        MatDatepickerModule, 
        MatInputModule,
        TranslateModule,
    ],
    declarations: [DatepickerComponent],
    exports: [DatepickerComponent],
})

export class DatepickerModule {}
