import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import set from 'lodash/set';

import { EntityService } from 'app/services/EntityService';
import { UiUtilites } from 'app/utilites/UiUtilites';

@Component({
    selector: 'vce-entity-modal',
    styleUrls: ['./modal_v1.style.scss'],
    templateUrl: './modal_v1.template.html',
    providers: [
        // BPEffects,
    ],
})

export class ModalV1Component implements OnInit, OnDestroy {
    @Input() modalData: any;
    
    @Output() onSave = new EventEmitter<void>();
    @Output() onClose = new EventEmitter<void>();

    fieldsData: any = {};
    sendingObj: any = {};
    canSave: boolean;
    invalidFields = {};

    constructor(
        private entityService: EntityService,
    ) {}
    
    close() {
        this.onClose.emit();
    }

    cancel() {
        this.close();
    }

    ngOnInit() {
        this.modalData.sectionsOrder = UiUtilites.sortObject(this.modalData.data, 'order');
        Object.keys(this.modalData.data).forEach((section) => {
            this.modalData.data[section].sortOrder = UiUtilites.sortObject(this.modalData.data[section].fields, 'order');
            Object.keys(this.modalData.data[section].fields).forEach((field) => {
                const modalField = this.modalData.data[section].fields[field];
                set(this.fieldsData, [section, field], modalField);
                if (modalField.display_type === 'section') {
                    this.fieldsData[section][field].sortOrder = UiUtilites.sortObject(modalField.fields, 'order');
                } else if (modalField.display_type === 'field_map') {
                    this.fieldsData[section][field].sortOrder = UiUtilites.sortObject(modalField.value.field_info, 'order');
                }
            });
        });

        this.canSave = Object.keys(this.invalidFields).length === 0;
    }

    ngOnDestroy() {

    }

    sendErrorMessage(obj: any): void {
        if (!obj.event) {
            delete this.invalidFields[obj.fieldName];
        } else {
            this.invalidFields[obj.fieldName] = obj.event;
        }

        this.canSave = Object.keys(this.invalidFields).length === 0;
    }

    save() {
        this.sendingObj.data = {
            data: this.entityService.generateSaveObject(this.fieldsData),
            priceList: [],
        };
        this.onSave.emit({ ...this.modalData, ...this.sendingObj });
    }

}
