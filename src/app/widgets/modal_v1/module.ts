import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
} from '@angular/material';

import { ModalV1Component } from './modal_v1.component';
import { VCEBlockModule } from '../block/module';
import { VCEActionsModule } from '../actions/module';
import { ModalModule } from '../modal/module';

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FlexLayoutModule,
        MatToolbarModule,
        MatIconModule,
        MatButtonModule,
        MatMenuModule,
        TranslateModule,
        VCEBlockModule,
        VCEActionsModule,
        ModalModule,
    ],
    declarations: [ModalV1Component],
    exports: [ModalV1Component],
})
export class ModalV1Module {}
