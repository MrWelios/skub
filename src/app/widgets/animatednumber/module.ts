import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnimatedNumerComponent } from './component';

@NgModule({
    imports: [CommonModule],
    declarations: [AnimatedNumerComponent],
    exports: [AnimatedNumerComponent],
})

export class AnimatedNumberModule {}
