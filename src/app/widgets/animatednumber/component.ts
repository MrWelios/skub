import { Component, Input, Output } from '@angular/core';
import cloneDeep from 'lodash/cloneDeep';

@Component({
    selector: 'vce-animated-number',
    templateUrl: './template.html',
    styleUrls: ['./style.scss'],
})

export class AnimatedNumerComponent {
    @Input() number: number;
    @Input() label: string;
    @Output() countoChange;
    private _timer: any;
    private _countTo: number;
    private _countFrom: number;
    private _step: number;

    @Input()
    set countTo(countTo: string) {
        this._countTo = parseFloat(countTo);
        this.run();
    }

    @Input()
    set countFrom(countFrom: string) {
        this._countFrom = parseFloat(countFrom);
        this.run();
    }

    @Input()
    set step(step: string) {
        this._step = parseFloat(step);
        this.run();
    }

    run() {
        clearInterval(this._timer);

        if (isNaN(this._step)) {
            return false;
        }

        if (isNaN(this._countFrom)) {
            return false;
        }

        if (isNaN(this._countTo)) {
            return false;
        }

        if (this._step <= 0) {
            console.log('Step must be greater than 0.');
            return false;
        }

        let intermediate  = this._countFrom;
        const increment = 1;
        this.countoChange = Math.floor(intermediate);

        this._timer = setInterval(() => {
            if (this._countTo < this._countFrom) {
                if (intermediate <= this._countTo) {
                    clearInterval(this._timer);
                    this.countoChange = Math.floor(this._countTo);
                } else {
                    this.countoChange = Math.floor(intermediate);
                    intermediate -= increment;
                }
            } else {
                if (intermediate >= this._countTo) {
                    clearInterval(this._timer);
                    this.countoChange = Math.floor(this._countTo);
                } else {
                    this.countoChange = Math.floor(intermediate);
                    intermediate += increment;
                }
            }
        },                        this._step);
    }
}
