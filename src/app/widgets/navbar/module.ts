import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
} from '@angular/material';

import { NavbarComponent } from './component';

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FlexLayoutModule,
        MatToolbarModule,
        MatIconModule,
        MatButtonModule,
        MatMenuModule,
        TranslateModule,
    ],
    declarations: [NavbarComponent],
    exports: [NavbarComponent],
})
export class NavbarModule { }
