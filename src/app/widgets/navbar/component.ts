import { Component, OnDestroy, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { ISubscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import cloneDeep from 'lodash/cloneDeep';

import { addNotification, setUserRole, clearModulesSchema } from 'app/actions';
import { NOTIFICATION_TYPE_SUCCESS } from 'app/constants';
import { LS_SESSIONID } from 'app/services/constants/CustomHttpService.constants';
import { ManagerChannelWsService } from 'app/services/ManagerChannelWsService';
import { UIService } from 'app/services/UIService';
import { AppStateInterface } from 'app/store';
import { Storages } from 'app/utilites/Storages';

@Component({
    selector: 'vce-navbar',
    styleUrls: ['./style.scss'],
    templateUrl: './template.html',
})

export class NavbarComponent implements OnInit, OnDestroy {
    title = '';
    navbarState = false;
    countNewClaim = 0;
    userRole: string;
    isOpenSidebar = false;

    userProfile$: Observable<any>;

    private isManagerChanelSubscribe: boolean;
    private navbarStateSubscription: ISubscription;
    private managerCWSSubcription: ISubscription;
    private managerCWSIsSubSubscription: ISubscription;

    constructor(
        private managerCWS: ManagerChannelWsService,
        private store$: Store<AppStateInterface>,
        private uiService: UIService,
        private route: Router,
        private translate: TranslateService,
    ) {}

    ngOnInit() {

        this.managerCWSIsSubSubscription = this.managerCWS.subscribed.subscribe((data) => {
            this.isManagerChanelSubscribe = cloneDeep(data);
        });
        
        this.store$.select(state => state.userRole).subscribe((role) => {
            this.userRole = cloneDeep(role);
        });

        this.userProfile$ = this.store$.select(state => state.profileData);
        
        this.navbarStateSubscription = this.uiService.getIsLogginedState().subscribe((state) => {
            if (state) {
                this.managerCWS.identityId = Storages.getSessionItem(LS_SESSIONID);
                this.subscribeToManagerChannel();
            } else {
                this.unsubscribeToMAnagerChannel();
            }

            this.navbarState = cloneDeep(state);
        });
    }

    logout() {
        this.uiService.updateSettingsVisible(false);
        
        Storages.clearSessionInfo();
        this.store$.dispatch(setUserRole(null));
        this.store$.dispatch(clearModulesSchema());
        window.location.href = '/';
    }
    
    ngOnDestroy() {
        if (this.managerCWSSubcription) {
            this.managerCWSSubcription.unsubscribe();
        }

        this.navbarStateSubscription.unsubscribe();
        this.managerCWSIsSubSubscription.unsubscribe();
    }

    menuActivate() {
        this.isOpenSidebar = !this.isOpenSidebar;
        this.uiService.updateSidebarVisible(this.isOpenSidebar);
    }

    goToMain() {
        window.location.href = '/';
    }
    
    private subscribeToManagerChannel() {
        if (!this.isManagerChanelSubscribe) {
            this.managerCWS.subscribe();
        }
    }

    private unsubscribeToMAnagerChannel() {
        if (this.isManagerChanelSubscribe) {
            this.managerCWS.unsubscribe();
        }
    }
}
