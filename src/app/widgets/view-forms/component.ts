import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import { ISubscription } from 'rxjs/Subscription';
import get from 'lodash/get';
import cloneDeep from 'lodash/cloneDeep';
// import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

import { FORM_TYPES , NOTIFICATION_TYPE_ERROR } from 'app/constants';
import { AppStateInterface } from 'app/store';
import { addNotification, getFile, getSystemLabelLink } from 'app/actions';
import { UIService } from 'app/services/UIService';

// @AutoUnsubscribe()
@Component({
    selector: 'vce-view-forms',
    templateUrl: './template.html',
    styleUrls: ['./style.scss'],
})
export class VCEViewFormsComponent implements OnInit, OnDestroy {
    @Input() field: any;
    @Input() width = '100%';
    
    validationErrors: string[];
    displayedValue: string[] = [];
    passwordHide = true;
    isOpenMarkdown = false;

    private systemLabelLinkSubscription: ISubscription;
    private profileDataSubscription: ISubscription;
    
    constructor(
        protected store$: Store<AppStateInterface>,
        private activatedRoute: ActivatedRoute,
        private translate: TranslateService,
        private uiService: UIService,
        private router: Router,
    ) {}

    ngOnInit(): void {
        this.validationErrors = this.validate(this.field);
        if (this.validationErrors.length !== 0) {
            this.validationErrors.forEach((element) => {
                this.translate.get(element).subscribe((value) => {
                    this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                });
            });
        }

        if (this.field.value) {
            if (this.field.display_type === 'date') {
                this.field.value = this.uiService.setDateFormat(new Date(this.field.value));
            }
    
            if (this.field.display_type === 'datetime') {
                this.field.value = this.uiService.setDateTimeFormat(new Date(this.field.value));
            }
            if (this.field.display_type === 'multi_label_link') {
                this.field.value = this.field.value.map(row => row.label).join(', ');
            }
        }

        if (this.field.list_data && this.field.list_data.length > 0) {
            this.field.list_data.forEach((element) => {
                if ((this.field.value instanceof Array && this.field.value.includes(element.value)) || element.value === this.field.value) {
                    this.translate.get(element.label).subscribe(value => this.displayedValue.push(value));
                }
            });
            if (this.displayedValue.length === 0 && this.field.value) {
                this.translate.get(this.field.value).subscribe(value => this.displayedValue.push(value));
            }
        }

        if (this.field.display_type === 'price') {
            this.profileDataSubscription = this.store$.select(state => state.profileData).subscribe((data) => {
                const currencyId = cloneDeep(get(data, ['data', 'main', 'fields', 'currency_id', 'value'], null));
                if (currencyId) {
                    this.store$.dispatch(getSystemLabelLink('ichain_currencies'));
                    this.systemLabelLinkSubscription = this.store$.select(state => state.systemLabelLink).subscribe((systemData) => {
                        const currency = get(systemData, ['ichain_currencies', 'data'], []).filter(element => element.id === currencyId)[0];
                        if (currency) {
                            this.field.shortLabel = currency.shortName;
                        }
                    });
                }
            });
        }
    }

    validate(field: any): string[] {
        const errors = [];
        let fieldLabel = field.label;
        if (fieldLabel) {
            this.translate.get(fieldLabel).subscribe((value) => {
                fieldLabel = value;
            });
        }

        if (!field.display_type || !FORM_TYPES.includes(field.display_type)) {
            this.translate.get('ERRORS.INVALID_TYPE_FIELD', { label: fieldLabel }).subscribe((value) => {
                errors.push(value);
            });
        }

        if (!fieldLabel) {
            this.translate.get('ERRORS.INVALID_LABEL_FIELD').subscribe((value) => {
                errors.push(value);
            });
        }

        return errors;
    }

    goToUser(id: number) {
        this.router.navigate([`./profile/${id}`]);
    }

    goToLink() {
        if (this.field.module) {
            this.router.navigate([`/modules/${this.field.module}/view/${this.field.value}`]);
        }
    }

    goToLabelLink() {
        if (this.field.module && this.field.value.enabled) {
            this.router.navigate([`/modules/${this.field.module}/view/${this.field.value.value}`]);
        }
    }

    goToExternalLink() {
        if (this.field.value.enabled) {
            window.open(this.field.value.url);
        }
    }

    downloadFiles() {
        if (this.field.value) {
            this.store$.dispatch(getFile(this.field.value));
        }
    }

    openMarkdown () {
        this.isOpenMarkdown = !this.isOpenMarkdown;
    }

    closeMarkdown() {
        this.isOpenMarkdown = false;
    }

    ngOnDestroy(): void {
    }
}
