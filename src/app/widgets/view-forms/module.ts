import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DateAdapter, MatButtonModule, MatCheckboxModule, MatChipsModule, MatIconModule, MatDatepickerModule, MatInputModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { VCEDateAdapter } from 'app/utilites/VCEDateAdapter';

import { VCEViewFormsComponent } from './component';
import { CopyClickModule } from 'app/directives/copy-click/module';
import { MarkdownModule } from 'app/widgets/markdown/module';
import { ModalModule } from 'app/widgets/modal/module';

@NgModule({
    imports: [
        MatDatepickerModule,
        MatCheckboxModule,
        MatButtonModule,
        TranslateModule,
        MatInputModule,
        CopyClickModule,
        MatChipsModule,
        MatIconModule,
        CommonModule,
        FormsModule,
        MarkdownModule,
        ModalModule,

    ],
    declarations: [VCEViewFormsComponent],
    exports: [VCEViewFormsComponent],
    providers: [
        {
            provide: DateAdapter, 
            useClass: VCEDateAdapter,
        },
    ],
})

export class VCEViewFormsModule {}
