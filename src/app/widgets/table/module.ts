import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { 
    MatButtonModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatMenuModule,
    MatSortModule,
    MatIconModule,
    MatCheckboxModule,
    MatInputModule,
    MatTooltipModule,
    MatPaginatorIntl,
    MatSelectModule,
} from '@angular/material';

import { ToArrayModule } from 'app/directives/toArray.pipe';
import { TableComponent } from './components/table/component';
import { SimpleTableComponent } from './components/simpleTable/component';
import { PaginatorI18n } from 'app/utilites/PaginatorI18n';

@NgModule({
    imports: [
        RouterModule,
        FormsModule,
        TranslateModule,
        CommonModule,
        ToArrayModule,
        MatButtonModule,
        MatCardModule,
        MatTableModule,
        MatPaginatorModule,
        MatMenuModule,
        MatIconModule,
        MatSortModule,
        MatCheckboxModule,
        MatInputModule,
        MatTooltipModule,
        MatSelectModule,
    ],
    providers: [
        {
            provide: MatPaginatorIntl, deps: [TranslateService],
            useFactory: (translateService: TranslateService) => new PaginatorI18n(translateService).getPaginatorIntl(),
        },
    ],
    declarations: [TableComponent, SimpleTableComponent],
    exports: [TableComponent, SimpleTableComponent],
})

export class TableModule {}
