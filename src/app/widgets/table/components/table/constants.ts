export interface SortDataInterface {
    field?: string;
    type?: string;
}

export const DONT_SHOWING_FIELDS = [
    'viewed', 'data', 'actions', 'linked', 'priceList', 'entityName',
];
