import { Component, Input, Output, EventEmitter, ViewChild, AfterViewInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { ISubscription } from 'rxjs/Subscription';
import { merge } from 'rxjs/observable/merge';
import cloneDeep from 'lodash/cloneDeep';
import { Store } from '@ngrx/store';
import get from 'lodash/get';
import set from 'lodash/set';

import { UIService } from 'app/services/UIService';
import { AppStateInterface } from 'app/store';
import { SortDataInterface } from './constants';

@Component({
    selector: 'vce-table',
    templateUrl: './template.html',
    styleUrls: ['./style.scss'],
})
export class TableComponent implements AfterViewInit, OnDestroy {
    @Input() set data(data) {
        this.listData = data;
        this.initData(data);
    }
    @Input() module: string;
    @Input() loading: boolean;
    @Input() indexKey = 'id';
    @Input() mainSectionKey = 'main';
    @Input() filterInitData = {};
    @Input() checked: number[];
    @Input() moduleSchema: any;
    @Input() needCheckedColumn = true;
    @Input() header: string[] = [];
    @Input() displayHeader: string[] = [];
    @Input() filteredRow: string[] = [];
    @Input() approvedHeader = {};
    @Input() labels = {};
    @Input() filterData = [];
    @Input() modalMode = false;

    @Output() rowAction = new EventEmitter<any>();
    @Output() multiRowAction = new EventEmitter<any>();
    @Output() pageAction = new EventEmitter<any>();
    @Output() clickRowAction = new EventEmitter<any>();

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    dataSource = new MatTableDataSource<any>();
    dataSourceFilter = new MatTableDataSource<any>();
    total = 0;
    itemsPerPage = 0;
    isFiltered = false;
    filteredFields = [];
    selectedRow = [];
    sortData: SortDataInterface = {};
    fieldSchema = {};
    displayedValues = [];
    listData: any;
    checkedAll = false;
    currentModule: string;
    
    private page = 0;
    private fieldIndex = [];
    private paginationSubscription: ISubscription;
    private firstSelect = true;

    constructor(
        private translate: TranslateService,
        private uiService: UIService,
        private store$: Store<AppStateInterface>,
        private cdRef: ChangeDetectorRef,
    ) { }

    ngAfterViewInit() {
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        if (this.paginator) {
            this.paginationSubscription = merge(this.sort.sortChange, this.paginator.page).subscribe(() => {
                this.loading = true;
                this.setDefDisplayHeader();
                this.emitTable();
            });
        }

        if (Object.getOwnPropertyNames(this.filterInitData).length > 0) {
            this.setFilterValue();
            this.cdRef.detectChanges();
        }

        this.firstSelect = false;
        this.initData(this.listData);
        this.filteredFields = cloneDeep(this.filteredRow);
    }

    ngOnDestroy() {
        if (this.paginationSubscription) {
            this.paginationSubscription.unsubscribe();
        }
    }

    renderNames(props) {
        return props;
    }

    actionEvent(action, value) {
        this.rowAction.emit({
            ...action,
            rowID: value,
        });
    }

    resetFilter() {
        this.filteredFields.forEach((key) => {
            this.dataSourceFilter.data[0][key] = '';
        });
        this.searchFilter();
    }

    changeColumnDisplay(event, prop) {
        // this.approvedHeader[prop] = cloneDeep(event.checked);
        this.approvedHeader[prop] = cloneDeep(!this.approvedHeader[prop]);
        this.setDefDisplayHeader();
        this.filteredRow = cloneDeep(this.displayHeader);
    }

    viewFilter() {
        this.isFiltered = !this.isFiltered;
        if (this.isFiltered) {
            this.dataSourceFilter.data = [
                {},
            ];
            this.filteredFields.forEach((key) => {
                this.dataSourceFilter.data[0][key] = '';
            });
        } else {
            this.dataSourceFilter.data = [];
        }
    }

    searchFilter() {
        this.paginator.pageIndex = 0;
        this.emitTable();
    }

    keyDownFunction(event) {
        if (event.keyCode === 13) {
            this.searchFilter();
        }
    }

    toggleAll() {
        this.checkedAll = !this.checkedAll;
        this.dataSource.data.forEach((element) => {
            element.checked = this.checkedAll;
            if (this.checkedAll) {
                this.checked.push(element.id);
            }
        });
        if (!this.checkedAll) {
            this.checked.length = 0;
        }
    }

    toggleOne(entityId) {
        const entity = this.dataSource.data.filter(element => element.id === entityId)[0];
        entity.checked = !entity.checked;
        if (entity.checked) {
            this.checked.push(entityId);
        } else {
            this.checked.splice(this.checked.indexOf(entityId), 1);
        }
    }

    clickRow(element) {
        this.clickRowAction.emit(element);
    }

    private initData(data) {
        if (!this.firstSelect) {
            Object.keys(this.moduleSchema.data).forEach((section) => {
                Object.keys(this.moduleSchema.data[section].fields).forEach((field) => {
                    const obj = this.moduleSchema.data[section].fields[field];
                    this.fieldSchema[obj.label] = obj;
                });
            });

            if (data) {
                if (this.needCheckedColumn) {
                    this.checked.length = 0;
                    this.checkedAll = false;
                }
                data.data.forEach((element, key) => {
                    this.displayHeader.forEach((schemaKey) => {
                        const field = this.fieldSchema[this.labels[schemaKey]];
                        if (field && field.list_data && field.list_data.length > 0) {
                            set(this.displayedValues, [element.id, schemaKey], []);
                            field.list_data.forEach((listElement) => {
                                if ((element.data[schemaKey] instanceof Array && element.data[schemaKey].includes(listElement.value)) || 
                                    listElement.value === element.data[schemaKey]) {
                                    this.translate.get(listElement.label).subscribe(value => this.displayedValues[element.id][schemaKey].push(value));
                                }
                            });
                            if (this.displayedValues[element.id][schemaKey].length === 0 && element.data[schemaKey]) {
                                this.translate.get(element.data[schemaKey]).subscribe(value => this.displayedValues[element.id][schemaKey].push(value));
                            }
                        }
                    });
                    data.data[key] = {
                        id: element.id,
                        name: element.entityName,
                        ...element.data,
                        actions: element.actions,
                        checked: false,
                    };
                });
                this.sortData = cloneDeep(data.sort);
                this.dataSource.data = data.data;
                this.total = data.total;
                this.itemsPerPage = data.itemsPerPage;
                if (this.paginator) {
                    this.paginator.pageIndex = data.page - 1;
                }
                this.fieldIndex = this.getFieldIndex(data.data);
            }
            this.currentModule = this.module;
        }
    }

    private setFilterValue() {
        this.viewFilter();
        Object.keys(this.filterInitData).forEach((key) => {
            this.dataSourceFilter.data[0][key] = this.filterInitData[key];
        });
    }

    private getFieldIndex(data: any[]): any[] {
        const index = [];

        data.forEach((element) => {
            index.push(element[this.indexKey]);
        });

        return index;
    }

    private emitTable() {
        const filter = get(this, ['dataSourceFilter', 'data', 0], {});
        const sendFilter = [];
        Object.keys(filter).forEach((key) => {
            if (filter[key]) {
                const filterRow = this.filterData[key];
                const obj = {
                    value: filter[key],
                    type: !filterRow || filterRow.filterType === 'string' ? 'like' : 'equal',
                };
                obj['key'] = key;
                sendFilter.push(obj);
            }
        });
        this.pageAction.emit({
            sort: {
                field: this.sort.active,
                type: this.sort.direction,
            },
            page: this.paginator.pageIndex + 1,
            itemsPerPage: this.paginator.pageSize,
            filters: sendFilter,
        });
    }

    private setDefDisplayHeader() {
        this.displayHeader = this.header.filter(val => this.approvedHeader[val]);
    }

}
