import { Component, Input, Output, EventEmitter, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { ISubscription } from 'rxjs/Subscription';
import { merge } from 'rxjs/observable/merge';
import { Store } from '@ngrx/store';
import get from 'lodash/get';
import cloneDeep from 'lodash/cloneDeep';

import { UIService } from 'app/services/UIService';
import { AppStateInterface } from 'app/store';
import { SortDataInterface } from './constants';

@Component({
    selector: 'vce-simple-table',
    templateUrl: './template.html',
    styleUrls: ['./style.scss'],
})
export class SimpleTableComponent implements OnDestroy {
    @Input() set data(data) {
        this.initData(data);
    }
    @Input() module: string;
    @Input() loading: boolean;
    @Input() indexKey = 'id';
    @Input() filterInitData = {};
    @Input() linkedModule: string;

    @Output() rowAction = new EventEmitter<any>();
    @Output() multiRowAction = new EventEmitter<any>();
    @Output() pageAction = new EventEmitter<any>();

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    dataSource = new MatTableDataSource<any>();
    dataSourceFilter = new MatTableDataSource<any>();
    header: string[] = [];
    displayHeader: string[] = [];
    filteredRow: string[] = [];
    approvedHeader = {};
    total = 0;
    itemsPerPage = 0;
    isFiltered = false;
    isCheckedAll = false;
    filteredFields = {};
    selectedRow = [];
    sortData: SortDataInterface = {};
    
    private page = 0;
    private fieldIndex = [];
    private mediaScreenSubscription: ISubscription;
    private paginationSubscription: ISubscription;
    private firstSelect = true;

    constructor(
        private translate: TranslateService,
        private uiService: UIService,
        private store$: Store<AppStateInterface>,
    ) { }

    ngOnDestroy() {
        if (this.mediaScreenSubscription) {
            this.mediaScreenSubscription.unsubscribe();
        }
        if (this.paginationSubscription) {
            this.paginationSubscription.unsubscribe();
        }
    }

    actionEvent(action, value) {
        this.rowAction.emit({
            action,
            value,
        });
    }

    resetFilter() {
        Object.keys(this.filteredFields).forEach((key) => {
            this.dataSourceFilter.data[0][key] = '';
        });
        this.searchFilter();
    }

    changeColumnDisplay(event, prop) {
        this.approvedHeader[prop] = cloneDeep(!this.approvedHeader[prop]);
        this.setDefDisplayHeader();
        this.filteredRow = cloneDeep(this.displayHeader);
    }

    viewFilter() {
        this.isFiltered = !this.isFiltered;
        if (this.isFiltered) {
            this.dataSourceFilter.data = [
                {},
            ];
            Object.keys(this.filteredFields).forEach((key) => {
                this.dataSourceFilter.data[0][key] = '';
            });
        } else {
            this.dataSourceFilter.data = [];
        }
    }

    searchFilter() {
        this.paginator.pageIndex = 0;
        this.emitTable();
    }

    keyDownFunction(event) {
        if (event.keyCode === 13) {
            this.searchFilter();
        }
    }

    private initData(data) {
        if (data && data.length > 0 || !this.firstSelect) {
            if (!get(this.header, [0], false)) {
                this.firstSelect = false;
                this.header = [
                ];
                Object.keys(data[0]).forEach((key) => {
                    if (key !== 'viewed') {
                        this.header.push(key);
                        this.approvedHeader[key] = true;
                    }
                });
                this.displayHeader = cloneDeep(this.header);
                this.filteredRow = cloneDeep(this.header);
                this.setCountColumn();
                if (Object.getOwnPropertyNames(this.filterInitData).length > 0) {
                    this.setFilterValue();
                }
            }
            this.dataSource.data = data;
            this.fieldIndex = this.getFieldIndex(data);
        }
    }

    private setFilterValue() {
        this.viewFilter();
        Object.keys(this.filterInitData).forEach((key) => {
            this.dataSourceFilter.data[0][key] = this.filterInitData[key];
        });
    }

    private setCountColumn() {
        this.mediaScreenSubscription = this.store$.select(state => state.mediaScreen).subscribe((screen) => {
            switch (screen) {
                case 'xs': {
                    this.showColumn(2);
                    break;
                }
                case 'sm': {
                    this.showColumn(4);
                    break;
                }
                default: {
                    this.showColumn();
                    break;
                }
            }
        });
    }

    private showColumn(count?: number) {
        let k = 0;
        Object.keys(this.approvedHeader).forEach((key) => {
            k += 1;
            if (!count || k <= count || key === 'actions') {
                this.approvedHeader[key] = true;
            } else {
                this.approvedHeader[key] = false;
            }
        });
        this.setDefDisplayHeader();
        this.filteredRow = cloneDeep(this.displayHeader);
    }

    private getFieldIndex(data: any[]): any[] {
        const index = [];

        data.forEach((element) => {
            index.push(element[this.indexKey]);
        });

        return index;
    }

    private emitTable() {
        const filter = get(this, ['dataSourceFilter', 'data', 0], {});
        const sendFilter = {};
        Object.keys(filter).forEach((key) => {
            if (filter[key]) {
                sendFilter[key] = cloneDeep(filter[key]);
            }
        });
        this.pageAction.emit({
            sort: {
                field: this.sort.active,
                type: this.sort.direction,
            },
            page: this.paginator.pageIndex + 1,
            itemsPerPage: this.paginator.pageSize,
            filter: sendFilter,
        });
    }

    private setDefDisplayHeader() {
        this.displayHeader = this.header.filter(vall => this.approvedHeader[vall]);
    }

}
