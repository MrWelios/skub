import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { DateIntervalComponent } from './component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
    ],
    providers: [
        // LoginService,
    ],
    declarations: [
        DateIntervalComponent,
    ],
    exports: [DateIntervalComponent],
})

export class DateIntervalModule {

}
