export const MONTH_NAMES = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
];

export const WEEK_DAY = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
];

export const TABLE_DAY = [
    'Sun',
    'Mon',
    'Tue',
    'Wed',
    'Thu',
    'Fri',
    'Sat',
];

export interface CalendarInterface {
    date: number;
    isCurrentMonth: boolean;
    isCurrentDay: boolean;
}

export interface DisplayedCalendarMonthInterface {
    calendar: CalendarInterface[][];
    year: number;
    monthName: string;
}
