import { Component, Output, Input, OnInit, EventEmitter } from '@angular/core';
import get from 'lodash/get';
import cloneDeep from 'lodash/cloneDeep';

import { MONTH_NAMES, WEEK_DAY, TABLE_DAY, DisplayedCalendarMonthInterface, CalendarInterface } from './constants';
import { UIService } from 'app/services/UIService';

@Component({
    selector: 'vce-date-interval',
    templateUrl: './template.html',
    styleUrls: ['./style.scss'],
})

export class DateIntervalComponent implements OnInit {
    @Input() fromDate: string;
    @Input() toDate: string;

    @Output() fromDateChange = new EventEmitter<string>();
    @Output() toDateChange = new EventEmitter<string>();
    @Output() closeEvent = new EventEmitter<boolean>();

    firstSelect = true;
    monthNames = MONTH_NAMES;
    weekdays = WEEK_DAY;
    tableDays = TABLE_DAY;
    currentDate = (new Date()).setHours(0, 0, 0, 0);

    displayMonth1: number;
    displayYear1: number;
    month1: any;

    displayMonth2: number;
    displayYear2: number;
    month2: any;

    constructor(
        private uiService: UIService,
    ) { }

    ngOnInit(): void {
        const dates = this.fromDate.split('-');
        const current = new Date(dates[0] + '/' + dates[1] + '/' + dates[2]);

        current.getUTCMonth();
        this.displayMonth1 = current.getMonth();
        this.displayYear1 = current.getFullYear();
        this.month1 = this.generateMonth(this.displayMonth1, this.displayYear1);

        if (this.displayMonth1 >= 11) {
            this.displayMonth2 = 0;
            this.displayYear2 = this.displayYear1 + 1;
        } else {
            this.displayMonth2 = this.displayMonth1 + 1;
            this.displayYear2 = this.displayYear1;
        }

        this.month2 = this.generateMonth(this.displayMonth2, this.displayYear2);
    }

    /**
     * selecting fromDate | toDate
     * 
     * @param {Number} day 
     * @param {String} mont 
     * @param {Number} year  
     */
    selectDateInterval(day: number, mont: string, year: number): void {
        const date = new Date(year + '/' + (this.monthNames.indexOf(mont) + 1) + '/' + day);
        const dates = this.fromDate.split('-');
        const current = new Date(dates[0] + '/' + dates[1] + '/' + dates[2]);

        if (this.firstSelect || date.valueOf() <= current.valueOf()) {
            this.firstSelect = false;
            this.fromDate = cloneDeep(this.uiService.setDateFormat(date));
            this.toDate = cloneDeep(this.uiService.setDateFormat(date));
        } else {
            this.firstSelect = true;
            this.toDate = cloneDeep(this.uiService.setDateFormat(date));
        }

    }

    /**
     * returning timestamp numeric value
     * 
     * @param {Number} day 
     * @param {String} mont 
     * @param {Number} year 
     * 
     * @return {Timestamp}
     */
    getTimestamp(day: number, mont: string, year: number): number {
        const date = new Date(year + '/' + (this.monthNames.indexOf(mont) + 1) + '/' + day);
        return date.valueOf();
    }

    getDate(date: string): Date {
        if (typeof date === 'string') {
            const dates = date.split('-');
            return new Date(dates[0] + '/' + dates[1] + '/' + dates[2]);
        }
        return new Date(date);
    }

    /**
     * close widget
     */
    close(): void {
        this.closeEvent.emit(false);
    }

    /**
     * emitting fromDate and toDate to parent component and close widget
     */
    save(): void {
        this.fromDateChange.emit(this.fromDate);
        this.toDateChange.emit(this.toDate);
        this.close();
    }

    /**
     * Reduce the displayed months by one
     */
    prewMont(): void {
        this.month2 = cloneDeep(this.month1);
        this.displayMonth2 = cloneDeep(this.displayMonth1);
        this.displayYear2 = cloneDeep(this.displayYear1);

        if (this.displayMonth1 <= 0) {
            this.displayMonth1 = 11;
            this.displayYear1 -= 1;
        } else {
            this.displayMonth1 -= 1;
        }
        this.month1 = this.generateMonth(this.displayMonth1, this.displayYear1);
    }

    /**
     * Increase the displayed months by one
     */
    nextMonth(): void {
        this.month1 = cloneDeep(this.month2);
        this.displayMonth1 = cloneDeep(this.displayMonth2);
        this.displayYear1 = cloneDeep(this.displayYear2);

        if (this.displayMonth2 >= 11) {
            this.displayMonth2 = 0;
            this.displayYear2 += 1;
        } else {
            this.displayMonth2 += 1;
        }
        this.month2 = this.generateMonth(this.displayMonth2, this.displayYear2);
    }

    /**
     * @typedef {Object} Month returning in generateMont function
     * @property {CalendarInterface[][]} calendar - an array of weeks that contain days
     * @property {Number} year - the year that owns the month
     * @property {String} monthName - displayed mont name
     */
    
    /**
     * generates the days of the month to display them in the calendar
     * @param {Number} month
     * @param {Number} year
     * 
     * @returns {Month}
     */
    private generateMonth(month: number, year: number): DisplayedCalendarMonthInterface {
        let totalFeb = null;

        if ((year % 100 !== 0) && (year % 4 === 0) || (year % 400 === 0)) {
            totalFeb = 29;
        } else {
            totalFeb = 28;
        }

        const totalDays = [31, totalFeb, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        const tempDate = new Date(year + '/' + (month + 1) + '/1');
        const prewMonth = month !== 0 ? month - 1 : 11;
        const nextMonth = month !== 11 ? month + 1 : 0;

        let tempweekday = tempDate.getDay();
        let tmpWeek = 0;
        
        const calendar: CalendarInterface[][] = [[]];
        const curDate = new Date();
        const curMonth = curDate.getMonth();
        const curDay = curDate.getDate();
        const curYear = curDate.getFullYear();

        for (let i = (tempweekday - 1); i >= 0; i -= 1) {
            calendar[tmpWeek].push({
                date: totalDays[prewMonth] - i,
                isCurrentMonth: false,
                isCurrentDay: false,
            });
        }
        for (let i = 1; i <= totalDays[month]; i += 1) {
            if (tempweekday > 6) {
                tempweekday = 0;
                tmpWeek += 1;
                calendar.push([]);
            }
            calendar[tmpWeek].push({
                date: i,
                isCurrentMonth: true,
                isCurrentDay: curYear === year && curMonth === month && curDay === i ? true : false,
            });
            tempweekday += 1;
        }
        let tmpDate = 1;
        for (let i = tempweekday; i <= 6; i += 1) {
            calendar[tmpWeek].push({
                date: tmpDate,
                isCurrentMonth: false,
                isCurrentDay: false,
            });
            tmpDate += 1;
        }
        return {
            calendar,
            year,
            monthName: this.monthNames[month],
        };
    }

}
