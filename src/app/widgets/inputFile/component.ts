import { Subject } from 'rxjs/Subject';
import { Component, OnInit, Input, ElementRef, OnDestroy, HostBinding, Renderer2, HostListener, Output, EventEmitter } from '@angular/core';
import { ControlValueAccessor, NgControl } from '@angular/forms';
import { MatFormFieldControl } from '@angular/material';
import { FocusMonitor } from '@angular/cdk/a11y';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { Store } from '@ngrx/store';
import { ISubscription } from 'rxjs/Subscription';
import get from 'lodash/get';

import { FileInput } from './model';
import { AppStateInterface } from '../../store';
import { startFileUpload } from '../../actions';
import { addNotification } from 'app/actions';
import { NOTIFICATION_TYPE_ERROR } from 'app/constants';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'vce-input-file',
    templateUrl: './template.html',
    styleUrls: ['./style.scss'],
    providers: [
        { 
            provide: MatFormFieldControl, 
            useExisting: InputFileComponent,
        },
    ],
})

export class InputFileComponent implements MatFormFieldControl<FileInput>, ControlValueAccessor, OnInit, OnDestroy {
    static nextId = 0;
    
    stateChanges = new Subject<void>();
    focused = false;
    controlType = 'file-input';
    
    @Input() fieldName: string;
    @Input() valuePlaceholder: string;
    @Input() multiple: boolean;
    @Input() allowedTypes: string[];
    @Input() inputFilenames: string[];

    @HostBinding() id = `app-input-file-${InputFileComponent.nextId += 1}`;
    @HostBinding('attr.aria-describedby') describedBy = '';

    @Input() get value(): FileInput | null {
        return this.empty ? null : new FileInput(this._elementRef.nativeElement.value || []);
    }
    set value(fileInput: FileInput | null) {
        if (fileInput) {
            this.writeValue(fileInput.files);
            this.sendFiles(fileInput.files);
        }
            
        this.stateChanges.next();
    }

    @Input() get placeholder() {
        return this._placeholder;
    }
    set placeholder(plh) {
        this._placeholder = plh;
        this.stateChanges.next();
    }

    get empty() {
        return !this._elementRef.nativeElement.value || this._elementRef.nativeElement.value.length === 0;
    }

    @HostBinding('class.mat-form-field-should-float') get shouldLabelFloat() {
        return this.focused || !this.empty || this.valuePlaceholder !== undefined;
    }

    @Input() get required() {
        return this._required;
    }
    set required(req: boolean) {
        this._required = coerceBooleanProperty(req);
        this.stateChanges.next();
    }

    @HostBinding('class.file-input-disabled') get isDisabled() {
        return this.disabled;
    }
    @Input() get disabled() {
        return this._elementRef.nativeElement.disabled;
    }
    set disabled(dis: boolean) {
        this.setDisabledState(coerceBooleanProperty(dis));
        this.stateChanges.next();
    }

    @Input() get errorState() {
        return this.ngControl.errors !== null && this.ngControl.touched;
    }

    get fileNames() {
        return this.value ? this.value.fileNames : (this.inputFilenames ? this.inputFilenames.join(', ') :  this.valuePlaceholder);
    }

    @Output() returnCodes = new EventEmitter<string[]>();

    progress = 0;
    error: string = null;

    private _placeholder: string;
    private _required = false;
    private loadingFileSubscription: ISubscription;

    constructor(
        public ngControl: NgControl,
        private fm: FocusMonitor, 
        private _elementRef: ElementRef, 
        private _renderer: Renderer2,
        private store$: Store<AppStateInterface>,
        private translate: TranslateService,
    ) {
        ngControl.valueAccessor = this;
        // fm.monitor(_elementRef.nativeElement, _renderer, true).subscribe((origin) => {
        fm.monitor(_elementRef.nativeElement, true).subscribe((origin) => {
            this.focused = !!origin;
            this.stateChanges.next();
        });
    }

    setDescribedByIds(ids: string[]) {
        this.describedBy = ids.join(' ');
    }

    onContainerClick(event: MouseEvent) {
        if ((event.target as Element).tagName.toLowerCase() !== 'input' && !this.disabled) {
            this._elementRef.nativeElement.querySelector('input').focus();
            this.focused = true;
            this.open();
        }
    }

    writeValue(obj: any): void {
        this._renderer.setProperty(this._elementRef.nativeElement, 'value', obj);
    }

    registerOnChange(fn: (_: any) => void): void {
        this._onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this._onTouched = fn;
    }

    @HostListener('change', ['$event']) change(event) {
        const fileList = event.target.files;
        const fileArray = [];
        if (fileList) {
            for (let i = 0; i < fileList.length; i += 1) {
                fileArray.push(fileList[i]);
            }
        }
        this.value = new FileInput(fileArray);
        this._onChange(this.value);
    }

    @HostListener('focusout') blur() {
        this.focused = false;
        this._onTouched();
    }

    setDisabledState?(isDisabled: boolean): void {
        this._renderer.setProperty(this._elementRef.nativeElement, 'disabled', isDisabled);
    }

    ngOnInit() {
        this.multiple = coerceBooleanProperty(this.multiple);

        this.loadingFileSubscription = this.store$.select(state => state.loadingFile).subscribe((filesState) => {
            this.progress = get(filesState, [this.fieldName, 'percentage']);
            this.error = get(filesState, [this.fieldName, 'error']);
            
            const codes = get(filesState, [this.fieldName, 'codes']);
            if (codes) {
                this.returnCodes.emit(codes);
            }
        });
    }

    open() {
        if (!this.disabled) {
            this._elementRef.nativeElement.querySelector('input').click();
        }
    }

    sendFiles(files, event?) {
        let isSuccessCheckType = true;
        if (event) {
            event.stopPropagation();
        }
        if (files.length > 0) {
            const formData = new FormData();
            formData.append('filename', this.fieldName);
            files.forEach((document) => {
                if (!this.allowedTypes || this.allowedTypes.includes(document.type) || (document.type === '' && document.name.indexOf('.doc') !== -1)) {
                    formData.append('documents', document);
                } else {
                    this.translate.get('ERRORS.UNSUPPORTED_TYPEFILES').subscribe((value) => {
                        this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                    });
                    isSuccessCheckType = false;
                    return false;
                }
            });
            if (isSuccessCheckType) {
                this.store$.dispatch(startFileUpload(this.fieldName, formData));
            }
        }
    }

    ngOnDestroy() {
        this.loadingFileSubscription.unsubscribe();
        this.stateChanges.complete();
        this.fm.stopMonitoring(this._elementRef.nativeElement);
    }

    private _onChange = (_: any) => { };
    private _onTouched = () => { };

}
