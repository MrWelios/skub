import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, NG_VALUE_ACCESSOR, NgControl, ReactiveFormsModule } from '@angular/forms';
import { 
    MatButtonModule, 
    MatFormFieldModule, 
    MatIconModule, 
    MatInputModule, 
    MatProgressSpinnerModule, 
    MatTooltipModule,
} from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';

import { InputFileComponent } from './component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatIconModule,
        MatProgressSpinnerModule,
        MatTooltipModule,
        TranslateModule,
    ],
    providers: [
        { 
            provide: NgControl, 
            useValue: NG_VALUE_ACCESSOR, 
        },
    ],
    declarations: [InputFileComponent],
    exports: [InputFileComponent],
})

export class InputFileModule { }
