import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule, MatButtonModule, MatSelectModule, MatFormFieldModule, MatInputModule, MatTableModule, MatTooltipModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { VCEFiltersComponent } from './component';
import { ModalModule } from '../modal/module';

@NgModule({
    imports: [
        MatIconModule,
        MatButtonModule,
        MatSelectModule,
        MatFormFieldModule,
        MatInputModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        ModalModule,
        MatTableModule,
        MatTooltipModule,
    ],
    declarations: [VCEFiltersComponent],
    exports: [VCEFiltersComponent],
})

export class VCEFiltersModule {}
