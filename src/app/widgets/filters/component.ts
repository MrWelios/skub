import { Component, Input, OnDestroy, OnInit, Output, EventEmitter } from '@angular/core';
import cloneDeep from 'lodash/cloneDeep';
import { MatTableDataSource } from '@angular/material';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import get from 'lodash/get';

import { CustomViewService } from 'app/services/CustomViewService';
import { addNotification } from 'app/actions';
import { NOTIFICATION_TYPE_SUCCESS, NOTIFICATION_TYPE_ERROR } from 'app/constants';
import { AppStateInterface } from 'app/store';

@Component({
    selector: 'vce-filters',
    templateUrl: './template.html',
    styleUrls: ['./style.scss'],
})
export class VCEFiltersComponent implements OnInit, OnDestroy {
    @Input() set moduleSchema(moduleSchema) {
        this.schema = moduleSchema;
        this.getFilters(false);
    }
    @Output() filterEvent = new EventEmitter<any>();
    schema: any;
    filters: any;
    fields = [];
    currentFilterId: number;
    editFilter: any;
    isOpenEditModal = false;
    isOpenDeleteModal = false;
    dataSource: MatTableDataSource<any>;
    displayedColumns = ['label', 'type', 'value', 'removeBtn'];
    defaultFilter: any;

    constructor(
        protected store$: Store<AppStateInterface>,
        private customViewService: CustomViewService,
        private translate: TranslateService,
    ) {}

    ngOnInit(): void {       
    }

    apply(refresh: boolean) {
        const data = { filter: this.filters.filter(element => element.id === this.currentFilterId)[0] };
        data['refresh'] = refresh;
        this.filterEvent.emit(data);
    }

    edit() {
        this.editFilter = cloneDeep(this.filters.filter(element => element.id === this.currentFilterId)[0]);
        this.editFilter.fields = Object.keys(this.editFilter.fields).filter((element) => {
            return this.editFilter.fields[element].label !== 'id';
        }).map((it) => {
            if (this.editFilter.fields[it].label !== 'id') {
                this.editFilter.fields[it].field = this.editFilter.fields[it].key;
                return this.editFilter.fields[it];
            }
        });
        this.dataSource = new MatTableDataSource<any>(this.editFilter.fields);
        this.isOpenEditModal = true;
    }

    create() {
        this.editFilter = {
            label: '',
            fields: [],
        };
        this.dataSource = new MatTableDataSource<any>(this.editFilter.fields);
        this.isOpenEditModal = true;
    }

    closeEdit() {
        this.isOpenEditModal = false;
    }

    addRow() {
        const newValue = {
            label: '',
        };
        this.editFilter.fields.push(newValue);
        this.dataSource = new MatTableDataSource<any>(this.editFilter.fields);
    }

    removeRow(rowIndex: number) {
        this.editFilter.fields.splice(rowIndex, 1);
        this.dataSource = new MatTableDataSource<any>(this.editFilter.fields);
    }

    save() {
        const data = {
            id: this.editFilter.id,
            name: this.editFilter.name,
            fields: [],
            filters: [],
        };
        let index = 1;
        this.dataSource.data.forEach((row) => {
            data.fields.push({
                key: row.field,
                order: index,
            }); 
            if (row.type && row.value) {
                data.filters.push({
                    key: row.field,
                    type: row.type,
                    value: row.value,
                });
            }
            index = index + 1;
        });
        this.customViewService.updateData(data)
            .subscribe((response) => {
                this.translate.get('SUCCESS').subscribe((value) => {
                    this.store$.dispatch(addNotification(NOTIFICATION_TYPE_SUCCESS, value));
                });
                this.closeEdit();
                this.getFilters(this.editFilter.active !== undefined && this.editFilter.active);
            });
    }

    modalDelete() {
        this.isOpenDeleteModal = true;
    }

    delete() {
        const currentFilter = this.filters.filter(element => element.id === this.currentFilterId)[0];
        if (currentFilter.active) {
            this.currentFilterId = this.defaultFilter.id;
            this.apply(true);
            currentFilter.active = false;
        }
        this.customViewService.deleteFilter(currentFilter.id)
            .subscribe(
            (response) => {
                this.translate.get('SUCCESS').subscribe((value) => {
                    this.store$.dispatch(addNotification(NOTIFICATION_TYPE_SUCCESS, value));
                });
                this.closeDelete();
                this.getFilters(currentFilter.active);
            }, 
            (error) => {
                this.translate.get(get(error, 'error.error', 'ERROR')).subscribe((value) => {
                    this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                });
            });
    }

    closeDelete() {
        this.isOpenDeleteModal = false;
    }

    changeField(element) {
        element.type = '';
        element.value = '';
    }

    ngOnDestroy(): void {
    }

    private getFilters(refresh = true) {
        this.customViewService.moduleName = this.schema.name;
        this.customViewService.getFilters()
            .subscribe((response) => {
                this.filters = response;
                const currentFilter = this.filters.filter(element => element.active)[0];
                this.currentFilterId = currentFilter ? currentFilter.id : this.filters[0].id;
                this.defaultFilter = this.filters.filter(element => element.builtIn)[0];
                this.customViewService.getFields()
                    .subscribe((fields) => {
                        this.fields = Object.keys(fields).map((field) => {
                            fields[field].key = field;
                            fields[field].label = fields[field].label.slice(-1)[0];
                            return fields[field];
                        });
                    });
                this.apply(refresh);

                this.filters.forEach((filter) => {
                    if (filter.filters) {
                        filter.filters.forEach((subFilter) => {
                            const field = filter.fields.filter(element => element.key === subFilter.key)[0];
                            if (field) {
                                field.type = subFilter.type;
                                field.value = subFilter.value;
                            }
                        });
                    }
                });
            });
    }
}
