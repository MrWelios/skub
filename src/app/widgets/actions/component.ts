import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';

@Component({
    selector: 'vce-actions',
    templateUrl: './template.html',
    styleUrls: ['./style.scss'],
})
export class VCEActionsComponent implements OnInit, OnDestroy {
    @Input() actions: any;
    @Input() enableCancel = true;
    @Output() cancelEvent = new EventEmitter<boolean>();
    @Output() actionEvent = new EventEmitter<any>();

    ngOnInit(): void {
    }

    cancel() {
        this.cancelEvent.emit(true);
    }

    action(item: any) {
        this.actionEvent.emit(item);
    }

    ngOnDestroy(): void {
    }
}
