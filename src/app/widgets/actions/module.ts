import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule, MatIconModule, MatButtonModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { VCEActionsComponent } from './component';
import { ToArrayModule } from 'app/directives/toArray.pipe';

@NgModule({
    imports: [
        MatIconModule,
        MatButtonModule,
        CommonModule,
        FormsModule,
        TranslateModule,
        MatCardModule,
        ToArrayModule,
    ],
    declarations: [VCEActionsComponent],
    exports: [VCEActionsComponent],
})

export class VCEActionsModule {}
