import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ViewBPComponent } from './viewBP.component';
import { ModalModule } from 'app/widgets/modal/module';
import { MatButtonModule } from '@angular/material';

@NgModule({
    imports: [
        RouterModule,
        FormsModule,
        TranslateModule,
        CommonModule,
        ModalModule,
        MatButtonModule,
    ],
    declarations: [ViewBPComponent],
    exports: [ViewBPComponent],
})

export class VCEViewBPModule {}
