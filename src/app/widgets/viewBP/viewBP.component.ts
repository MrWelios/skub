import { Component, OnDestroy, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import bpmnJs from 'bpmn-js';
import * as Modeler from 'bpmn-js/dist/bpmn-modeler.development.js';
import bpmnJsPropertiesPanel from 'bpmn-js-properties-panel';
import camunda from 'bpmn-js-properties-panel/lib/provider/camunda';
import camundaJson from 'camunda-bpmn-moddle/resources/camunda.json';
@Component({
    selector: 'vce-view-bp',
    styleUrls: [
        './viewBP.style.scss',
    ],
    templateUrl: './viewBP.template.html',
})

export class ViewBPComponent implements OnDestroy, AfterViewInit {
    @Input() BPID: string;
    @Input() data: any;
    @Input() isEdit: boolean;

    @Output() onClose = new EventEmitter<void>();
    @Output() onUpdate = new EventEmitter<any>();

    bpmViewer: any;

    constructor(
    ) { }

    ngAfterViewInit() {
        if (this.isEdit) {
            this.bpmViewer =  new Modeler({
                container: '#canvas',
                width: '100%',
                height: '600px',
                propertiesPanel: {
                    parent: '#properties-panel',
                },
                additionalModules: [
                    bpmnJsPropertiesPanel,
                    camunda,
                ],
                moddleExtensions: {
                    camunda: camundaJson,
                },
            });
        } else {
            this.bpmViewer = new bpmnJs({
                container: '#canvas',
                width: '100%',
                height: '600px',
            });
        }

        if (this.data) {
            this.bpmViewer.importXML(this.data.bpmn20Xml, (err) => {
                if (err) {
                    console.log(err);
                } else {
                    const canvas = this.bpmViewer.get('canvas');
                    if (this.data.activityId) {
                        this.data.activityId.forEach((activityID) => {
                            canvas.addMarker(activityID, 'highlight');
                        });
                    }
                }
            });
        }
    }

    ngOnDestroy() {
    }

    close() {
        this.onClose.emit();
    }

    save() {
        this.bpmViewer.saveXML({ format: true }, (err, xml) => {
            this.onUpdate.emit(xml);
        });
    }

}
