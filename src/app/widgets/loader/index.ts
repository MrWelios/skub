import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

const INTERVAL_LOADING_STEP = 5;
const LOADING_INTERVAL_SPEED_MS = 300;
const LOADING_COMPLETE_SPEED_MS = 600;

@Component({
    selector: 'vce-loader',
    styleUrls: ['./style.scss'],
    template: `
        <div class="loading-bar" *ngIf="isProgressBarActive">
            <div class="loading-bar__progress" [style.width]="loadingProgressPercent + '%'"></div>
        </div>`,
})

export class LoaderComponent implements OnInit {
    isProgressBarActive = false;
    loadingProgressPercent = 0;
    private intervalCounterId: any = null;

    constructor(private store$: Store<any>) {
    }

    initLoadingBar(): void {
        this.stopLoading();
        this.resetLoading();
        this.isProgressBarActive = true;
        this.intervalCounterId = setInterval(() => {
            if (this.loadingProgressPercent < 75) {
                this.loadingProgressPercent += INTERVAL_LOADING_STEP;
            } else {
                this.loadingProgressPercent += INTERVAL_LOADING_STEP / 10;
            }
        },                                   LOADING_INTERVAL_SPEED_MS);
    }

    completeLoading(): void {
        this.loadingProgressPercent = 100;
        this.stopLoading();
        setTimeout(() => {
            this.loadingProgressPercent = 0;
            this.isProgressBarActive = false;
        },         LOADING_COMPLETE_SPEED_MS);
    }

    stopLoading(): void {
        if (this.intervalCounterId) {
            clearInterval(this.intervalCounterId);
            this.intervalCounterId = null;
        }
    }

    resetLoading(): void {
        this.loadingProgressPercent = 0;
    }

    ngOnInit() {
        this.store$.select(state => state.isLoading).subscribe((isLoading) => {
            if (isLoading) {
                this.initLoadingBar();
            } else {
                this.completeLoading();
            }
        });
    }
}
