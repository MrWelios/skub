import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import cloneDeep from 'lodash/cloneDeep';
import { TranslateService } from '@ngx-translate/core';
import 'prismjs/components/prism-javascript';

@Component({
    selector: 'vce-markdown',
    templateUrl: './template.html',
    styleUrls: [
        './style.scss',
    ],
})

export class MarkdownComponent implements OnInit, OnDestroy {
    @Input() data: string;
    @Input() readonly = false;
    @Output() dataChange = new EventEmitter<string>();
    
    constructor(
        private translate: TranslateService,
    ) { }

    ngOnInit(): void {
        if (!this.data) {
            this.data = '';
        }
    }

    closeDialog() {
    }

    ngOnDestroy(): void {
    }

    onChangeData(event) {
        this.dataChange.emit(event);
    }

    addBold(textAreaWriter) {
        const position = cloneDeep(textAreaWriter.selectionStart);
        this.data = this.data.substring(0, position) + '****' + this.data.substring(position);
        this.onChangeData(this.data);
        textAreaWriter.focus();
        setTimeout(() => textAreaWriter.setSelectionRange(position + 2, position + 2), 0);
    }

    addItalic(textAreaWriter) {
        const position = cloneDeep(textAreaWriter.selectionStart);
        this.data = this.data.substring(0, position) + '**' + this.data.substring(position);
        this.onChangeData(this.data);
        textAreaWriter.focus();
        setTimeout(() => textAreaWriter.setSelectionRange(position + 1, position + 1), 0);
    }

    addQuote(textAreaWriter) {
        const position = cloneDeep(textAreaWriter.selectionStart);
        this.data = this.data.substring(0, position) + '>  ' + this.data.substring(position);
        this.onChangeData(this.data);
        textAreaWriter.focus();
        setTimeout(() => textAreaWriter.setSelectionRange(position + 3, position + 3), 0);
    }

    addCode(textAreaWriter) {
        const position = cloneDeep(textAreaWriter.selectionStart);
        this.data = this.data.substring(0, position) + '``' + this.data.substring(position);
        this.onChangeData(this.data);
        textAreaWriter.focus();
        setTimeout(() => textAreaWriter.setSelectionRange(position + 1, position + 1), 0);
    }

    addBulletList(textAreaWriter) {
        const position = cloneDeep(textAreaWriter.selectionStart);
        this.data = this.data.substring(0, position) + '*  ' + this.data.substring(position);
        this.onChangeData(this.data);
        textAreaWriter.focus();
        setTimeout(() => textAreaWriter.setSelectionRange(position + 3, position + 3), 0);
    }

    addNumberedList(textAreaWriter) {
        const position = cloneDeep(textAreaWriter.selectionStart);
        this.data = this.data.substring(0, position) + '1.  ' + this.data.substring(position);
        this.onChangeData(this.data);
        textAreaWriter.focus();
        setTimeout(() => textAreaWriter.setSelectionRange(position + 4, position + 4), 0);
    }

    addTaskList(textAreaWriter) {
        const position = cloneDeep(textAreaWriter.selectionStart);
        this.data = this.data.substring(0, position) + '* [ ]  ' + this.data.substring(position);
        this.onChangeData(this.data);
        textAreaWriter.focus();
        setTimeout(() => textAreaWriter.setSelectionRange(position + 7, position + 7), 0);
    }

}
