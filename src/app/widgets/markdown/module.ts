import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { 
    MatInputModule,
    MatFormFieldModule,
    MatTabsModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
} from '@angular/material';
import { FormsModule } from '@angular/forms';
import { NgxMdModule } from 'ngx-md';

import { MarkdownComponent } from './component';

@NgModule({
    imports: [
        MatInputModule,
        MatFormFieldModule,
        MatTabsModule,
        MatButtonModule,
        MatIconModule,
        MatTooltipModule,
        NgxMdModule,
        CommonModule,
        FormsModule,
        TranslateModule,
    ],
    declarations: [MarkdownComponent],
    exports: [MarkdownComponent],
})

export class MarkdownModule {}
