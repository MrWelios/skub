import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { NotificationsService, Options, NotificationAnimationType } from 'angular2-notifications';
import { TranslateService } from '@ngx-translate/core';
import { Route, Router } from '@angular/router';
import { ISubscription } from 'rxjs/Subscription';
import { Store } from '@ngrx/store';
import cloneDeep from 'lodash/cloneDeep';

import { AppStateInterface } from 'app/store';
import { NOTIFICATION_TYPE_ERROR, NOTIFICATION_TYPE_SUCCESS, NOTIFICATION_TYPE_INFO } from 'app/constants';

@Component({
    selector: 'vce-notifications',
    styleUrls: ['./style.scss'],
    encapsulation: ViewEncapsulation.None,
    templateUrl: './template.html',
})

export class NotificationsComponent implements OnInit, OnDestroy {
    notiOptions: Options = {
        position: ['top', 'right'],
        timeOut: 3000,
        lastOnBottom: true,
        pauseOnHover: true,
        clickToClose: true,
        maxStack: 3,
        animate: NotificationAnimationType.FromRight,
    };

    notimess: string;

    private notificationsSubscription: ISubscription;

    constructor(
        private store$: Store<AppStateInterface>,
        private notiService: NotificationsService,
        private router: Router,
        private translate: TranslateService,
    ) {
    }

    notiCreated(event) {
        // console.log('==NOTI_CREATED_EVENT', event);
        return false;
    }

    notiDestroyed(event) {
        // console.log('==NOTI_DESTROYED_EVENT', event);
        return false;
    }

    ngOnInit(): void {
        this.notificationsSubscription = this.store$.select(state => state.notifications)
            .subscribe((notification) => {
                if (!notification) {
                    return false;
                }

                this.notimess = cloneDeep(notification.message);

                if (notification.type === NOTIFICATION_TYPE_SUCCESS) {
                    this.notiService.success(notification.message);
                } else if (notification.type === NOTIFICATION_TYPE_ERROR) {
                    this.notiService.error(notification.message);
                } else if (notification.type === NOTIFICATION_TYPE_INFO) {
                    this.notiService.info(notification.message);
                }

            });
    }

    ngOnDestroy(): void {
        this.notificationsSubscription.unsubscribe();
    }

    onclick(event) {
        if (this.notimess === this.translate.instant('NEW_CLAIM')) {
            // this.router.navigate(['/claims/list']);
        }
    }
}
