import { NgModule } from '@angular/core';
import { SimpleNotificationsModule } from 'angular2-notifications';

import { NotificationsComponent } from './component';

@NgModule({
    imports: [
        SimpleNotificationsModule.forRoot(),
    ],
    declarations: [NotificationsComponent],
    exports: [NotificationsComponent],
})
export class NotificationsModule { }
