import { Action } from '@ngrx/store';
import {
    CANGE_MEDIA,
    APP_LOADING_STATE_CHANGED,
    REGISTER_ERRORS,
    NOTIFICATION_TYPE_SUCCESS,
    NOTIFICATION_TYPE_ERROR,
    NOTIFICATION_TYPE_WARN,
    NOTIFICATION_TYPE_INFO,
    APP_LANG_STATE_CHANGED,
    CLOSE_MODAL,
    STATE_SET_USER_ROLE,
    STATE_LOADING_START,
    STATE_LOADING_PROCESS,
    STATE_LOADING_FINISH,
    STATE_LOADING_ERROR,
    STATE_LOADING_CLEAR,
    SET_MODULES_SCHEMA,
    CLEAR_MODULES_SCHEMA,
    APP_LOCALES_STATE_CHANGED,
    SET_SYSTEM_LABEL_LINK,
    SET_PROFILE_DATA,
} from './constants';

export interface ActionWithPayloadInterface extends Action {
    payload?: any;
}

export const isLoading = (state: boolean = false, action: ActionWithPayloadInterface) => {
    if (action.type === APP_LOADING_STATE_CHANGED) {
        return action.payload.isLoading;
    }

    return state;
};

export const userRole = (state: string, action: ActionWithPayloadInterface) => {
    if (action.type === STATE_SET_USER_ROLE) {
        return action.payload;
    }

    return state;
};

export const modulesSchema = (state, action: ActionWithPayloadInterface) => {
    if (action.type === SET_MODULES_SCHEMA) {
        return action.payload.data;
    }
    if (action.type === CLEAR_MODULES_SCHEMA) {
        return {};
    }

    return state;
};

export const profileData = (state, action: ActionWithPayloadInterface) => {
    if (action.type === SET_PROFILE_DATA) {
        return action.payload.data;
    }

    return state;
};

export const loadingFile = (state: any, action: ActionWithPayloadInterface) => {
    switch (action.type) {
        case STATE_LOADING_START: {
            const newState = { ...state };
            newState[action.payload.filename] = {};
            return newState;
        }
        case STATE_LOADING_FINISH: {
            const newState = { ...state };
            newState[action.payload.filename].codes = action.payload.result;
            newState[action.payload.filename].percentage = 101;
            return newState;
        }
        case STATE_LOADING_ERROR: {
            const newState = { ...state };
            newState[action.payload.filename].error = action.payload.error;
            return newState;
        }
        case STATE_LOADING_PROCESS: {
            const newState = { ...state };
            newState[action.payload.filename] = { percentage: action.payload.percentage };
            return newState;
        }
        case STATE_LOADING_CLEAR: {
            return {};
        }

        default:
            return state;
    }
};

export const langState = (state: string, action: ActionWithPayloadInterface) => {
    if (action.type === APP_LANG_STATE_CHANGED) {
        return action.payload;
    }

    return state;
};

export const localesState = (state: string[], action: ActionWithPayloadInterface) => {
    if (action.type === APP_LOCALES_STATE_CHANGED) {
        return action.payload;
    }

    return state;
};

export const notifications = (state, action: ActionWithPayloadInterface) => {
    switch (action.type) {
        case NOTIFICATION_TYPE_SUCCESS:
        case NOTIFICATION_TYPE_ERROR:
        case NOTIFICATION_TYPE_WARN:
        case NOTIFICATION_TYPE_INFO: {
            return { type: action.type, message: action.payload };
        }
    }
};

export const modal = (state, action: ActionWithPayloadInterface) => {
    switch (action.type) {
        case CLOSE_MODAL: {
            return action.payload;
        }
    }
};

export const mediaScreen = (state, action: ActionWithPayloadInterface) => {
    switch (action.type) {
        case CANGE_MEDIA: {
            return action.payload;
        }
    }
    return state;
};

export const systemLabelLink = (state, action: ActionWithPayloadInterface) => {
    if (action.type === SET_SYSTEM_LABEL_LINK) {
        return {
            ...state,
            [action.payload.moduleName]: action.payload.data,
        };
    }
    return state;
};

export const errors = (state: string[] = [], action: ActionWithPayloadInterface) => {
    switch (action.type) {
        case REGISTER_ERRORS:
            return state.some(e => e === action.payload.errors[0])
                ? state
                : [...state, ...action.payload.errors];
    }

    return state;
};
