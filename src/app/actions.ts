import { ActionWithPayloadInterface } from 'app/reducers';
import { 
    ModuleSchemaInterface, 
    APP_LOCALES_STATE_CHANGED, 
    GET_SYSTEM_LABEL_LINK, 
    SET_SYSTEM_LABEL_LINK, 
    GET_PROFILE_DATA, 
    SET_PROFILE_DATA, 
    SAVE_PROFILE_DATA,
} from 'app/constants';
import { 
    APP_LANG_STATE_CHANGED, 
    APP_LOADING_STATE_CHANGED, 
    CANGE_MEDIA, 
    CLOSE_MODAL,
    NotificationType, 
    RESET_APP_STATE, 
    STATE_GET_USER_ROLE, 
    STATE_LOADING_CLEAR, 
    STATE_LOADING_ERROR, 
    STATE_LOADING_FINISH, 
    STATE_LOADING_PROCESS, 
    STATE_LOADING_START, 
    STATE_SET_USER_ROLE, 
    CLEAR_MODULES_SCHEMA,
    GENERATE_CODE,
    GET_MODULES_SCHEMA,
    SET_MODULES_SCHEMA,
    DOWNLOAD_FILE_START,
    DOWNLOAD_FILE_FINISH,
} from './constants';

export function setAppLoadingState(isLoading: boolean = true): ActionWithPayloadInterface {
    return {
        type: APP_LOADING_STATE_CHANGED,
        payload: {
            isLoading,
        },
    };
}

export function generateConfirmCode(type: string): ActionWithPayloadInterface {
    return {
        type: GENERATE_CODE,
        payload: {
            type,
        },
    };
}

export function startFileUpload(filename: string, documents: FormData): ActionWithPayloadInterface {
    return {
        type: STATE_LOADING_START,
        payload: {
            filename,
            documents,
        },
    };
}

export function uploadFileProcess(filename: string, percentage: number): ActionWithPayloadInterface {
    return {
        type: STATE_LOADING_PROCESS,
        payload: {
            filename,
            percentage,
        },
    };
}

export function uploadFileWithSuccess(filename: string, result): ActionWithPayloadInterface {
    return {
        type: STATE_LOADING_FINISH,
        payload: {
            filename,
            result,
        },
    };
}

export function getSystemLabelLink(moduleName: string): ActionWithPayloadInterface {
    return {
        type: GET_SYSTEM_LABEL_LINK,
        payload: {
            moduleName,
        },
    };
}

export function getSystemLabelLinkFinishedWithSuccess(data: any, moduleName): ActionWithPayloadInterface {
    return {
        type: SET_SYSTEM_LABEL_LINK,
        payload: {
            data,
            moduleName,
        },
    };
}

export function uploadFileWithError(filename: string, error: string): ActionWithPayloadInterface {
    return {
        type: STATE_LOADING_ERROR,
        payload: {
            filename,
            error,
        },
    };
}

export function uploadFileClearing(): ActionWithPayloadInterface {
    return {
        type: STATE_LOADING_CLEAR,
    };
}

export function setLang(lang: string): ActionWithPayloadInterface {
    return {
        type: APP_LANG_STATE_CHANGED,
        payload: lang,
    };
}

export function setLocales(locales: string[]): ActionWithPayloadInterface {
    return {
        type: APP_LOCALES_STATE_CHANGED,
        payload: locales,
    };
}

export function resetAppState(): ActionWithPayloadInterface {
    return {
        type: RESET_APP_STATE,
    };
}

export function addNotification(type: NotificationType, notificationMessage: string): ActionWithPayloadInterface {
    return {
        type,
        payload: notificationMessage,
    };
}

export function closeModal(isReloadPage: boolean): ActionWithPayloadInterface {
    return {
        type: CLOSE_MODAL,
        payload: {
            isReloadPage,
        },
    };
}

export function changeMediaScreen(screen: string): ActionWithPayloadInterface {
    return {
        type: CANGE_MEDIA,
        payload: screen,
    };
}

export function getUserRole(): ActionWithPayloadInterface {
    return {
        type: STATE_GET_USER_ROLE,
    };
}

export function setUserRole(role: string): ActionWithPayloadInterface {
    return {
        type: STATE_SET_USER_ROLE,
        payload: role,
    };
}

export function setModulesSchema(data: ModuleSchemaInterface[]): ActionWithPayloadInterface {
    return {
        type: SET_MODULES_SCHEMA,
        payload: {
            data,
        },
    };
}

export function getModulesSchema(): ActionWithPayloadInterface {
    return {
        type: GET_MODULES_SCHEMA,
    };
}

export function clearModulesSchema(): ActionWithPayloadInterface {
    return {
        type: CLEAR_MODULES_SCHEMA,
    };
}

export function getFile(fileName: string): ActionWithPayloadInterface {
    return {
        type: DOWNLOAD_FILE_START,
        payload: {
            fileName,
        },
    };
}

export function getFileFinishedWithSuccess(result): ActionWithPayloadInterface {
    return {
        type: DOWNLOAD_FILE_FINISH,
        payload: {
            result,
        },
    };
}

export function getProfileData(): ActionWithPayloadInterface {
    return {
        type: GET_PROFILE_DATA,
        payload: {
        },
    };
}

export function setProfileData(data: any): ActionWithPayloadInterface {
    return {
        type: SET_PROFILE_DATA,
        payload: {
            data,
        },
    };
}

export function updateProfileData(data: any): ActionWithPayloadInterface {
    return {
        type: SAVE_PROFILE_DATA,
        payload: {
            data,
        },
    };
}
