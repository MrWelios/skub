import { Injectable } from '@angular/core';
import { Subject, Observable, Subscription } from 'rxjs/Rx';
import { WebSocketSubject } from 'rxjs/observable/dom/WebSocketSubject';

@Injectable()
export class WebSocketService {
    public message: Observable<Object>;
    public opened: Observable<boolean>;
    public isOpened: boolean;
    
    public identity = {};
    
    private messageSubject: Subject<Object> = new Subject();
    private openedSubject:  Subject<boolean> = new Subject();
    private ws: WebSocketSubject<Object>;
    private socket: Subscription;
    private url: string;

    constructor() {
        this.opened = this.openedSubject.asObservable();
        this.message = this.messageSubject.asObservable();
        this.openedSubject.subscribe((value) => {
            this.isOpened = value;
        });

    }

    close(): void {
        this.socket.unsubscribe();
        this.ws.complete();
    }

    sendMessage(message: any): void {
        this.ws.next(message);
    }

    start(url: string): void {
        const self = this;

        this.url = url;

        this.ws = Observable.webSocket(this.url);

        this.socket = this.ws.subscribe({

            next: (data: MessageEvent) => {
                if (data['type'] === 'pong') {
                    self.openedSubject.next(true);
                }
                this.messageSubject.next(data);
            },
            error: () => {

                self.openedSubject.next(false);
                this.messageSubject.next({ type: 'closed' });

                self.socket.unsubscribe();

                setTimeout(() => {
                    self.start(self.url);
                },         1000);

            },
            complete: () => {
                this.messageSubject.next({ type: 'closed' });
            },
        });
    }
}
