export interface BodyGetChange {
    page: number;
    itemsPerPage: number;
}
