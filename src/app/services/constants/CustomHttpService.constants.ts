export const LS_SESSIONID = 'JWT';
export const LS_EXPIRES = 'EXPIRES';
export const SS_RETURN_URL = 'returnUrl';
