import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { RestService } from './RestService';

@Injectable()
export class CustomViewService extends RestService {
    public moduleName: string;

    getFilters(): Observable<any> {
        return this.get(`/ichain/ichain_custom_view/${this.moduleName}`);
    }

    updateData(data): Observable<any> {
        if (data.id) {
            return this.put(`/ichain/ichain_custom_view/${this.moduleName}/${data.id}`, data);
        }
        return this.post(`/ichain/ichain_custom_view/${this.moduleName}/`, data);
    }

    deleteFilter(id) {
        return this.delete(`/ichain/ichain_custom_view/${id}`);
    }
    
    setFilter(filter) {
        return this.post(`/ichain/ichain_custom_view/setFilter`, {
            id: filter.id,
            schemaName: this.moduleName,
        });
    }

    getFields(): Observable<any> {
        return this.get(`/ichain/ichain_custom_view/${this.moduleName}/fieldsForFilter`);
    }
}
