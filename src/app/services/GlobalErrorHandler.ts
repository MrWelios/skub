import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import * as StackTrace from 'stacktrace-js';
import { environment } from '../../environments/environment';

const MAX_STACK_TRACE_LINE_DEPTH = 20;

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
    constructor(private injector: Injector) { }

    handleError(error) {
        const location = this.injector.get(LocationStrategy);
        const message = error.message ? error.message : error.toString();
        const url = location instanceof PathLocationStrategy ? location.path() : '';
        StackTrace.fromError(error)
            .then((stackFrames) => {
                const errorObj = {
                    url,
                    msg: message,
                    trace: stackFrames.splice(0, MAX_STACK_TRACE_LINE_DEPTH).map(sf => sf.toString()),
                };

                console.error(errorObj);
                if (environment.production) {
                    // log error to server
                }
            });
    }
}
