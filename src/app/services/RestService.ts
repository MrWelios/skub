import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import get from 'lodash/get';

import { environment } from 'environments/environment';

@Injectable()
export class RestService {

    constructor(
        private httpClient: HttpClient,
    ) { }

    protected get(url, options?): Observable<any> {
        return this.httpClient.get(this.getUrl(url), options);
    }

    protected post(url, data?, options?): Observable<any> {
        return this.httpClient.post(this.getUrl(url), data, options);
    }

    protected patch(url, data?, options?): Observable<any> {
        return this.httpClient.patch(this.getUrl(url), data, options);
    }

    protected put(url, data?, options?): Observable<any> {
        return this.httpClient.put(this.getUrl(url), data, options);
    }

    protected delete(url, options?): Observable<any> {
        return this.httpClient.delete(this.getUrl(url), options);
    }

    private getUrl(url: string): string {
        let newUrl = url;
        if (newUrl.indexOf('http') === -1) {
            newUrl = get(environment, 'api.url', '') + url;
        }
        return newUrl;
    }

}
