import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { switchMap, catchError } from 'rxjs/operators';

import { AppStateInterface } from 'app/store';
import { setLang, setUserRole, setLocales } from 'app/actions';
import { LS_SESSIONID } from './constants/CustomHttpService.constants';
import { ModuleSchemaInterface } from 'app/constants';
import { RestService } from './RestService';
import { Storages } from 'app/utilites/Storages';

@Injectable()
export class UIService extends RestService {
    
    private methodOpenSettings = new Subject<any>();
    private methodOpenSidebar = new Subject<any>();
    
    // tslint:disable-next-line:member-ordering
    methodOpenSettingsObservable = this.methodOpenSettings.asObservable();
    // tslint:disable-next-line:member-ordering
    methodOpenSidebarObservable = this.methodOpenSidebar.asObservable();

    private langState = 'en';
    private localesState = ['en'];

    constructor(
        private store$: Store<AppStateInterface>,
        httpClient: HttpClient,
        /*private translate: TranslateService,*/
    ) {
        super(httpClient);
    }

    updateSettingsVisible(newState: boolean) {
        this.methodOpenSettings.next(newState);
    }

    updateSidebarVisible(newState: boolean) {
        this.methodOpenSidebar.next(newState);
    }

    getLangState(): Observable<string> {
        return this.store$.select((state) => {
            return state.langState;
        });
    }

    getLocalesState(): Observable<string[]> {
        return this.store$.select((state) => {
            return state.localesState;
        });
    }

    processLangState(lang: string): Observable<any> {
        let langSchema = Storages.getLangSchema(lang);
        if (!langSchema) {
            return this.getLangSchema(lang).pipe(
                switchMap((response: any) => {
                    langSchema = response.localizationData;
                    if (response.currentLocalization !== lang) {
                        this.store$.dispatch(setLang(response.currentLocalization));
                    }
                    const currentLocales = Storages.getLocales();
                    if (!currentLocales) {
                        Storages.setLocales(response.availableLocalizations);
                        this.store$.dispatch(setLocales(response.availableLocalizations));
                    }
                    Storages.setLangSchema(response.currentLocalization, langSchema);
                    return Observable.of(langSchema);
                }),
                catchError((error) => {
                    /*this.translate.get(get(error, 'error.error', 'ERROR')).subscribe((value) => {
                        this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                    });*/
                    return Observable.of({});
                }),
            );
        } 
        return Observable.of(langSchema);
    }

    setDateFormat(date: Date): string {
        const dd = date.getDate();
        const mm = date.getMonth() + 1;
        const yyyy = date.getFullYear();

        let displayDD = dd.toString();
        let displayMM = mm.toString();
        if (dd < 10) {
            displayDD = `0${dd}`;
        } 
        
        if (mm < 10) {
            displayMM = `0${mm}`;
        } 
        return `${yyyy}-${displayMM}-${displayDD}`;
    }

    setDateTimeFormat(date: Date): string {

        const dateCalendar = this.setDateFormat(date);

        let minute = date.getMinutes().toString();

        if (date.getMinutes() < 10) {
            minute = `0${minute}`;
        }

        let second = date.getSeconds().toString();

        if (date.getSeconds() < 10) {
            second = `0${second}`;
        }
        
        return `${dateCalendar} ${date.getHours()}:${minute}:${second}`;
    }

    getIsLogginedState(): Observable<boolean> {
        return this.store$.select((state) => {
            return !!(sessionStorage.getItem(LS_SESSIONID) || document.cookie.match(LS_SESSIONID));
        });
    }

    getStoreModulesSchema(): Observable<ModuleSchemaInterface[]> {
        return this.store$.select(state => state.modulesSchema);
    }

    getStoreUserRole(): Observable<string> {
        return this.store$.select(state => state.userRole);
    }

    setStoreUserRole(role: string) {
        this.store$.dispatch(setUserRole(role));
    }

    getMenuListRequest(): Observable<any> {
        return this.get(`/v1/crm/menu`);
    }

    getUserRole(): Observable<any> {
        return this.get('/auth/roles/name').map(response => response);
    }

    getSystemLabelLink(moduleName: string): Observable<any> {
        return this.post(`/ichain/${moduleName}/namesList`, {
            nameToFind: '',
            maxResultLimit: 10000,
        }).map(response => response);
    }

    loginKibana() {
        return this.post('/reports/auth', {}, { headers: { 'kbn-version': '6.2.4' } });
    }

    getModules(): Observable<any> {
        return this.get('/getModules/');
    }

    getLangSchema(lang: string): Observable<any> {
        return this.get(`/getLocalization/${lang}`);
    }

    isEmpty(obj) {

        if (obj == null) {
            return true;
        }
    
        if (obj.length > 0) {
            return false;
        }
        if (obj.length === 0) {
            return true;
        }
    
        if (typeof obj !== 'object') {
            return true;
        }
    
        for (const key in obj) {
            if (key) {
                return false;
            }
        }
    
        return true;
    }
}
