import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { RestService } from '../RestService';

@Injectable()
export class SettingsService extends RestService {

    getAllowedSetting(data): Observable<any> {
        return this.post('/modules/setting/list', data);
    }

    getDelayData(): Observable<any> {
        return this.get('/modules/settings/current');
    }

    updateDelayData(data): Observable<any> {
        return this.patch(`/modules/settings/${data.id}`, data);
    }

    updateData(id, data): Observable<any> {
        return this.patch(`/modules/setting/${id}`, { data });
    }

    generateCode(type): Observable<any> {
        return this.get(`/modules/tools/generateCode?type=${type}`);
    }

    generateBurnCode(): Observable<any> {
        return this.get('/modules/tools/generateCode?type=burn');
    }

    burnContract(code): Observable<any> {
        return this.post('/modules/contract/burn/', { confirmCode: code });
    }
}
