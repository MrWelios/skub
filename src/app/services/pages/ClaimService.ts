import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { forkJoin } from 'rxjs/observable/forkJoin';

import { RestService } from '../RestService';

@Injectable()
export class ClaimService extends RestService {

    getList(payload): Observable<any> {
        return this.post('/modules/claims/list', payload).map(response => response);
    }
    
    getData(id): Observable<any> {
        return this.get('/modules/claims/' + id).map(response => response);
    }
    
    updateData(data): Observable<any> {
        return this.patch('/modules/claims/' + data.id, data).map(response => response);
    }

    verifyPolicy(data): Observable<any> {
        return this.post('/modules/policies/verify', data).map(response => response);
    }

    updateClaimForm(data): Observable<any> {
        return this.post('/modules/claims/update', data).map(response => response);
    }

    approveClaim(data): Observable<any> {
        return this.post('/process/instance/continue', data).map(response => response);
    }
}
