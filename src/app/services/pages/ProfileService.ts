import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { forkJoin } from 'rxjs/observable/forkJoin';

import { RestService } from '../RestService';

@Injectable()
export class ProfileService extends RestService {

    getData(): Observable<any> {
        return this.get('/user/profile').map(response => response);
    }
    
    updateData(data): Observable<any> {
        return this.patch('/user/profile/', data).map(response => response);
    }

}
