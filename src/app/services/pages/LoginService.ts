import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { UIService } from 'app/services/UIService';
import { LS_SESSIONID } from '../constants/CustomHttpService.constants';
import { RestService } from '../RestService';

@Injectable()
export class LoginService extends RestService {

    constructor(
        private router: Router,
        private uiService: UIService,
        httpClient: HttpClient,
    ) {
        super(httpClient);
    }

    obtainAccessToken(loginData: any): Observable<any> {
        return this.post('/auth/login', {
            username: loginData.email,
            password: loginData.password,
        });
    }

    continueAuth(loginData: any): any {
        const body = {
            email: loginData.email,
            password: loginData.password,
            confirmCode: loginData.confirmCode,
        };
        return this.post('/two-factor/continue', body)
            .map(response => response);
    }

    saveToken(token: any, rememberme: boolean, returnLocation) {
        sessionStorage.setItem(LS_SESSIONID, token.auth);
        window.location.href = 'modules';
    }
}
