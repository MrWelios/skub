import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { isPrimitive, isArray } from 'util';
import cloneDeep from 'lodash/cloneDeep';
import get from 'lodash/get';

import { RestService } from './RestService';
import { Storages } from 'app/utilites/Storages';
import { LS_SESSIONID } from './constants/CustomHttpService.constants';
import { UiUtilites } from 'app/utilites/UiUtilites';
import { BodyGetChange } from 'app/services/constants/EntityService.constants';

@Injectable()
export class EntityService extends RestService {
    public moduleName: string;

    getList(payload): Observable<any> {
        return this.post(`/modules/${this.moduleName}/list`, payload);
    }

    getLinkList(payload, linkModule): Observable<any> {
        return this.post(`/modules/${linkModule}/list`, payload);
    }

    getTasksList(id, module): Observable<any> {
        return this.post(`/modules/${module}/${id}/taskList`);
    }

    createTask(data): Observable<any> {
        return this.post(`/modules/task`, { data });
    }

    getData(id): Observable<any> {
        return this.get(`/modules/${this.moduleName}/${id}`);
    }

    searchIchane(payload): Observable<any> {
        return this.post(`/ichain/${this.moduleName}/namesList`, payload);
    }

    search(payload): Observable<any> {
        return this.post(`/modules/${this.moduleName}/namesList`, payload);
    }

    loadPriceItems(payload): Observable<any> {
        return this.post(`/modules/${this.moduleName}/namesWithPricesList`, payload);
    }

    getChangelog(id: number, body: BodyGetChange): Observable<any> {
        return this.post(`/modules/${this.moduleName}/${id}/changelog`, body);
    }

    updateData(data): Observable<any> {
        if (data.id) {
            return this.patch(`/modules/${this.moduleName}/${data.id}`, data);
        }
        return this.post(`/modules/${this.moduleName}/`, data);
    }

    updateForm(data): Observable<any> {
        return this.post(`/modules/${this.moduleName}/update`, data);
    }

    getComments(data): Observable<any> {
        return this.post(`/modules/${this.moduleName}/${data.entityId}/comments`, data);
    }

    getChildComments(commentId): Observable<any> {
        return this.get(`/comments/${commentId}/childsHierarchy`);
    }

    processEntitySections(dataSchema, data): any {
        const fieldsData = {};
        Object.keys(dataSchema.data).forEach((key) => {
            const sectionSchema = dataSchema.data[key];
            fieldsData[key] = { 
                ...fieldsData[key], 
                ...this.processSectionData(sectionSchema, data.data[key], dataSchema.edit),
            };
            sectionSchema.sortOrder = UiUtilites.sortObject(sectionSchema.fields, 'order');
        });
        Object.keys(fieldsData).forEach((key) => {
            Object.keys(fieldsData[key]).forEach((field) => {
                if (get(fieldsData, [key, field, 'display_type']) === 'section') {
                    Object.keys(fieldsData[key][field].fields).forEach((subField) => {
                        this.processDependentField(fieldsData, fieldsData[key][field].fields[subField]);
                    });
                } else if (get(fieldsData, [key, field, 'display_type']) === 'field_map') {
                    if (fieldsData[key][field].value) {
                        Object.keys(fieldsData[key][field].value.field_info).forEach((subField) => {
                            this.processDependentField(fieldsData, fieldsData[key][field].value.field_info[subField]);
                        });
                    }
                } else {
                    this.processDependentField(fieldsData, fieldsData[key][field]);
                }
            });
        });
        return fieldsData;
    }

    generateSaveObject(fieldsData: any) {
        const data: any = {};
        Object.keys(fieldsData).forEach((section) => {
            data[section] = { fields: {} };
            const fieldsObj = get(fieldsData, [section, 'fields'], fieldsData[section]);
            Object.keys(fieldsObj).forEach((field) => {
                if (fieldsObj[field].display_type === 'section') {
                    data[section].fields[field] = {};
                    Object.keys(fieldsObj[field].fields).forEach((subField) => {
                        data[section].fields[field][subField] = this.processSaveValue(fieldsObj[field].fields[subField]);
                    });
                } else {
                    data[section].fields[field] = this.processSaveValue(fieldsObj[field]);
                }
            });
        });
        return data;
    }

    requestButton(data): Observable<any> {
        switch (data.methodType) {
            case 'DELETE': {
                return this.delete(data.url);
            }
            case 'POST': {
                return this.post(data.url, data.data);
            }
            case 'PATCH': {
                return this.patch(data.url, data.data);
            }
            case 'GET': {
                return this.get(`${data.url}${data.data ? `?${this.generateUrlParams(data.data)}` : ``}`);
            }
            default: {
                return Observable.of({});
            }
        }
    }

    downloadButton(data: any): Observable<any> {
        const storageId = Storages.getSessionItem(LS_SESSIONID);
        const options: any = {
            responseType: 'blob',
            observe: 'response',
            headers: {
                Authorization: `Bearer ${storageId}`,
            },
        };
        return this.get(data.url, options);
    }

    getFilename(disposition) {
        if (disposition && disposition.indexOf('attachment') !== -1) {
            const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
            const matches = filenameRegex.exec(disposition);
            if (matches != null && matches[1]) {
                return matches[1].replace(/['"]/g, '');
            }
        }
        return '';
    }

    private processSaveValue(field: any) {
        if (field.display_type === 'field_map' && get(field, ['value', 'field_info'], false)) {
            const mapData = {};
            Object.keys(field.value.field_info).forEach((mapField) => { 
                mapData[mapField] = get(
                    field,
                    ['value', 'field_info', mapField, 'value', 'value'],
                    get(field, ['value', 'field_info', mapField, 'value']),
                );
            });
            return mapData;
        }
        if (isArray(field.value)) {
            return field.value.map(row => get(row, 'value', row));
        }
        return get(field, ['value', 'value'], get(field, 'value'));
    }

    private processSectionData(section: any, sectionData: any, isEdit: boolean): any {
        const data = cloneDeep(section.fields);
        Object.keys(data).forEach((field) => {
            if (data[field].display_type === 'section') {
                data[field].sortOrder = UiUtilites.sortObject(data[field].fields, 'order');
                Object.keys(data[field].fields).forEach((subField) => {
                    this.processFieldValue(data[field].fields[subField], get(sectionData.fields[field], subField), isEdit);
                });
            } else {  
                this.processFieldValue(data[field], get(sectionData.fields, field), isEdit);
            }
        });
        return data;
    }

    private processFieldValue(field: any, value: any, isEdit: boolean) {
        let targetValue = cloneDeep(value);
        if (field.display_type === 'field_map') {
            if (targetValue && targetValue.field_info) {
                field.sortOrder = UiUtilites.sortObject(targetValue.field_info, 'order');
                Object.keys(targetValue.field_info).forEach((mapField) => {
                    const displayType = targetValue.field_info[mapField].display_type;
                    if (targetValue.field_map[mapField]) {
                        let val = targetValue.field_map[mapField];
                        if ((displayType === 'label_link' || displayType === 'external_link') && isEdit) {
                            val = get(val, 'value', null);
                        }
                        targetValue.field_info[mapField].value = val;
                    } else {
                        targetValue.field_info[mapField].value = null;
                    }
                });
            }
        } else {
            const displayType = field.display_type;
            if ((displayType === 'label_link' || displayType === 'external_link') && targetValue && isEdit) {
                targetValue = get(targetValue, 'value', null);
            }
        }
        field.value = targetValue ? targetValue : null;
    }

    private processDependentField(fieldsData: any, field: any) {
        if (field.display_type === 'dependent_label_link') {
            const parsedModule = field.module.split('.');
            parsedModule.splice(1, 1);
            const dependsOn = get(fieldsData, parsedModule);
            dependsOn.dependentField = field;
            field.module = dependsOn.value;
        }
    }

    private generateUrlParams(data) {
        const params = new URLSearchParams();
        Object.keys(data).forEach((key) => {
            params.set(key, isPrimitive(data[key]) ? data[key] : JSON.stringify(data[key]));
        });

        return params.toString();
    }
}
