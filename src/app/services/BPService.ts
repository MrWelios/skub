import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { forkJoin } from 'rxjs/observable/forkJoin';

import { RestService } from './RestService';

@Injectable()
export class BPService extends RestService {
    
    getViewBP(defenition, currentActivity, isEdit): Observable<any> {
        const requests = [];
        requests.push(this.getProcessDefinition(defenition));
        if (!isEdit) {
            requests.push(this.currentActivity(currentActivity));
        }
        return forkJoin(requests).pipe(
            map(response => response),
        );
    }

    updateBP(data, id): Observable<any> {
        return this.put(`/modules/bpm_definition/${id}/update`, data);
    }

    getProcessDefinition(data): Observable<any> {
        return this.get(data.url);
    }
    
    currentActivity(data): Observable<any> {
        return this.get(data.url);
    }
}
