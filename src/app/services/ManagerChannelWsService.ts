import { Injectable } from '@angular/core';

import { ChannelWsService } from './ChannelWsService';
import { WebSocketService } from './WebSocketService';
import { Storages } from '../utilites/Storages';
import { LS_SESSIONID } from 'app/services/constants/CustomHttpService.constants';

@Injectable()
export class ManagerChannelWsService extends ChannelWsService {
    channel = 'manager';
    identityId = Storages.getSessionItem(LS_SESSIONID);

    constructor(websocketService: WebSocketService) {
        super(websocketService);
    }

    protected getSubscribeString(): any {
        return {
            command: 'subscribe',
            channel: this.channel,
            JWT: this.identityId,
        };
    }
}
