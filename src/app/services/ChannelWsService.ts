import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { WebSocketService } from './WebSocketService';
import cloneDeep from 'lodash/cloneDeep';

@Injectable()
export class ChannelWsService {

    public observableData: Subject<Object> = new Subject();
    public channel: string;
    public identifierStr: string;
    public subscribed: Subject<boolean> = new Subject();
    private isSubscribed: boolean;

    private socketStarted: boolean;

    constructor(private websocketService: WebSocketService) {
        this.observeOpened();
        this.observeMessage();
    }

    send(data: Object) {
        this.websocketService.sendMessage({
            command: 'message',
            channel: this.channel,
            data: JSON.stringify(data),
        });
    }

    subscribe() {
        if (this.socketStarted && !this.isSubscribed) {
            this.websocketService.sendMessage(this.getSubscribeString());
        }
    }

    unsubscribe() {
        this.websocketService.sendMessage({
            command: 'unsubscribe',
            channel: this.channel,
        });
    }

    protected getSubscribeString(): any {
        return {
            command: 'subscribe',
            channel: this.channel,
        };
    }

    protected observeOpened() {
        this.socketStarted = cloneDeep(this.websocketService.isOpened);
        this.websocketService.opened.subscribe((data: boolean) => {
            if (!data) {
                this.isSubscribed = false;
            }
            this.socketStarted = data;
            this.subscribe();
        });
    }

    protected observeMessage() {
        const self = this;
        this.websocketService.message.subscribe((data: Object) => {
            if (self.isThisChannel(data)) {
                if (data['type']) {
                    switch (data['type']) {
                        case 'subscriptionSuccess': {
                            this.subscribed.next(true);
                            this.isSubscribed = true;
                            break;
                        }
                        case 'subscriptionFail': {
                            // setTimeout(() => this.subscribe(), 1000);
                            break;
                        }
                        case 'unsubscriptionSuccess': {
                            this.subscribed.next(false);
                            this.isSubscribed = false;
                            break;
                        }
                        case 'unsubscriptionFail': {
                            // setTimeout(() => this.unsubscribe(), 1000);
                            break;
                        }
                    }
                } else if (data['message']) {
                    this.observableData.next(data['message']);
                }
            }
        });
    }

    private encodeIdentifier(identifier: string): Object {
        return JSON.parse(identifier);
    }

    private isThisChannel(data: Object): boolean {
        if (data['channel']) {
            if (this.channel === data['channel']) {
                return true;
            }
        }
        return false;
    }

}
