import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import get from 'lodash/get';

import { Storages } from '../utilites/Storages';
import { LS_SESSIONID } from './constants/CustomHttpService.constants';
import { environment } from 'environments/environment';
import { RestService } from './RestService';

@Injectable()
export class FileService extends RestService {

    uploadFile(documents: FormData): Observable<any> {
        return this.post('/documents', documents, { 
            observe: 'events',
            reportProgress: true,
        });
    }

    uploadExcel(fieldName: string, documents: FormData, format: string): Observable<any> {
        const storageId = Storages.getSessionItem(LS_SESSIONID);
        const options: any = { 
            observe: 'events',
            reportProgress: true,
            headers: {
                Authorization: 'Bearer ' + storageId,
            },
        };
        
        return this.post(`/actuary/ratio_table/${fieldName}/import/${format}`, documents, options);
    }

    getFile(fileName: string) {
        const storageId = Storages.getSessionItem(LS_SESSIONID);
        const options: any = { 
            responseType: 'blob',
            headers: {
                Authorization: 'Bearer ' + storageId,
            },
        };
        return this.get(`/documents/${fileName}`, options);
    }

}
