import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { ErrorPageComponent } from 'app/pages/components/error-page/index';
import { LoggedGuard } from 'app/guards/LoggedGuards';
import { RoleGuard } from './guards/RoleGuard';
import { LoadGuard } from './guards/LoadGuard';
import { AuthGuard } from './guards/AuthGuard';

const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'login' },
    { path: 'login', loadChildren: 'app/pages/guest/login/login.module#LoginModule', canActivate: [LoggedGuard] },
    { path: 'profile', loadChildren: 'app/pages/protected/profile/profile.module#ProfileModule', canActivate: [AuthGuard] },
    { path: 'modules', loadChildren: 'app/pages/protected/entity/entity.module#EntityModule', canActivate: [RoleGuard] },
    { path: 'modules/:entity', 
        loadChildren: 'app/pages/protected/entity/entity.module#EntityModule',
        data: { breadcrumb: 'entity', module: 'claims' }, 
        canActivate: [RoleGuard],
        canLoad: [LoadGuard],
    },
    { path: '**', component: ErrorPageComponent, data: { breadcrumb: 'ERROR' } },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})

export class AppRoutingModule { }
