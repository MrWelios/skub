import { routerReducer, RouterReducerState } from '@ngrx/router-store';

import { 
    isLoading, 
    notifications, 
    modal, 
    langState,
    localesState,
    mediaScreen, 
    errors, 
    userRole, 
    loadingFile,
    modulesSchema,
    systemLabelLink,
    profileData,
} from './reducers';
import { ModuleSchemaInterface } from './constants';

export interface AppStateInterface {
    isLoading: boolean;
    router: RouterReducerState;
    notifications: any;
    langState: string;
    localesState: string[];
    mediaScreen: string;
    errors: string[];
    userRole: string;
    loadingFile: any;
    systemLabelLink: any;
    modulesSchema: ModuleSchemaInterface[];
    profileData: any;
}

export const commonReducers = {
    isLoading,
    notifications,
    modal,
    langState,
    localesState,
    mediaScreen,
    errors,
    userRole,
    loadingFile,
    modulesSchema,
    systemLabelLink,
    profileData,
    router: routerReducer,
};
