import { HttpEvent, HttpEventType } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { catchError, debounceTime, map, switchMap, tap, concatMap } from 'rxjs/operators';
import 'rxjs/add/observable/of';
import get from 'lodash/get';
import { saveAs } from 'file-saver';

import { 
    addNotification, 
    uploadFileProcess, 
    uploadFileWithError,
    uploadFileWithSuccess, 
    setAppLoadingState, 
    setUserRole,
    setModulesSchema,
    getSystemLabelLinkFinishedWithSuccess,
    setProfileData,
    getProfileData,
} from 'app/actions';
import { 
    NOTIFICATION_TYPE_ERROR, 
    STATE_LOADING_START, 
    STATE_GET_USER_ROLE,
    GET_MODULES_SCHEMA,
    GENERATE_CODE,
    DOWNLOAD_FILE_START,
    GET_SYSTEM_LABEL_LINK,
    GET_PROFILE_DATA,
    SAVE_PROFILE_DATA,
} from 'app/constants';
import { FileService } from 'app/services/FileService';
import { AppStateInterface } from 'app/store';
import { UIService } from './services/UIService';
import { SettingsService } from './services/pages/SettingsService';
import { Storages } from './utilites/Storages';
import { ActionWithPayloadInterface } from './reducers';
import { ProfileService } from './services/pages/ProfileService';

const LOADING_DEBOUNCE = 500;

@Injectable()
export class GlobalEffects {
    loadingDebounce = LOADING_DEBOUNCE;

    @Effect() requestUploadFile = this.actions$.pipe(
        ofType(STATE_LOADING_START),
        debounceTime(this.loadingDebounce),
    ).distinctUntilChanged().pipe(
        switchMap((action: ActionWithPayloadInterface) => {
            const { filename, documents } = action.payload;
            return this.fileService.uploadFile(documents).pipe(
                map((event: HttpEvent<Object>) => {
                    if (event.type === HttpEventType.UploadProgress) {
                        this.store$.dispatch(uploadFileProcess(filename, Math.round(100 * event.loaded / event.total)));
                    } else if (event.type === HttpEventType.Response) {
                        this.store$.dispatch(uploadFileWithSuccess(filename, event.body));
                    }
                    return  Observable.of({});
                }),
                catchError((error) => {
                    this.translate.get(get(error, 'error.message', 'ERROR')).subscribe((value) => {
                        this.store$.dispatch(uploadFileWithError(filename, value));
                    });
                    this.translate.get(get(error, 'error.error', 'ERROR')).subscribe((value) => {
                        this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                    });
                    return Observable.of({});
                }),
            );
        }),
    );
    
    @Effect() getUserRole = this.actions$.pipe(
        ofType(STATE_GET_USER_ROLE),
        debounceTime(this.loadingDebounce),
        tap(() => this.store$.dispatch(setAppLoadingState(true))),
    ).distinctUntilChanged().pipe(
        switchMap((action: ActionWithPayloadInterface) => {
            return this.uiService.getUserRole().pipe(
                switchMap((data: any) => {
                    this.store$.dispatch(setUserRole(data.role));
                    return  Observable.of({});
                }),
                catchError((error) => {
                    this.translate.get(get(error, 'error.error', 'ERROR')).subscribe((value) => {
                        this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                    });
                    return Observable.of({});
                }),
                tap(() => this.store$.dispatch(setAppLoadingState(false))),
            );
        }),
    );

    @Effect({ dispatch: false }) requestGetLinkedList = this.actions$.pipe(
        ofType(GET_SYSTEM_LABEL_LINK),
        tap(() => this.store$.dispatch(setAppLoadingState(true))),
    ).distinctUntilChanged().pipe(
        concatMap((action: ActionWithPayloadInterface) => {
            const { moduleName } = action.payload;
            return this.uiService.getSystemLabelLink(moduleName).pipe(
                concatMap((response: any) => {
                    this.store$.dispatch(getSystemLabelLinkFinishedWithSuccess(response, moduleName));
                    return  Observable.of({});
                }),
                catchError((error) => {
                    this.translate.get(get(error, 'error.error', 'ERROR')).subscribe((value) => {
                        this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                    });
                    return Observable.of({});
                }),
                tap(() => this.store$.dispatch(setAppLoadingState(false))),
            );
        }),
    );

    @Effect() requestGetModulesSchema = this.actions$.pipe(
        ofType(GET_MODULES_SCHEMA),
        debounceTime(this.loadingDebounce),
        tap(() => this.store$.dispatch(setAppLoadingState(true))),
    ).distinctUntilChanged().pipe(
        switchMap((action: ActionWithPayloadInterface) => {
            return this.uiService.getModules().pipe(
                switchMap((response: { modules: any }) => {
                    if (response.modules) {
                        Storages.setModulesSchema(response.modules);
                        this.store$.dispatch(setModulesSchema(response.modules));
                    }
                    return  Observable.of({});
                }),
                catchError((error) => {
                    this.translate.get(get(error, 'error.error', 'ERROR')).subscribe((value) => {
                        this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                    });
                    return Observable.of({});
                }),
                tap(() => this.store$.dispatch(setAppLoadingState(false))),
            );
        }),
    );

    @Effect() requestGenerateCode = this.actions$.pipe(
        ofType(GENERATE_CODE),
        debounceTime(this.loadingDebounce),
        tap(() => this.store$.dispatch(setAppLoadingState(true))),
    ).distinctUntilChanged().pipe(
        switchMap((action: ActionWithPayloadInterface) => {
            const { type } = action.payload;
            return this.settingsService.generateCode(type).pipe(
                switchMap((response: any) => {
                    // this.store$.dispatch(addNotification(NOTIFICATION_TYPE_SUCCESS, 'Success'));
                    return  Observable.of({});
                }),
                catchError((error) => {
                    this.translate.get(get(error, 'error.error', 'ERROR')).subscribe((value) => {
                        this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                    });
                    return Observable.of({});
                }),
                tap(() => this.store$.dispatch(setAppLoadingState(false))),
            );
        }),
    );

    @Effect() requestGetFile = this.actions$.pipe(
        ofType(DOWNLOAD_FILE_START),
        debounceTime(this.loadingDebounce),
    ).distinctUntilChanged().pipe(
        switchMap((action: ActionWithPayloadInterface) => {
            const { fileName } = action.payload;
            return this.fileService.getFile(fileName).pipe(
                switchMap((data: any) => {
                    const blob = new Blob([data], { type: '' });
                    if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window['MSStream']) {
                        const reader = new FileReader();
                        reader.onload = () => {
                            window.location.href = (reader.result as string);
                        };
                        reader.readAsDataURL(blob);
                    } else {
                        saveAs(blob, fileName, true);
                    }

                    return Observable.of({});
                }),
                catchError((error) => {
                    const blb    = new Blob([error.error], { type: error.error.type });
                    const reader = new FileReader();

                    reader.addEventListener('loadend', (e) => {
                        const text = get(e, 'srcElement.result', '');
                        this.translate.get(get(JSON.parse(text), 'error', 'ERROR')).subscribe((value) => {
                            this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                        });
                    });

                    reader.readAsText(blb);
                    return Observable.of({});
                }),
            );
        }),
    );

    @Effect() requestGetProfileData = this.actions$.pipe(
        ofType(GET_PROFILE_DATA),
        debounceTime(this.loadingDebounce),
        tap(() => this.store$.dispatch(setAppLoadingState(true))),
    ).distinctUntilChanged().pipe(
        switchMap((action: ActionWithPayloadInterface) => {
            const { } = action.payload;
            return this.profileService.getData().pipe(
                switchMap((response: any) => {
                    this.store$.dispatch(setProfileData(response));
                    return  Observable.of({});
                }),
                catchError((error) => {
                    this.translate.get(get(error, 'error.error', 'ERROR')).subscribe((value) => {
                        this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                    });
                    return Observable.of({});
                }),
                tap(() => this.store$.dispatch(setAppLoadingState(false))),
            );
        }),
    );

    @Effect() requestSaveProfileData = this.actions$.pipe(
        ofType(SAVE_PROFILE_DATA),
        debounceTime(this.loadingDebounce),
        tap(() => this.store$.dispatch(setAppLoadingState(true))),
    ).distinctUntilChanged().pipe(
        switchMap((action: ActionWithPayloadInterface) => {
            const { data } = action.payload;
            return this.profileService.updateData(data).pipe(
                switchMap((response: any) => {
                    this.store$.dispatch(getProfileData());
                    return  Observable.of({});
                }),
                catchError((error) => {
                    this.translate.get(get(error, 'error.error', 'ERROR')).subscribe((value) => {
                        this.store$.dispatch(addNotification(NOTIFICATION_TYPE_ERROR, value));
                    });
                    return Observable.of({});
                }),
                tap(() => this.store$.dispatch(setAppLoadingState(false))),
            );
        }),
    );

    constructor(
        private store$: Store<AppStateInterface>,
        private actions$: Actions,
        private fileService: FileService,
        private settingsService: SettingsService,
        private translate: TranslateService,
        private uiService: UIService,
        private profileService: ProfileService,
    ) {}
}
