#!/bin/sh

# Version number

#YEAR=date +%y
#WEEK=date +%V
SHA=$(git rev-parse --short HEAD)

VERSION="v-$SHA"

IMAGE_NAME="docker-dev.i-chain.net/skub-ui-admin:$VERSION"
echo ${IMAGE_NAME}
docker build -t ${IMAGE_NAME} -f ./Dockerfile .
#docker push ${IMAGE_NAME}