package com.skub.site;

import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ClaimTest extends AbstractTest {

    @Test
    public void testCreateClaim() throws Exception {
        driver.get("http://172.104.228.153:8380/policy");
        driver.findElement(By.linkText("Claim")).click();
        driver.findElement(By.xpath("//mat-radio-button[@id='mat-radio-3']/label/div/div")).click();
        driver.findElement(By.id("mat-input-2")).click();
        driver.findElement(By.id("mat-input-2")).clear();
        driver.findElement(By.id("mat-input-2")).sendKeys("18");
        driver.findElement(By.xpath("//mat-radio-button[@id='mat-radio-5']/label/div/div")).click();
        driver.findElement(By.xpath("//button[@type='button']")).click();
        driver.findElement(By.xpath("//mat-calendar[@id='mat-datepicker-0']/div[2]/mat-month-view/table/tbody/tr[5]/td[4]/div")).click();
        driver.findElement(By.xpath("//div[@id='cdk-step-content-0-0']/vce-step-one/div[2]/button[2]")).click();

        /* Need pause to load */
        WebElement element = (new WebDriverWait(driver, 10))
          .until(ExpectedConditions.elementToBeClickable(
              By.xpath("//mat-select[@id='mat-select-0']/div/div[2]/div")
        ));
        element.click();

        driver.findElement(By.xpath("//mat-option[@id='mat-option-1']/span")).click();
        driver.findElement(By.xpath("//mat-select[@id='mat-select-1']/div/div[2]")).click();
        driver.findElement(By.xpath("//mat-option[@id='mat-option-3']/span")).click();
        driver.findElement(By.id("mat-input-3")).click();
        driver.findElement(By.id("mat-input-3")).clear();
        driver.findElement(By.id("mat-input-3")).sendKeys("10 000");
        driver.findElement(By.xpath("//div[@id='cdk-step-content-0-1']/vce-step-two/div[2]/button[2]")).click();

        element = (new WebDriverWait(driver, 10))
        .until(ExpectedConditions.elementToBeClickable(
          By.xpath("//div[@id='cdk-step-content-0-2']/div[2]/button")
        ));
      element.click();
    }

}
