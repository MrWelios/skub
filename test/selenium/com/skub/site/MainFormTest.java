package com.skub.site;

import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;

public class MainFormTest extends AbstractTest {

    @Test
    public void testMainForm() throws Exception {
        driver.get("http://172.104.228.153:8380/");
        driver.findElement(By.xpath("//input[@type='string']")).click();
        driver.findElement(By.xpath("//input[@type='string']")).clear();
        driver.findElement(By.xpath("//input[@type='string']")).sendKeys("10");
        driver.findElement(By.xpath("//div[3]/div/select")).click();
        new Select(driver.findElement(By.xpath("//div[3]/div/select"))).selectByVisibleText("Albania");
        driver.findElement(By.xpath("//option[@value='Albania']")).click();
        driver.findElement(By.xpath("(//input[@type='string'])[2]")).click();
        driver.findElement(By.xpath("(//button[@type='button'])[54]")).click();
        driver.findElement(By.xpath("(//button[@type='button'])[60]")).click();
        driver.findElement(By.xpath("(//button[@type='button'])[75]")).click();
        driver.findElement(By.name("age")).click();
        driver.findElement(By.name("age")).clear();
        driver.findElement(By.name("age")).sendKeys("30");
        driver.findElement(By.xpath("//div[7]/div/select")).click();
        new Select(driver.findElement(By.xpath("//div[7]/div/select"))).selectByVisibleText("Albania");
        driver.findElement(By.xpath("(//option[@value='Albania'])[2]")).click();
        driver.findElement(By.xpath("//div[8]/div/select")).click();
        new Select(driver.findElement(By.xpath("//div[8]/div/select"))).selectByVisibleText("Afghanistan");
        driver.findElement(By.xpath("(//option[@value='Afghanistan'])[3]")).click();
        driver.findElement(By.xpath("//div[2]/div/div/button")).click();
        driver.findElement(By.xpath("//div[2]/button")).click();
        driver.findElement(By.xpath("//input[@type='string']")).click();
        driver.findElement(By.xpath("//input[@type='string']")).clear();
        driver.findElement(By.xpath("//input[@type='string']")).sendKeys("Test");
        driver.findElement(By.xpath("(//input[@type='string'])[2]")).clear();
        driver.findElement(By.xpath("(//input[@type='string'])[2]")).sendKeys("Users");
        driver.findElement(By.xpath("//input[@type='email']")).click();
        driver.findElement(By.xpath("//input[@type='email']")).clear();
        driver.findElement(By.xpath("//input[@type='email']")).sendKeys("alexey.dedushenko@salesplatform.ru");
        driver.findElement(By.xpath("//vce-datepicker/div/input")).click();
        driver.findElement(By.xpath("//vce-datepicker/div/input")).clear();
        driver.findElement(By.xpath("//vce-datepicker/div/input")).sendKeys("28.07.1992");
        driver.findElement(By.xpath("(//input[@type='string'])[3]")).click();
        driver.findElement(By.xpath("(//input[@type='string'])[3]")).clear();
        driver.findElement(By.xpath("(//input[@type='string'])[3]")).sendKeys("+79523538863");
        driver.findElement(By.xpath("//label/div")).click();
        driver.findElement(By.xpath("(//input[@type='string'])[4]")).click();
        driver.findElement(By.xpath("(//input[@type='string'])[4]")).clear();
        driver.findElement(By.xpath("(//input[@type='string'])[4]")).sendKeys("Sain-Petersburg");
        driver.findElement(By.xpath("(//input[@type='string'])[5]")).click();
        driver.findElement(By.xpath("(//input[@type='string'])[5]")).clear();
        driver.findElement(By.xpath("(//input[@type='string'])[5]")).sendKeys("Saint-Petersburg");
        driver.findElement(By.xpath("(//input[@type='string'])[7]")).click();
        driver.findElement(By.xpath("(//input[@type='string'])[7]")).clear();
        driver.findElement(By.xpath("(//input[@type='string'])[7]")).sendKeys("123456");
        driver.findElement(By.xpath("//div[2]/div/div/button")).click();
        driver.findElement(By.xpath("//select")).click();
        new Select(driver.findElement(By.xpath("//select"))).selectByVisibleText("Card");
        driver.findElement(By.xpath("//option[2]")).click();
        driver.findElement(By.xpath("//button")).click();
    }

}
