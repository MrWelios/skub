# build image
FROM node:8.11.3-jessie as builder

WORKDIR /app
COPY . ./

# install dependencies
RUN set -x \
    && cd /app/ \
    && npm install

## build
RUN set -x \
    && cd /app \
    && node node_modules/@angular/cli/bin/ng build --base-href=/admin/ --configuration=dev

# executable image
FROM nginx:1.15-alpine

# builded site
COPY --from=builder /app/dist /usr/share/nginx/html

# nginx config file
COPY --from=builder /app/config/nginx/nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]